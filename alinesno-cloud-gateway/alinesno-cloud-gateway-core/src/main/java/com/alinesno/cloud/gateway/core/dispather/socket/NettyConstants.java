package com.alinesno.cloud.gateway.core.dispather.socket;

import io.netty.util.AttributeKey;

/**
 * 参数 
 * @author LuoAnDong
 * @since 2019年9月21日 下午9:07:05
 */
public interface NettyConstants {

	AttributeKey<Integer> NETTY_CHANNEL_PORT = AttributeKey.valueOf("netty.channel.port");
	 
	interface Opcode {
		
		public static final byte GET = 0x00;
		public static final byte SET = 0x01;
		public static final byte DELETE = 0x04;
		
	}
	
	interface MessageStatus {
		
		public static final short NO_ERROR = 0x0000;
		public static final short KEY_NOT_FOUND = 0x0001;
		public static final short KEY_EXISTS = 0x0002;
		public static final short VALUE_TOO_LARGE = 0x0003;
		public static final short INVALID_ARGUMENTS = 0x0004;
		public static final short ITEM_NOT_STORED = 0x0005;
		public static final short INC_DEC_NON_NUM_VAL = 0x0006;
		
	}
	
}
