package com.alinesno.cloud.gateway.core.dispather.socket.channel;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.alinesno.cloud.gateway.core.dispather.SocketDispather;
import com.alinesno.cloud.gateway.core.dispather.socket.bean.BusinessServerBean;
import com.alinesno.cloud.gateway.core.dispather.socket.coder.RequestMessageEncoder;
import com.alinesno.cloud.gateway.core.dispather.socket.coder.ResponseMessageDecoder;
import com.alinesno.cloud.gateway.core.dispather.socket.handler.IdleServerHandler;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.timeout.IdleStateHandler;

/**
 * 初始化Netty接收服务,用于默认接收Socket请求
 * @author LuoAnDong
 * @since 2019年9月21日 下午6:59:19
 */
@Scope("prototype") // 多例
@Component
public class ServerChannelInitializer extends ChannelInitializer<SocketChannel> {

	private static final Logger log = LoggerFactory.getLogger(ServerChannelInitializer.class);
	
	private final static int READER_IDLE_TIME_SECONDS = 45;//读操作空闲20秒
	private final static int WRITER_IDLE_TIME_SECONDS = 45;//写操作空闲20秒
	private final static int ALL_IDLE_TIME_SECONDS = 60;//读写全部空闲40秒

	private int listenPort ; // 监听的端口
	private BusinessServerBean businessServerBean ; 
	
    @Autowired
    private SocketDispather socketDispather ; 
    
    private RequestMessageEncoder encoder ; 
    private ResponseMessageDecoder decoder ; 
    
    @Override
    protected void initChannel(SocketChannel socketChannel) throws Exception {
    
		log.debug("socketDispather:{}" , socketDispather.hashCode());
    	socketDispather.setBusinessServerBean(businessServerBean); 
    	
    	ChannelPipeline p = socketChannel.pipeline();
    	
    	p.addLast("idleStateHandler", new IdleStateHandler(READER_IDLE_TIME_SECONDS , WRITER_IDLE_TIME_SECONDS, ALL_IDLE_TIME_SECONDS, TimeUnit.SECONDS));
	    p.addLast("idleTimeoutHandler", new IdleServerHandler());
	   
	    // 待优化和验证 设置解码器和编码器
        p.addLast(this.getEncoder()) ; 
        p.addLast(this.getDecoder()) ; 
	    
        p.addLast("socketDispather", socketDispather);
    }

	public int getListenPort() {
		return listenPort;
	}

	public void setListenPort(int listenPort) {
		this.listenPort = listenPort;
	}

	public BusinessServerBean getBusinessServerBean() {
		return businessServerBean;
	}

	public void setBusinessServerBean(BusinessServerBean businessServerBean) {
		this.businessServerBean = businessServerBean;
	}

	public RequestMessageEncoder getEncoder() {
		return encoder;
	}

	public void setEncoder(RequestMessageEncoder encoder) {
		this.encoder = encoder;
	}

	public ResponseMessageDecoder getDecoder() {
		return decoder;
	}

	public void setDecoder(ResponseMessageDecoder decoder) {
		this.decoder = decoder;
	}
    
}