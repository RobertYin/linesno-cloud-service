package com.alinesno.cloud.gateway.core.dispather.annotation;
/**
 * 接口数据请求的类型
 * @author LuoAnDong
 * @date 2017年12月1日 下午9:01:56
 */
public enum HttpApiRequestMethod {
	
	SOCKET("socket") , 
	WEBSERVER("webserver") , 
	HTTP("http") , 
	POST("post") , 
	GET("get")  ; 

	private String value ; 
	
	HttpApiRequestMethod(String value) {
		this.value = value ; 
	}
	
	public String value() {
		return value ; 
	}
}