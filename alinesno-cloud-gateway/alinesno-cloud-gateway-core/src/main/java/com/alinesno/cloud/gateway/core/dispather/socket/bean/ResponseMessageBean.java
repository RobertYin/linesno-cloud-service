package com.alinesno.cloud.gateway.core.dispather.socket.bean;

/**
 * 消息传递实体<br/>
 * 该类,代表从 Memcached 服务器返回的响应
	幻数
	opCode,这反映了创建操作的响应
	数据类型,这表明这个是基于二进制还是文本
	响应的状态,这表明如果请求是成功的
	惟一的 id
	compare-and-set 值
	使用额外的 flag
	表示该值存储的一个有效期
	响应创建的 key
	实际数据
 * 
 * @author LuoAnDong
 * @since 2019年9月21日 下午7:38:43
 */
@SuppressWarnings("serial")
public class ResponseMessageBean extends MessageBaseBean {
	
	private int magic = 0x80; // 固定头部
	private int length ; // 报文长度
	private String data ; // 内容
	
	public int getMagic() {
		return magic;
	}
	public void setMagic(int magic) {
		this.magic = magic;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	
}
