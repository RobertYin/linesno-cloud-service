package com.alinesno.cloud.gateway.core.dispather.annotation;
/**
 * 注解值 
 * @author LuoAnDong
 *
 */
public class HttpApiAnnotationBean {
	
	private String method ; 
	private boolean validate ; 
	private String desc ;
	
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public boolean isValidate() {
		return validate;
	}
	public void setValidate(boolean validate) {
		this.validate = validate;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	} 
	
	
}