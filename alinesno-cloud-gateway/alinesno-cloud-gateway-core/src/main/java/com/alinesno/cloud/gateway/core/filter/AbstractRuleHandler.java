package com.alinesno.cloud.gateway.core.filter;

import com.alinesno.cloud.gateway.core.filter.rule.RuleHandler;

/**
 * 规则引擎基础
 * @author LuoAnDong
 * @since 2019年9月21日 下午11:32:08
 */
public abstract class AbstractRuleHandler implements RuleHandler {

	@Override
	public int getOrder() {
		return 0;
	}

	@Override
	public String named() {
		return null;
	}

}
