package com.alinesno.cloud.gateway.core.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.core.annotation.AliasFor;
import org.springframework.stereotype.Component;

import com.alinesno.cloud.gateway.core.enums.EncryptEnums;

/**
 * Http服务注解  
 * @author LuoAnDong
 * @since 2019年9月22日 上午12:42:25
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface SocketService {

	/**
	 * The value may indicate a suggestion for a logical component name,
	 * to be turned into a Spring bean in case of an autodetected component.
	 * @return the suggested component name, if any (or empty String otherwise)
	 */
	@AliasFor(annotation = Component.class)
	String value() default "";

	/**
	 * 服务端口号
	 * @return
	 */
	int port() default 8080 ; 

	/**
	 * 业务代码
	 * @return
	 */
	String busincess() default "" ; 

	/**
	 * 银行代码
	 * @return
	 */
	String bank() default "" ; 

	/**
	 * 接口代码
	 * @return
	 */
	String api() default "" ; 

	/**
	 * 交易代码
	 * @return
	 */
	String transaction() default "" ; 

	/**
	 * 是否身份验证
	 * @return
	 */
	boolean validate() default false ; 

	/**
	 * 是否加密 
	 * @return
	 */
	boolean encrypt() default false ; 

	/**
	 * 加密方法
	 * @return
	 */
	EncryptEnums encryptMethod() default EncryptEnums.MD5 ; 
}
