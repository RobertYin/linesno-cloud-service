package com.alinesno.cloud.gateway.core.dispather.socket.channel;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import io.netty.channel.Channel;

/**
 * 通道管理
 * @author LuoAnDong
 * @since 2019年9月21日 下午7:23:49
 */
public class ChannelCache {

	private final static Map<String, Channel> channelCache = new ConcurrentHashMap<String, Channel>();

	public void put(String key, Channel value) {
		channelCache.put(key, value);
	}

	public Channel get(String key) {
		return channelCache.get(key);
	}

	public void remove(String key) {
		channelCache.remove(key);
	}

	public int size() {
		return channelCache.size();
	}
}