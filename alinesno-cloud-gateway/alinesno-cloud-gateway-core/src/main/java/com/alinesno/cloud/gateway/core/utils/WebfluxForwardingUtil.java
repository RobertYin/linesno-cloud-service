package com.alinesno.cloud.gateway.core.utils;
import java.util.Map;

import org.springframework.http.HttpMethod;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilterChain;

import com.alinesno.cloud.gateway.core.constants.Constant;

import reactor.core.publisher.Mono;

/**
 *  工具类
 * @author LuoAnDong
 * @since 2019年9月21日 下午6:17:04
 */
public class WebfluxForwardingUtil {
	/**
	 * 
	 * @param forwardToPath: forward target path that begin with /.
	 * @param exchange: the current source server exchange
	 * @param forwardAttrs : the attributes that added to forward Exchange.
	 * @return Mono<Void> to signal forwarding request completed.
	 */
	public static Mono<Void> forward(String forwardToPath,ServerWebExchange exchange,Map<String,Object> forwardAttrs){
	    WebFilterChain webFilterChain = (WebFilterChain)exchange.getAttributes().get(Constant.WEB_FILTER_ATTR_NAME);
	    ServerHttpRequest forwardReq = exchange.getRequest().mutate().path(forwardToPath).build();
	    ServerWebExchange forwardExchange = exchange.mutate().request(forwardReq).build();
        if(null != forwardAttrs && !forwardAttrs.isEmpty()) {
        		forwardExchange.getAttributes().putAll(forwardAttrs);
        }
        return webFilterChain.filter(forwardExchange);
	}
	

	/**
	 * 判断请求链接 
	 * @param method
	 * @return
	 */
	public static boolean requireHttpBody(HttpMethod method) {
		return HttpMethod.POST == method || HttpMethod.PUT == method || HttpMethod.PATCH == method
				|| HttpMethod.TRACE == method;
	}

}