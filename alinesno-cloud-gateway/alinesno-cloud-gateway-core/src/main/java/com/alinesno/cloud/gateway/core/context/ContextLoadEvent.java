package com.alinesno.cloud.gateway.core.context;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.alinesno.cloud.gateway.core.annotation.GatewayFilter;
import com.alinesno.cloud.gateway.core.annotation.GatewayHttpResponseFilter;
import com.alinesno.cloud.gateway.core.annotation.GatewayPlugin;
import com.alinesno.cloud.gateway.core.annotation.GatewayRule;
import com.alinesno.cloud.gateway.core.filter.FilterExecutionChain;

/**
 * 获取到指定注解的对象并初始化
 * 
 * @author LuoAnDong
 * @since 2019年9月22日 上午12:37:20
 */
@Component
public class ContextLoadEvent implements ApplicationListener<ContextRefreshedEvent> {

	private static final Logger log = LoggerFactory.getLogger(ContextLoadEvent.class) ; 
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		log.debug("加载自定义插件:{}" , (event.getApplicationContext().getParent() == null));
		
		// 过滤器注解
		Map<String, Object> filters = event.getApplicationContext().getBeansWithAnnotation(GatewayFilter.class);
		Map<String, Object> responseFilters = event.getApplicationContext().getBeansWithAnnotation(GatewayHttpResponseFilter.class);
		FilterExecutionChain.addFilterList(filters);
		FilterExecutionChain.addFilterList(responseFilters);

		// 插件注解
		Map<String, Object> plugins = event.getApplicationContext().getBeansWithAnnotation(GatewayPlugin.class);
		FilterExecutionChain.addFilterList(plugins);

		// 规则注解 
		Map<String, Object> rules = event.getApplicationContext().getBeansWithAnnotation(GatewayRule.class);
		FilterExecutionChain.addFilterList(rules);
	}

}
