# 服务规划

网关服务组件，由业务组服务形成，此处接口样例标准

## 接口样例

### Http接口样例 

**请求URL：** 
- ` http://localhost:27008/api/user/register `

- URL 参数说明:
/api/{version}/{model}/{method}

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|version |是  |string |接口版本号   |
|model|是  |string | 所属模块    |
|method |否  |string | 所属接口  |

**请求方式：**
- POST  

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|username |是  |string |用户名   |
|password |是  |string | 密码    |
|name     |否  |string | 昵称    |

**返回示例**

``` 
  {
    "code": 0,
    "message":"请求成功" , 
    "data": {
      "uid": "1",
      "username": "12154545",
      "name": "吴系挂",
      "groupid": 2 ,
      "reg_time": "1436864169",
      "last_login_time": "0",
    }
  }
```

 **返回参数说明** 

|参数名|类型|说明|
|:-----  |:-----|-----                           |
|code|int   |状态码|
|message |string |提示语|
|data|string ||


### Socket 接口样例 

