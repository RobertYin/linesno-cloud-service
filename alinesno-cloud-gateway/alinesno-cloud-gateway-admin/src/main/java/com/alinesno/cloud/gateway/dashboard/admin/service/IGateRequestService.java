package com.alinesno.cloud.gateway.dashboard.admin.service;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.common.facade.services.IBaseService;
import com.alinesno.cloud.gateway.dashboard.admin.entity.GateRequestEntity;

/**
 * <p> 最好编辑完后删除主键，这样就是只读状态，不能随意更改。需要更改就重新加上主键。

每次启动服务器时加载整个表到内存。
这个表不可省略，model内注解的权限只是客户端能用的，其它可以保证即便服务端代码错误时也不会误删数据。 服务类 </p>
 *
 * @author LuoAnDong
 * @since 2019-09-22 05:22:27
 */
@NoRepositoryBean
public interface IGateRequestService extends IBaseService<GateRequestEntity, String> {

}
