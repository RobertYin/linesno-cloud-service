package com.alinesno.cloud.gateway.dashboard.admin.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-09-22 05:22:27
 */
@Entity
@Table(name="gate_dashboard_user")
public class GateDashboardUserEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 用户名
     */
	@Column(name="user_name")
	private String userName;
    /**
     * 用户密码
     */
	private String password;
    /**
     * 角色
     */
	private Integer role;
    /**
     * 是否删除
     */
	private Integer enabled;
    /**
     * 创建时间
     */
	@Column(name="date_created")
	private Date dateCreated;
    /**
     * 更新时间
     */
	@Column(name="date_updated")
	private Date dateUpdated;


	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getRole() {
		return role;
	}

	public void setRole(Integer role) {
		this.role = role;
	}

	public Integer getEnabled() {
		return enabled;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}


	@Override
	public String toString() {
		return "GateDashboardUserEntity{" +
			"userName=" + userName +
			", password=" + password +
			", role=" + role +
			", enabled=" + enabled +
			", dateCreated=" + dateCreated +
			", dateUpdated=" + dateUpdated +
			"}";
	}
}
