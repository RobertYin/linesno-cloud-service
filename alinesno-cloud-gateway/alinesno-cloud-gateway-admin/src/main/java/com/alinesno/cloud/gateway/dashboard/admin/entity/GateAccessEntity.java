package com.alinesno.cloud.gateway.dashboard.admin.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;


/**
 * <p>
 * 权限配置
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-09-22 05:22:27
 */
@Entity
@Table(name="gate_access")
public class GateAccessEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 是否为调试表，只允许在开发环境使用，测试和线上环境禁用
     */
	private Integer debug;
    /**
     * 实际表名，例如 apijson_user
     */
	private String name;
    /**
     * 外部调用的表别名，例如 User
     */
	private String alias;
    /**
     * 允许 get 的角色列表，例如 ["LOGIN", "CONTACT", "CIRCLE", "OWNER"]
用 JSON 类型不能设置默认值，反正权限对应的需求是明确的，也不需要自动转 JSONArray。
TODO: 直接 LOGIN,CONTACT,CIRCLE,OWNER 更简单，反正是开发内部用，不需要复杂查询。
     */
	private String get;
    /**
     * 允许 head 的角色列表，例如 ["LOGIN", "CONTACT", "CIRCLE", "OWNER"]
     */
	private String head;
    /**
     * 允许 gets 的角色列表，例如 ["LOGIN", "CONTACT", "CIRCLE", "OWNER"]
     */
	private String gets;
    /**
     * 允许 heads 的角色列表，例如 ["LOGIN", "CONTACT", "CIRCLE", "OWNER"]
     */
	private String heads;
    /**
     * 允许 post 的角色列表，例如 ["LOGIN", "CONTACT", "CIRCLE", "OWNER"]
     */
	private String httpPost;
    /**
     * 允许 put 的角色列表，例如 ["LOGIN", "CONTACT", "CIRCLE", "OWNER"]
     */
	private String httpPut;
    /**
     * 允许 delete 的角色列表，例如 ["LOGIN", "CONTACT", "CIRCLE", "OWNER"]
     */
	private String httpDelete;
    /**
     * 创建时间
     */
	private Date date;


	public Integer getDebug() {
		return debug;
	}

	public void setDebug(Integer debug) {
		this.debug = debug;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getGet() {
		return get;
	}

	public void setGet(String get) {
		this.get = get;
	}

	public String getHead() {
		return head;
	}

	public void setHead(String head) {
		this.head = head;
	}

	public String getGets() {
		return gets;
	}

	public void setGets(String gets) {
		this.gets = gets;
	}

	public String getHeads() {
		return heads;
	}

	public void setHeads(String heads) {
		this.heads = heads;
	}



	public String getHttpPost() {
		return httpPost;
	}

	public void setHttpPost(String httpPost) {
		this.httpPost = httpPost;
	}

	public String getHttpPut() {
		return httpPut;
	}

	public void setHttpPut(String httpPut) {
		this.httpPut = httpPut;
	}

	public String getHttpDelete() {
		return httpDelete;
	}

	public void setHttpDelete(String httpDelete) {
		this.httpDelete = httpDelete;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "GateAccessEntity [debug=" + debug + ", name=" + name + ", alias=" + alias + ", get=" + get + ", head="
				+ head + ", gets=" + gets + ", heads=" + heads + ", httpPost=" + httpPost + ", httpPut=" + httpPut
				+ ", httpDelete=" + httpDelete + ", date=" + date + "]";
	}

	
}
