package com.alinesno.cloud.gateway.dashboard.admin.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.common.core.service.impl.IBaseServiceImpl;
import com.alinesno.cloud.gateway.dashboard.admin.entity.GateSelectorConditionEntity;
import com.alinesno.cloud.gateway.dashboard.admin.service.IGateSelectorConditionService;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2019-09-22 05:22:27
 */
@Service
public class GateSelectorConditionServiceImpl extends IBaseServiceImpl< GateSelectorConditionEntity, String> implements IGateSelectorConditionService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(GateSelectorConditionServiceImpl.class);

}
