package com.alinesno.cloud.gateway.dashboard.admin.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-09-22 05:22:27
 */
@Entity
@Table(name="gate_plugin")
public class GatePluginEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 插件名称
     */
	private String name;
    /**
     * 插件配置
     */
	private String config;
    /**
     * 插件角色
     */
	private Integer role;
    /**
     * 是否开启（0，未开启，1开启）
     */
	private Integer enabled;
    /**
     * 创建时间
     */
	@Column(name="date_created")
	private Date dateCreated;
    /**
     * 更新时间
     */
	@Column(name="date_updated")
	private Date dateUpdated;


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getConfig() {
		return config;
	}

	public void setConfig(String config) {
		this.config = config;
	}

	public Integer getRole() {
		return role;
	}

	public void setRole(Integer role) {
		this.role = role;
	}

	public Integer getEnabled() {
		return enabled;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}


	@Override
	public String toString() {
		return "GatePluginEntity{" +
			"name=" + name +
			", config=" + config +
			", role=" + role +
			", enabled=" + enabled +
			", dateCreated=" + dateCreated +
			", dateUpdated=" + dateUpdated +
			"}";
	}
}
