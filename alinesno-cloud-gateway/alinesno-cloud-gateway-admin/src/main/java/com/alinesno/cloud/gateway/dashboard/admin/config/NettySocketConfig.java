package com.alinesno.cloud.gateway.dashboard.admin.config;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.alinesno.cloud.gateway.core.dispather.socket.NettySocketServer;
import com.alinesno.cloud.gateway.core.dispather.socket.bean.BusinessServerBean;

@Configuration
public class NettySocketConfig {

	/**
	 * 启动netty服务
	 * @return
	 */
	@Bean(initMethod = "start" , destroyMethod = "stop")
	public NettySocketServer nettySocketServer() {
		NettySocketServer socketServer = new NettySocketServer();	
	
		// 初始化map方法
		Map<Integer , BusinessServerBean> inetSocketAddressMap= new HashMap<Integer , BusinessServerBean>(); 
		
		// 从数据库或者配置中查询
		for(int i = 18090 ; i < 18094 ; i ++) {
			BusinessServerBean bean = new BusinessServerBean() ; 
			
			bean.setServerPort(i); 
			bean.setBusinessCode("businessBank_" + i);
			bean.setBusinessDesc("业务服务:" + i);
			
			inetSocketAddressMap.put(i, bean) ; 
		}
		
		socketServer.setInetSocketAddressMap(inetSocketAddressMap); 
		
		return socketServer ; 
	}

}
