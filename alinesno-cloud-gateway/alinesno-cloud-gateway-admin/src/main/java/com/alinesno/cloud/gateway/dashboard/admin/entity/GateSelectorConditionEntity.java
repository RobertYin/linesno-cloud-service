package com.alinesno.cloud.gateway.dashboard.admin.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-09-22 05:22:27
 */
@Entity
@Table(name="gate_selector_condition")
public class GateSelectorConditionEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 选择器id
     */
	@Column(name="selector_id")
	private String selectorId;
    /**
     * 参数类型（post  query  uri等）
     */
	@Column(name="param_type")
	private String paramType;
    /**
     * 匹配符（=  > <  like match）
     */
	private String operator;
    /**
     * 参数名称
     */
	@Column(name="param_name")
	private String paramName;
    /**
     * 参数值
     */
	@Column(name="param_value")
	private String paramValue;
    /**
     * 创建时间
     */
	@Column(name="date_created")
	private Date dateCreated;
    /**
     * 更新时间
     */
	@Column(name="date_updated")
	private Date dateUpdated;


	public String getSelectorId() {
		return selectorId;
	}

	public void setSelectorId(String selectorId) {
		this.selectorId = selectorId;
	}

	public String getParamType() {
		return paramType;
	}

	public void setParamType(String paramType) {
		this.paramType = paramType;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getParamValue() {
		return paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}


	@Override
	public String toString() {
		return "GateSelectorConditionEntity{" +
			"selectorId=" + selectorId +
			", paramType=" + paramType +
			", operator=" + operator +
			", paramName=" + paramName +
			", paramValue=" + paramValue +
			", dateCreated=" + dateCreated +
			", dateUpdated=" + dateUpdated +
			"}";
	}
}
