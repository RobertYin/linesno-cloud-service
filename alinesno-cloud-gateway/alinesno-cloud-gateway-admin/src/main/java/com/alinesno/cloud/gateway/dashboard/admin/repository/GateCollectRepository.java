package com.alinesno.cloud.gateway.dashboard.admin.repository;

import com.alinesno.cloud.common.facade.orm.repository.IBaseJpaRepository;
import com.alinesno.cloud.gateway.dashboard.admin.entity.GateCollectEntity;

/**
 * <p>
  *  持久层接口
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-09-22 05:22:27
 */
public interface GateCollectRepository extends IBaseJpaRepository<GateCollectEntity, String> {

}
