package com.alinesno.cloud.project.sample.filter;

import java.net.InetSocketAddress;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alinesno.cloud.gateway.core.annotation.GatewayFilter;
import com.alinesno.cloud.gateway.core.exception.GatewayException;
import com.alinesno.cloud.gateway.core.filter.FilterHandler;

@GatewayFilter
public class SampleHelloFilter implements FilterHandler{

	private static final Logger log = LoggerFactory.getLogger(SampleHelloFilter.class) ; 
	
	@Override
	public void doFilter(InetSocketAddress insocket, Map<String, Object> params) throws GatewayException {
		log.debug("自定义请求过滤器.");
	}

}
