package com.alinesno.cloud.project.sample.filter;

import java.net.InetSocketAddress;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alinesno.cloud.gateway.core.annotation.GatewayHttpResponseFilter;
import com.alinesno.cloud.gateway.core.exception.GatewayException;
import com.alinesno.cloud.gateway.core.filter.FilterHandler;

@GatewayHttpResponseFilter
public class SampleHelloResponseFilter implements FilterHandler{

	private static final Logger log = LoggerFactory.getLogger(SampleHelloResponseFilter.class) ; 
	
	@Override
	public void doFilter(InetSocketAddress insocket, Map<String, Object> params) throws GatewayException {
		log.debug("自定义返回过滤器.");
	}

}
