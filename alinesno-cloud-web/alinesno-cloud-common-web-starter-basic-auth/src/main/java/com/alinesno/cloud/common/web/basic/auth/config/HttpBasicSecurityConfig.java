package com.alinesno.cloud.common.web.basic.auth.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * 基本配置信息
 * @author LuoAnDong
 * @since 2019年9月22日 下午1:35:59
 */
@Configuration
public class HttpBasicSecurityConfig extends WebSecurityConfigurerAdapter {

	@Value("${security.user.name:admin}")
	private String user ;
	
	@Value("${security.user.password:admin}")
	private String password ; 

	@Value("${security.user.role:USER}")
	private String role ; 
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable().authorizeRequests().anyRequest().authenticated().and().httpBasic();
		//防止iframe
        http.headers().frameOptions().disable();
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		
		//inMemoryAuthentication 从内存中获取  
        auth.inMemoryAuthentication()
        	.passwordEncoder(new BCryptPasswordEncoder())
            .withUser(user)
            .password(new BCryptPasswordEncoder()
            .encode(password)).roles(role);
	}
}