package com.alinesno.cloud.common.web.basic.auth.enable;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;

import com.alinesno.cloud.common.core.auto.CustomAutoConfigurationImportSelector;
import com.alinesno.cloud.common.core.context.ApplicationContextProvider;
import com.alinesno.cloud.common.web.basic.auth.config.HttpBasicSecurityConfig;
import com.alinesno.cloud.common.web.basic.auth.controller.HttpBasicDashboardController;

/**
 * 引入自动类
 * @author LuoAnDong
 * @sine 2019年4月5日 下午3:34:07
 */
public class HttpBasicAuthConfigurationSelector extends CustomAutoConfigurationImportSelector {

	@Override
	public String[] selectImports(AnnotationMetadata annotationMetadata) {
		List<String> importBean = new ArrayList<String>() ; 
	
		AnnotationAttributes attributes = getAttributes(annotationMetadata);
		Set<String> exclusions = getExclusions(annotationMetadata, attributes);
		
		importBean.add(ApplicationContextProvider.class.getName()) ; 
		importBean.add(HttpBasicSecurityConfig.class.getName()) ; 
		importBean.add(HttpBasicDashboardController.class.getName()) ; 
		
		importBean.removeAll(exclusions) ;  // 清理掉不需要引入的bean实体
		
		return importBean.toArray(new String[] {}) ;
	}

	@Override
	protected Class<?> getAnnotationClass() {
		return EnableHttpBasicAuth.class ;
	}

}
