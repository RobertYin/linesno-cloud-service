package com.alinesno.cloud.common.web.base.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.ui.Model;

import com.alinesno.cloud.common.core.page.DatatablesPageBean;
import com.alinesno.cloud.common.core.response.ResponseBean;
import com.alinesno.cloud.common.core.response.ResponseGenerator;
import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;
import com.alinesno.cloud.common.facade.orm.repository.IBaseJpaRepository;

/**
 * 工程基类
 * 
 * @author LuoAnDong
 * @since 2018年11月20日 上午8:10:06
 */
public class BaseController {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(BaseController.class);

	public final static String DASHBAORD_PATH = "/dashboard"; // 主面板入口

	@Autowired
	protected HttpServletRequest request;

	@Autowired
	protected HttpServletResponse response;

	@Autowired
	protected HttpSession session;

	/**
	 * Spring mvc 重定向
	 * 
	 * @param url
	 * @return
	 */
	public String redirect(String url) {
		return "redirect:" + url;
	}

	/**
	 * 返回成功
	 * 
	 * @return
	 */
	protected ResponseBean ok() {
		return ok(null);
	}

	/**
	 * 返回成功
	 * 
	 * @return
	 */
	protected ResponseBean ok(String message) {
		return ResponseGenerator.ok(message);
	}

	/**
	 * 返回失败
	 * 
	 * @return
	 */
	protected ResponseBean fail() {
		return fail(null);
	}

	/**
	 * 返回失败
	 * 
	 * @return
	 */
	protected ResponseBean fail(String message) {
		return ResponseGenerator.fail(message);
	}

	public <T extends BaseEntity> DatatablesPageBean toPage(Model model, Specification<T> specification,
			IBaseJpaRepository<T, String> jpa, DatatablesPageBean page, Sort sort) {
		// Page<T> listPage = null;

		// Specification<T> filterSpec = specification;
		// List<SchoolEntity> schools = new ArrayList<SchoolEntity>();
		//
		// if (!AccountRoleEnum.ADMIN.getCode().equals(currentManager().getRolePower()))
		// {
		//
		// filterSpec = new Specification<T>() {
		// @Override
		// public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query,
		// CriteriaBuilder criteriaBuilder) {
		// List<Predicate> predicates = new ArrayList<Predicate>();
		//
		// Predicate condition = criteriaBuilder.equal(root.get("schoolCode"),
		// currentManager().getSchoolCode());
		// Predicate c = specification.toPredicate(root, query, criteriaBuilder);
		//
		// predicates.add(condition);
		// predicates.add(c);
		//
		// return query.where(predicates.toArray(new
		// Predicate[predicates.size()])).getRestriction();
		// }
		// };
		//
		// schools = schoolRepository.findAll();
		// } else {
		// schools =
		// schoolRepository.findAllBySchoolCode(currentManager().getSchoolCode());
		// }

		// listPage = jpa.findAll(filterSpec, PageRequest.of(page.getStart() /
		// page.getLength(), page.getLength(), sort));

		DatatablesPageBean p = new DatatablesPageBean();
		// p.setData(listPage.getContent());
		// p.setDraw(page.getDraw());
		// p.setRecordsFiltered((int) listPage.getTotalElements());
		// p.setRecordsTotal((int) listPage.getTotalElements());

		return p;
	}

	public <T extends BaseEntity> DatatablesPageBean toPage(Model model, Specification<T> specification,
			IBaseJpaRepository<T, String> jpa, DatatablesPageBean page) {
		Sort sort = Sort.by(Direction.DESC, "addTime");
		return toPage(model, specification, jpa, page, sort);
	}

	/**
	 * @Description 响应Ajax请求
	 * @param response
	 * @param content
	 *            响应内容
	 * @param charset
	 *            字符编码
	 * @throws IOException
	 */
	public static void outPrint(HttpServletResponse response, String content, String charset) throws IOException {
		response.setContentType("text/html;charset=" + charset);
		PrintWriter out = response.getWriter();
		out.print(content);
		out.flush();
	}

}
