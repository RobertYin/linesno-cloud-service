package com.alinesno.cloud.common.web.base.exceptions;


import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.alibaba.fastjson.JSONObject;
import com.alinesno.cloud.common.core.response.ResponseBean;

/**
 * Web异常切面，处理Dubbo返回的异常信息
 * @author LuoAnDong
 * @since 2019年9月18日 上午8:06:19
 */
@Aspect
@Component
public class WebExceptionAspect {
	private static final Logger log = LoggerFactory.getLogger(WebExceptionAspect.class);

	@Pointcut("@annotation(org.springframework.web.bind.annotation.RequestMapping)"
			+ "||@annotation(org.springframework.web.bind.annotation.GetMapping)"
			+ "||@annotation(org.springframework.web.bind.annotation.PostMapping)"
			)
	private void webPointcut() {
	}

	/**
	 * 拦截web层异常，记录异常日志，并返回友好信息到前端 目前只拦截Exception，是否要拦截Error需再做考虑
	 * 
	 * @param e
	 *            异常对象
	 */
	@AfterThrowing(pointcut = "webPointcut()", throwing = "e")
	public void handleThrowing(Exception e) {
	
		ResponseBean response = WebExceptionUtils.handleException(e, null) ; 
		this.writeContent(response);
		
//		String message = e.getMessage();
//		String errorMsg = null;
//		String eclass = e.getClass().getName();
//		
//		if ("com.alibaba.dubbo.rpc.RpcException".equals(eclass)) {
//			errorMsg = "服务调用异常，请联系系统管理员处理！";
//		} else if (e instanceof RpcServiceRuntimeException) {
//			logger.error("服务调用错误：" + ((RpcServiceRuntimeException) e).getThreadInfo());
//			errorMsg = e.getMessage();
//		} else if (message != null && message.startsWith("com.linesno.training.common.exception.RpcServiceRuntimeException")) {
//			errorMsg = message.substring(message.indexOf(":") + 1, message.indexOf("\n") - 1);
//		} else {
//			errorMsg = StringUtils.isEmpty(e.getMessage()) ? "系统异常！" : e.getMessage();
//		}
//		
//		logger.error("业务异常", e);
//		writeContent(errorMsg);
	}

	/**
	 * 将内容输出到浏览器
	 *
	 * @param content
	 *            输出内容
	 */
	private void writeContent(Object content) {
		HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
//		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		
		response.reset();
		response.setCharacterEncoding("UTF-8");
		response.setHeader("Content-Type", "text/plain;charset=UTF-8");
		response.setHeader("icop-content-type", "exception");
		PrintWriter writer = null;
		try {
			writer = response.getWriter();
		} catch (IOException e) {
			log.error("获取response异常:{}" , e);
		}
		
//		if (!(request.getHeader("accept").indexOf("application/json") > -1 || (request.getHeader("X-Requested-With") != null && request.getHeader("X-Requested-With").indexOf("XMLHttpRequest") > -1))) {
//			writer.print(JSONObject.toJSON(content));
//		}
		
//		} else {
//			ResponseBean res = new ResponseBean(); 
//			res.setMessage(content);
//			res.setCode(ResultCodeEnum.INTERNAL_SERVER_ERROR) ; //(HttpStatus.INTERNAL_SERVER_ERROR.value());
//			writer.print(JSONObject.toJSON(res));
//		}
			
		writer.print(JSONObject.toJSON(content));
			
		writer.flush();
		writer.close();
	}
}