package com.alinesno.cloud.common.web.base.exceptions;

import java.util.List;

import javax.lang.exception.RpcServiceRuntimeException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import com.alinesno.cloud.common.core.response.ResponseBean;
import com.alinesno.cloud.common.core.response.ResponseGenerator;
import com.alinesno.cloud.common.web.base.form.FormRepeatException;

/**
 * 统一异常处理包
 * 
 * @author LuoAnDong
 * @since 2019年9月18日 上午8:23:48
 */
public class WebExceptionUtils {

	private static final Logger log = LoggerFactory.getLogger(WebExceptionUtils.class);

	/**
	 * 统一前端异常处理实现
	 * 
	 * @return
	 */
	public static ResponseBean handleException(Exception e, BindingResult result) {

		String message = e.getMessage();
		String eclass = e.getClass().getName();

		log.error("前端调用异常 , eclass:{} , message:{} " , eclass , message , e);

		String errorMsg = "应用异常.";

		if (e instanceof FormRepeatException) {

			errorMsg = e.getMessage();

		} else if (message != null && message.startsWith(RpcServiceRuntimeException.class.getName())) {

			errorMsg = message.substring(message.indexOf(":") + 1, message.indexOf("\n") - 1);

		} else if (eclass != null && eclass.startsWith("org.apache.dubbo.rpc.RpcException")) {

			errorMsg = "远程服务连接不可用,请确认服务是否正常.";

		} else if (eclass != null && eclass.startsWith("feign.FeignException")) {

			errorMsg = "远程服务连接异常,请联系服务管理员处理:" + message;

		}else if (e instanceof IllegalAccessException) {

			String errorMessage = e.getMessage();

			return ResponseGenerator.genFailMessage(errorMessage);
		} else if (e instanceof BindException) {

			if(result != null) {
				String errorMessage = "请求参数异常:<br/>";
				if (result.hasErrors()) {
					List<ObjectError> allErrors = result.getAllErrors();
					for (ObjectError error : allErrors) {
						log.debug(error.getDefaultMessage());
						errorMessage = error.getDefaultMessage(); // +"<br/>" ;
						break;
					}
				}
				
				return ResponseGenerator.genFailMessage(errorMessage);
			}
			
		} else {
			errorMsg = StringUtils.isEmpty(e.getMessage()) ? "系统异常！" : e.getMessage();
		}

		return ResponseGenerator.genFailMessage(errorMsg);
	}

}
