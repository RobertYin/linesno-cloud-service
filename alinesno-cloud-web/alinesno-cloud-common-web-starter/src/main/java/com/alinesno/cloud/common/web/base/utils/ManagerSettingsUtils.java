package com.alinesno.cloud.common.web.base.utils;

import org.apache.commons.lang.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.alinesno.cloud.base.boot.entity.ManagerSettingsEntity;
import com.alinesno.cloud.base.boot.service.IManagerSettingsService;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;

/**
 * 获取配置
 * @author Administrator
 *
 */
@Component
public class ManagerSettingsUtils {
	
	private static final Logger log = LoggerFactory.getLogger(ManagerSettingsUtils.class) ; 

	@Reference
	private IManagerSettingsService managerSettingsService ; 

	private IManagerSettingsService getSettings() {
//		return ApplicationContextProvider.getBean(IManagerSettingsService.class) ; 
		
		return managerSettingsService ; 
	}

	/**
	 * 默认不打开验证码
	 * @return
	 */
	public boolean isOpenCaptcha(){
		ManagerSettingsEntity d = queryKey("sys.captcha.status" , null) ; 
		log.debug("d:{}" , d);
		
		if(d == null) {
			return true ; 
		}
		
		if("open".equals(d.getConfigValue())) {
			return true ; 
		}else {
			return false ; 
		}
	}
	
	public ManagerSettingsEntity queryKey(String key , String applicationId) {
		RestWrapper restWrapper = new RestWrapper() ; 
		restWrapper.eq("configKey", key) ; 
		if(StringUtils.isNotBlank(applicationId)) {
			restWrapper.eq("applicationId", applicationId) ; 
		}
	    ManagerSettingsEntity dto = getSettings().findOne(restWrapper) ; 
	    return dto ; 
	}
	
}
