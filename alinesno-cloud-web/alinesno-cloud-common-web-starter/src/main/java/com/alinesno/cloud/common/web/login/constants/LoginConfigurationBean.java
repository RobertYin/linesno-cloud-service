package com.alinesno.cloud.common.web.login.constants;

import cn.hutool.setting.dialect.Props;

/**
 * 登陆配置
 * @author LuoAnDong
 * @since 2019年5月19日 上午11:46:16
 */
public class LoginConfigurationBean {

	/**
	 * 登陆配置
	 */
	private static Props props = new Props("classpath:shiro.properties");

	/**
	 * 登陆提交路径
	 * @return
	 */
	public static String shiroLoginSubmitPath() {
		return props.getProperty("shiro.path.login.submit", "/login") ;  // 默认登陆页
	}

	/**
	 * 登陆成功页面
	 * @return
	 */
	public static String shiroLoginSuccessPath() {
		return props.getProperty("shiro.path.login.success", "/dashboard") ;  // 默认首页
	}

	/**
	 * 登陆成功页面
	 * @return
	 */
	public static String shiroIndexPath() {
		return props.getProperty("shiro.path.index", "/dashboard") ;  // 默认首页
	}

	/**
	 * 登陆界面
	 * @return
	 */
	public static String shiroLoginPath() {
		return props.getProperty("shiro.path.login", "/login") ;  // 默认登陆页
	}
	
}
