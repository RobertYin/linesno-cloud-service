package com.alinesno.cloud.common.web.base.interceptors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.alinesno.cloud.common.web.base.constants.WebConstants;

/**
 * 判断是否超时
 * @author LuoAnDong
 * @since 2018年8月14日 上午8:14:09
 */
//@Component
public class ManagerSessionInterceptor implements HandlerInterceptor {
	
	private static final Logger log = LoggerFactory.getLogger(ManagerSessionInterceptor.class);
	
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
    		log.debug("管理员路径校验.路径:{}" , request.getRequestURL()); 
    		HttpSession session = request.getSession() ; 
    		Object managerObj = session.getAttribute(WebConstants.CURRENT_MANAGER) ; 
    		if(managerObj == null) {
    			response.sendRedirect("/manager/login");
    			return false ; 
    		}
        return true; // 只有返回true才会继续向下执行，返回false取消当前请求
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,  ModelAndView modelAndView) throws Exception {
    	
    		// 初始化菜单 
    	
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
    }

}