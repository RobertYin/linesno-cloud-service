package com.alinesno.cloud.common.web.login.dialect;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.dialect.AbstractProcessorDialect;
import org.thymeleaf.processor.IProcessor;
import org.thymeleaf.standard.processor.StandardXmlNsTagProcessor;
import org.thymeleaf.templatemode.TemplateMode;

import com.alinesno.cloud.common.web.login.dialect.processor.DictionaryTagProcessor;
import com.alinesno.cloud.common.web.login.dialect.processor.SayToAttributeTagProcessor;
import com.alinesno.cloud.common.web.login.dialect.processor.SystemSettingsTagProcessor;

/**
 * 自定义标签
 * @author LuoAnDong
 * @since 2019年9月29日 下午12:12:09
 */
public class PagesDialect extends AbstractProcessorDialect {

    private static final Logger log = LoggerFactory.getLogger(PagesDialect.class) ; 

    public static final String DEFAULT_PREFIX = "page";
    public static final String NAME = "AlinesnoCloudPageTags";

    public PagesDialect() {
        super(
        		NAME ,// Dialect name
                DEFAULT_PREFIX,            // Dialect prefix (hello:*)
                10000);              // Dialect precedence
    }

    
    /*
     * Initialize the dialect's processors.
     *
     * Note the dialect prefix is passed here because, although we set
     * "hello" to be the dialect's prefix at the constructor, that only
     * works as a default, and at engine configuration time the user
     * might have chosen a different prefix to be used.
     */
    public Set<IProcessor> getProcessors(final String dialectPrefix) {
   
    	log.debug("加载方言.");
    	
        final Set<IProcessor> processors = new HashSet<IProcessor>();
        
        processors.add(new SayToAttributeTagProcessor(dialectPrefix));
        
        processors.add(new SystemSettingsTagProcessor(dialectPrefix));
        processors.add(new DictionaryTagProcessor(dialectPrefix));
        processors.add(new StandardXmlNsTagProcessor(TemplateMode.HTML, dialectPrefix));
        
        return processors;
    }


}