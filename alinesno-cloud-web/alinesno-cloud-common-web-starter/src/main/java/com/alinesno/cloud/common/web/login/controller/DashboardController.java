package com.alinesno.cloud.common.web.login.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.dubbo.config.annotation.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.base.boot.entity.ManagerApplicationEntity;
import com.alinesno.cloud.base.boot.entity.ManagerResourceEntity;
import com.alinesno.cloud.base.boot.service.IManagerApplicationService;
import com.alinesno.cloud.base.boot.service.IManagerResourceService;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.core.response.ResponseBean;
import com.alinesno.cloud.common.core.response.ResponseGenerator;
import com.alinesno.cloud.common.web.base.controller.BaseController;
import com.alinesno.cloud.common.web.login.aop.AccountRecord;
import com.alinesno.cloud.common.web.login.aop.AccountRecord.RecordType;
import com.alinesno.cloud.common.web.login.constants.LoginConfigurationBean;
import com.alinesno.cloud.common.web.login.session.CurrentAccountSession;

/**
 * 控制层
 * @author LuoAnDong
 * @since 2018年11月27日 上午6:41:40
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
public class DashboardController extends BaseController {

	private static final Logger log = LoggerFactory.getLogger(DashboardController.class) ; 
	
//	@Reference
	@Reference
	private IManagerApplicationService IManagerApplicationService ; 
	
//	@Reference
	@Reference
	private IManagerResourceService managerResourceService ; 
	
	@AccountRecord(value="进入工作面板.",type=RecordType.ACCESS_PAGE)
	@RequestMapping(value = "/")
	public String index(Model model , HttpServletRequest request) {
		
		int sessionTime = request.getSession().getMaxInactiveInterval() ; 
		log.debug("session time : {}" , sessionTime);
		
		return this.redirect(LoginConfigurationBean.shiroIndexPath()); 
	}

	@ResponseBody
	@GetMapping("/dashboard/side")
    public ResponseBean side(String resourceParent , String applicationId , HttpServletRequest request){
		log.debug("resourceParant:{} , applicationId:{}" , resourceParent , applicationId);
		
		ManagerResourceEntity resources = managerResourceService.findMenusByApplicationAndAccount(
				resourceParent , 
				applicationId , 
				CurrentAccountSession.get(request).getId()); 
		
		if(resources != null && resources.getSubResource() != null) {
			return ResponseGenerator.ok(resources.getSubResource()) ; 
		}else {
			return ResponseGenerator.fail("") ; 
		}
		
    }
	
	@AccountRecord("进入工作面板.")
	@RequestMapping("/dashboard")
    public String dashboard(Model model , HttpServletRequest request){
		log.debug("dashboard");

		// 查询用户权限
		List<ManagerApplicationEntity> list =  IManagerApplicationService.findAllByAccountId(CurrentAccountSession.get(request).getId()) ; 
		model.addAttribute("applications", list) ; 
		
		return "dashboard/dashboard" ; 
    }
	
	@RequestMapping("/dashboard/home")
    public String home(){
		log.debug("home");
		return "dashboard/home" ;
    }
	
	
	
}
