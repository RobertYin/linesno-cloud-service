package com.alinesno.cloud.common.web.login.session;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alinesno.cloud.base.boot.entity.ManagerAccountEntity;
import com.alinesno.cloud.common.web.login.constants.LoginConstants;

/**
 * 获取当前用户
 * @author LuoAnDong
 * @sine 2019年4月5日 下午4:18:25
 */
@Component
public class CurrentAccountSession {

	private static final Logger log = LoggerFactory.getLogger(CurrentAccountSession.class);
	
	@Autowired
	private HttpServletRequest request ; 
	
	/**
	 * 获取当前用户
	 * @return
	 */
	public static ManagerAccountEntity get(HttpServletRequest request) {
		
		Object o = request.getSession().getAttribute(LoginConstants.CURRENT_USER) ; 
		if(o != null) {
			return (ManagerAccountEntity) o ; 
		}
		return null ; 
	}
	
	public ManagerAccountEntity get() {
	
		log.debug("request:{}" , request);
		Object o = request.getSession().getAttribute(LoginConstants.CURRENT_USER) ; 
		if(o != null) {
			return (ManagerAccountEntity) o ; 
		}
		return null ; 
	}
}
