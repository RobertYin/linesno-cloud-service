package com.alinesno.cloud.common.web.login.filter;

import java.lang.reflect.Method;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 基础类用户拦截截面 
 * @author LuoAnDong
 * @since 2019年9月20日 下午9:57:07
 */
@Order(5)
@Aspect
@Component
public class BeaEntityAdvice {
	
    private static final Logger log = LoggerFactory.getLogger(BeaEntityAdvice.class);
   
    /**
     * 拦截Service类
     */
//    @Pointcut("execution(* com.alinesno.cloud..*Service(..))")
    // @Pointcut("execution(* build*(..))")
    @Pointcut("execution(* com.alinesno.cloud..*.*(..))") 
    public void pointcut(){
    }
   
    @Around("pointcut()")
    public Object around(ProceedingJoinPoint point) {
        Object result = null;
        try {
        	injectOperatorAccount(point);
            result = point.proceed();
        } catch (Throwable e) {
        	e.printStackTrace();
        }
        
        return result;
    }

    /**
     * 注入当前操作用户
     * @param point
     */
    private void injectOperatorAccount(ProceedingJoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        String className = joinPoint.getTarget().getClass().getName(); // 请求的方法名
        Object[] argObj = joinPoint.getArgs() ; 

        log.debug("className:{} , method:{}" , className , method);
        if(argObj != null) {
        	for(Object arg : argObj) {
        		log.debug("arg:{}" , ToStringBuilder.reflectionToString(arg));
        	}
        }
		
	}
	
}


