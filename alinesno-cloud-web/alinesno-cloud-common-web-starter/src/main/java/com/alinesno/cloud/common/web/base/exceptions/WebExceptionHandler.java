package com.alinesno.cloud.common.web.base.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.common.core.response.ResponseBean;

/**
 * springboot全局异常处理
 * 
 * @author LuoAnDong
 * @since 2019年2月8日 下午2:24:05
 */
@ControllerAdvice
public class WebExceptionHandler {

	private static final Logger log = LoggerFactory.getLogger(WebExceptionHandler.class);

	/**
	 * 通用全局拦截
	 * 
	 * @param req
	 * @param e
	 * @return
	 * @throws Exception
	 */
//	@ExceptionHandler(value = {Exception.class , RpcServiceRuntimeException.class})
//	@ResponseBody
//	public ResponseBean defaultErrorHandler(HttpServletRequest req, Exception e , BindingResult result) throws Exception {
//		log.debug("前端异常,进入springmvc通用异常拦截:{}" , e);
//		return WebExceptionUtils.handleException(e, null ); 
//	}

	/**
	 * 前端参数异常
	 * @param e
	 * @param result
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@ExceptionHandler(value = {BindException.class})
	public ResponseBean illegalArgumentException(Exception e , BindingResult result) throws Exception {
		log.debug("前端异常,进入springmvc通用异常拦截:{}" , e);
		return WebExceptionUtils.handleException(e, result); 
	}
	
}