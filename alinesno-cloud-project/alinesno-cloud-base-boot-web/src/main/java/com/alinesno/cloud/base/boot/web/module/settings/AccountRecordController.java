package com.alinesno.cloud.base.boot.web.module.settings;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.dubbo.config.annotation.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.base.boot.entity.ManagerAccountRecordEntity;
import com.alinesno.cloud.base.boot.service.IManagerAccountRecordService;
import com.alinesno.cloud.base.boot.web.module.organization.AccountController;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.core.page.DatatablesPageBean;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.controller.FeignMethodController;

/**
 * 用户操作日志记录
 * @author LuoAnDong
 * @since 2019年4月8日 下午10:29:02
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
@RequestMapping("boot/platform/record")
public class AccountRecordController  extends FeignMethodController<ManagerAccountRecordEntity , IManagerAccountRecordService> {

	private static final Logger log = LoggerFactory.getLogger(AccountController.class) ; 

	@Reference
	private IManagerAccountRecordService managerAccountRecordService;  
	
	@TranslateCode(value="[{hasStatus:has_status}]") 
	@ResponseBody
	@RequestMapping("/datatables")
    public DatatablesPageBean datatables(HttpServletRequest request , Model model ,  DatatablesPageBean page){
		log.debug("page = {}" , ToStringBuilder.reflectionToString(page));
		return this.toPage(model, managerAccountRecordService , page) ;
    }

	@RequestMapping("/detail")
    public void detail(Model model , String id){
		ManagerAccountRecordEntity record = managerAccountRecordService.getOne(id) ; 
		model.addAttribute("bean", record) ; 
		
    }

	@Override
	public IManagerAccountRecordService getFeign() {
		return managerAccountRecordService;
	}
}
