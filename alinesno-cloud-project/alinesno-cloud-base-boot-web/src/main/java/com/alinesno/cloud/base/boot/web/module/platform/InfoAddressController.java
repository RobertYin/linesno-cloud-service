package com.alinesno.cloud.base.boot.web.module.platform;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.dubbo.config.annotation.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.base.boot.entity.InfoAddressEntity;
import com.alinesno.cloud.base.boot.service.IInfoAddressService;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.core.page.DatatablesPageBean;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.controller.FeignMethodController;

/**
 * <p>区域表 前端控制器 </p>
 *
 * @author WeiXiaoJin
 * @since 2019-07-06 13:15:25
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
@RequestMapping("boot/platform/address")
public class InfoAddressController extends FeignMethodController<InfoAddressEntity, IInfoAddressService> {

	//日志记录
	private static final Logger log = LoggerFactory.getLogger(InfoAddressController.class);

	@Reference
	private IInfoAddressService infoAddressService ; 
	
	@TranslateCode(value="[{hasStatus:has_status}]")
	@ResponseBody
	@PostMapping("/datatables")
    public DatatablesPageBean datatables(HttpServletRequest request , Model model ,  DatatablesPageBean page){
		log.debug("page = {}" , ToStringBuilder.reflectionToString(page));
		return this.toPage(model, infoAddressService , page) ;
    }

	/**
	 * 省市列表
	 * @return
	 */
	@ResponseBody
	@PostMapping("/province")
	public List<InfoAddressEntity> provinceList(){
		RestWrapper restWrapper = new RestWrapper() ; 
		restWrapper.lt("level", "3") ;
		
		List<InfoAddressEntity> list = infoAddressService.findAll(restWrapper) ; 
		return list ; 
	}

	@Override
	public IInfoAddressService getFeign() {
		return infoAddressService;
	}

}


























