package com.alinesno.cloud.base.boot.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

import com.alinesno.cloud.common.web.enable.EnableAlinesnoCommonLogin;

/**
 * 启动入口
 * 
 * @author LuoAnDong
 * @since 2018年8月7日 上午8:45:02
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class) 
@EnableAlinesnoCommonLogin
public class BaseBootWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(BaseBootWebApplication.class, args);
	}
	
}
