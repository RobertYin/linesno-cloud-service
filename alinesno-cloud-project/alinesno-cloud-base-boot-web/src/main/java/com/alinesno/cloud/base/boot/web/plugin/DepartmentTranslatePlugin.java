package com.alinesno.cloud.base.boot.web.plugin;

import org.apache.commons.lang.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;
import com.alinesno.cloud.base.boot.entity.ManagerDepartmentEntity;
import com.alinesno.cloud.base.boot.service.IManagerDepartmentService;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.advice.TranslatePlugin;

/**
 * 部门内容转换
 * @author LuoAnDong
 * @since 2019年4月7日 下午10:04:58
 */
@Component
public class DepartmentTranslatePlugin implements TranslatePlugin {

	@Reference
	private IManagerDepartmentService ManagerDepartmentService ; 

	private String DEPARTMENT = "department" ; 
	
	@Override
	public void translate(JSONObject node, TranslateCode convertCode) {
		Object department = node.get(DEPARTMENT) ; 
		
		String value = "" ; 
		if(department != null && StringUtils.isNotBlank(department.toString())) {
			try {
				ManagerDepartmentEntity dto = ManagerDepartmentService.findEntityById(department.toString()) ; 
				if(dto != null) {
					value = dto.getFullName() ;
				}
			}catch(Exception e) {
				log.error("代码转换异常:{}" , e);
			}
		}
		node.put(DEPARTMENT + LABEL_SUFFER, value) ; 
	}

}
