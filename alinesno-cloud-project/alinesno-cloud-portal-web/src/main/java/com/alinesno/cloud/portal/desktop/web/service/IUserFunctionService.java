package com.alinesno.cloud.portal.desktop.web.service;

import java.util.List;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.common.facade.services.IBaseService;
import com.alinesno.cloud.portal.desktop.web.entity.UserFunctionEntity;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2019-06-21 19:03:36
 */
@NoRepositoryBean
public interface IUserFunctionService extends IBaseService<UserFunctionEntity, String> {

	boolean updateFunctions(String id, String rolesId);

	List<UserFunctionEntity> findAllByUid(String id);

}
