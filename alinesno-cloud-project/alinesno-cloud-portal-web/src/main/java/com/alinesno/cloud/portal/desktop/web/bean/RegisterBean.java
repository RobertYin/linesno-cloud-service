package com.alinesno.cloud.portal.desktop.web.bean;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

/**
 * 注册实体对象 
 * @author LuoAnDong
 * @since 2019年7月26日 下午4:46:45
 */
public class RegisterBean {

	@Email(message = "注册用户名只能为邮箱.")
	@NotBlank(message = "邮箱不能为空.")
	@Length(max = 32 , message = "邮箱最大长度不能超过32位")
	private String loginName ; // 邮箱 
	
	@NotBlank(message = "密码不能为空.")
	@Length(min=3 ,max = 32 , message = "密码最大长度不能超过32位,最小不能小于4位")
	private String password ; // 密码
	
	@NotBlank(message = "验证码不能为空.")
	@Length(min=6 , max=6 ,message = "验证码长度为6位")
	private String phoneCode ; // 手机验证码
	
	private boolean original ; // 是否同意协议
	
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPhoneCode() {
		return phoneCode;
	}
	public void setPhoneCode(String phoneCode) {
		this.phoneCode = phoneCode;
	}
	public boolean isOriginal() {
		return original;
	}
	public void setOriginal(boolean original) {
		this.original = original;
	}
	
}
