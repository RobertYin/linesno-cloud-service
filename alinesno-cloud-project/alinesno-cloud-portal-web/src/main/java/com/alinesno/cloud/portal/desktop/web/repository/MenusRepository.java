package com.alinesno.cloud.portal.desktop.web.repository;

import java.util.List;

import com.alinesno.cloud.common.facade.orm.repository.IBaseJpaRepository;
import com.alinesno.cloud.portal.desktop.web.entity.MenusEntity;

/**
 * <p>
  *  持久层接口
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-05-18 14:46:04
 */
public interface MenusRepository extends IBaseJpaRepository<MenusEntity, String> {

	/**
	 * 查询通过主导航
	 * @param value 
	 * @return
	 */
	List<MenusEntity> findAllByHasNavOrderByMenusSortAsc(String value);

}
