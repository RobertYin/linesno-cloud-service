package com.alinesno.cloud.portal.desktop.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing ;

import com.alinesno.cloud.common.web.enable.EnableAlinesnoCommonLogin;

/**
 * 启动入口<br/>
 * @EnableSwagger2 //开启swagger2 
 * 
 * @author LuoAnDong 
 * @sine 2019-05-18 14:05:75
 */
@EnableJpaAuditing // jpa注解支持
@SpringBootApplication
@EnableAlinesnoCommonLogin
public class PortalWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(PortalWebApplication.class, args);
	}
	
}
