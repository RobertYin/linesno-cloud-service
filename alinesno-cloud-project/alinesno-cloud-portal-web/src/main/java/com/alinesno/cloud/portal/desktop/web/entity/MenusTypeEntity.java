package com.alinesno.cloud.portal.desktop.web.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-05-18 14:46:04
 */
@Entity
@Table(name="menus_type")
public class MenusTypeEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 类型名称
     */
	@Column(name="type_menus_name")
	private String typeMenusName;
    /**
     * 菜单类型(1主导航/2子菜单/3链接/4模块)
     */
	@Column(name="type_menus_type")
	private String typeMenusType;
    /**
     * 菜单排序
     */
	@Column(name="type_menus_sort")
	private Integer typeMenusSort;


	public String getTypeMenusName() {
		return typeMenusName;
	}

	public void setTypeMenusName(String typeMenusName) {
		this.typeMenusName = typeMenusName;
	}

	public String getTypeMenusType() {
		return typeMenusType;
	}

	public void setTypeMenusType(String typeMenusType) {
		this.typeMenusType = typeMenusType;
	}

	public Integer getTypeMenusSort() {
		return typeMenusSort;
	}

	public void setTypeMenusSort(Integer typeMenusSort) {
		this.typeMenusSort = typeMenusSort;
	}


	@Override
	public String toString() {
		return "MenusTypeEntity{" +
			"typeMenusName=" + typeMenusName +
			", typeMenusType=" + typeMenusType +
			", typeMenusSort=" + typeMenusSort +
			"}";
	}
}
