package com.alinesno.cloud.portal.desktop.web.ucenter;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.dubbo.config.annotation.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alinesno.cloud.base.boot.entity.ManagerApplicationEntity;
import com.alinesno.cloud.base.boot.service.IManagerApplicationService;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;
import com.alinesno.cloud.common.web.base.controller.BaseController;
import com.alinesno.cloud.common.web.login.aop.DataFilter;

/**
 * 显示设置
 * @author LuoAnDong
 * @since 2019年4月19日 上午6:18:05
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
@RequestMapping("public/portal/ucenter")
public class UcenterController extends BaseController {

	private static final Logger log = LoggerFactory.getLogger(UcenterController.class) ; 

	@Value("${server.servlet.context-path:/}")
	private String serlvetContextPath ; 

	@Reference
	private IManagerApplicationService managerApplicationService ; 

	@DataFilter
	@GetMapping(value = "home")
	public String home(Model model , HttpServletRequest request, RestWrapper restWrapper) {
		log.debug("serlvetContextPath:{}" , serlvetContextPath);

		// 取出前几条数据
		List<ManagerApplicationEntity> list = managerApplicationService.findTop(10 , restWrapper) ; 
		model.addAttribute("appList", list) ; 
		
		return "portal/ucenter/home" ; 
	}
	
	@GetMapping(value = "main")
	public String main(Model model , HttpServletRequest request) {
		log.debug("serlvetContextPath:{}" , serlvetContextPath);
		
		return "portal/ucenter/main" ; 
	}
	
}
