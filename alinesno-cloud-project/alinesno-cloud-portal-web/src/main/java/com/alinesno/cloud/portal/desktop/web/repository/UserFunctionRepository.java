package com.alinesno.cloud.portal.desktop.web.repository;

import java.util.List;

import com.alinesno.cloud.common.facade.orm.repository.IBaseJpaRepository;
import com.alinesno.cloud.portal.desktop.web.entity.UserFunctionEntity;

/**
 * <p>
  *  持久层接口
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-06-21 19:03:36
 */
public interface UserFunctionRepository extends IBaseJpaRepository<UserFunctionEntity, String> {

	void deleteByUid(String uid);

	List<UserFunctionEntity> findAllByUid(String id);

}
