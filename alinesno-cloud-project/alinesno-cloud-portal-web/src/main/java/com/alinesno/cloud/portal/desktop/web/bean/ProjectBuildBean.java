package com.alinesno.cloud.portal.desktop.web.bean;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

/**
 * 菜单创建实体
 * @author LuoAnDong
 * @since 2019年9月19日 下午8:58:45
 */
public class ProjectBuildBean {

	@NotBlank(message = "应用名称不能为空")
	@Length(min = 6 , max = 32 , message = "应用名称长度在6至32位之间")
	private String applicationName ; //: 应用名称
	
	@NotBlank(message = "应用描述不能为空")
	@Length(min = 6 , max = 128 , message = "应用描述长度在6至128位之间")
	private String applicationDesc ; //: 应用描述
	
	private String applicationIcons ; //:
	
	private String serviceProject ; //: on
	private String dockerProject ; //: on
	
	@NotBlank(message = "数据库连接不能为空")
	private String dbUrl ; //: 数据库链接
	
	@NotBlank(message = "数据库账号不能为空")
	private String dbUser ; //: 数据库账号
	
	@NotBlank(message = "数据库密码不能为空")
	private String dbPwd ; //: 数据库密码
	
	private String authorName ; //: 作者名称
	private String packageName ; //: com.alinesno.cloud
	
	@NotBlank(message = "工程名不能为空.")
	private String feignServerPath ; //: 工程名
	
	@NotBlank(message = "git地址不能为空.")
	private String gitRepositoryUrl ; //: git仓库地址
	
	@NotBlank(message = "git账号不能为空.")
	private String gitUserName ; //: git账号
	
	@NotBlank(message = "git密码不能为空.")
	private String gitPassword ; //: git密码
	
	private String jenkinsUrl ; //: Jenkins地址
	private String jenkinsJobname ; //: 任务名称
	private String jenkinsUserName ; //: Jenkins账号
	private String jenkinsPassword ; //: Jenkins密码
	
	public String getApplicationDesc() {
		return applicationDesc;
	}
	public void setApplicationDesc(String applicationDesc) {
		this.applicationDesc = applicationDesc;
	}
	public String getApplicationIcons() {
		return applicationIcons;
	}
	public void setApplicationIcons(String applicationIcons) {
		this.applicationIcons = applicationIcons;
	}
	public String getServiceProject() {
		return serviceProject;
	}
	public void setServiceProject(String serviceProject) {
		this.serviceProject = serviceProject;
	}
	public String getDockerProject() {
		return dockerProject;
	}
	public void setDockerProject(String dockerProject) {
		this.dockerProject = dockerProject;
	}
	public String getApplicationName() {
		return applicationName;
	}
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	public String getDbUrl() {
		return dbUrl;
	}
	public void setDbUrl(String dbUrl) {
		this.dbUrl = dbUrl;
	}
	public String getDbUser() {
		return dbUser;
	}
	public void setDbUser(String dbUser) {
		this.dbUser = dbUser;
	}
	public String getDbPwd() {
		return dbPwd;
	}
	public void setDbPwd(String dbPwd) {
		this.dbPwd = dbPwd;
	}
	public String getAuthorName() {
		return authorName;
	}
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getFeignServerPath() {
		return feignServerPath;
	}
	public void setFeignServerPath(String feignServerPath) {
		this.feignServerPath = feignServerPath;
	}
	public String getGitRepositoryUrl() {
		return gitRepositoryUrl;
	}
	public void setGitRepositoryUrl(String gitRepositoryUrl) {
		this.gitRepositoryUrl = gitRepositoryUrl;
	}
	public String getGitUserName() {
		return gitUserName;
	}
	public void setGitUserName(String gitUserName) {
		this.gitUserName = gitUserName;
	}
	public String getGitPassword() {
		return gitPassword;
	}
	public void setGitPassword(String gitPassword) {
		this.gitPassword = gitPassword;
	}
	public String getJenkinsUrl() {
		return jenkinsUrl;
	}
	public void setJenkinsUrl(String jenkinsUrl) {
		this.jenkinsUrl = jenkinsUrl;
	}
	public String getJenkinsJobname() {
		return jenkinsJobname;
	}
	public void setJenkinsJobname(String jenkinsJobname) {
		this.jenkinsJobname = jenkinsJobname;
	}
	public String getJenkinsUserName() {
		return jenkinsUserName;
	}
	public void setJenkinsUserName(String jenkinsUserName) {
		this.jenkinsUserName = jenkinsUserName;
	}
	public String getJenkinsPassword() {
		return jenkinsPassword;
	}
	public void setJenkinsPassword(String jenkinsPassword) {
		this.jenkinsPassword = jenkinsPassword;
	}
	@Override
	public String toString() {
		return "ProjectBuildBean [applicationName=" + applicationName + ", applicationDesc=" + applicationDesc
				+ ", applicationIcons=" + applicationIcons + ", serviceProject=" + serviceProject + ", dockerProject="
				+ dockerProject + ", dbUrl=" + dbUrl + ", dbUser=" + dbUser + ", dbPwd=" + dbPwd + ", authorName="
				+ authorName + ", packageName=" + packageName + ", feignServerPath=" + feignServerPath
				+ ", gitRepositoryUrl=" + gitRepositoryUrl + ", gitUserName=" + gitUserName + ", gitPassword="
				+ gitPassword + ", jenkinsUrl=" + jenkinsUrl + ", jenkinsJobname=" + jenkinsJobname
				+ ", jenkinsUserName=" + jenkinsUserName + ", jenkinsPassword=" + jenkinsPassword + "]";
	}

	
}
