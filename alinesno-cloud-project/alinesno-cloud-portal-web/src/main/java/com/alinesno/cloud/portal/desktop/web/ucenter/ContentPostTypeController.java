package com.alinesno.cloud.portal.desktop.web.ucenter;

import java.util.List;

import org.apache.dubbo.config.annotation.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.base.boot.entity.ContentPostTypeEntity;
import com.alinesno.cloud.base.boot.service.IContentPostTypeService;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.core.page.DatatablesPageBean;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;
import com.alinesno.cloud.common.web.base.controller.FeignMethodController;
import com.alinesno.cloud.common.web.login.aop.DataFilter;

/**
 * 内容管理
 * @author LuoAnDong
 * @since 2019年4月4日 下午1:23:40
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
@RequestMapping("portal/ucenter/contentType")
public class ContentPostTypeController  extends FeignMethodController<ContentPostTypeEntity , IContentPostTypeService> {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(ContentPostTypeController.class) ; 

	@Reference
	private IContentPostTypeService iContentPostTypeService ; 

	/**
	 * 查询过滤
	 */
	@DataFilter
	@ResponseBody
	@GetMapping("findAllByWrapper")
	public List<ContentPostTypeEntity> findAllByWrapper(DatatablesPageBean page){
		RestWrapper restWrapper = new RestWrapper() ; 
		restWrapper.builderCondition(page.getCondition()) ; 

		return getFeign().findAll(restWrapper.toSpecification()) ; 
	}


	@Override
	public IContentPostTypeService getFeign() {
		return iContentPostTypeService ;
	}

	
	
}
