package com.alinesno.cloud.portal.desktop.web.pages;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.core.response.ResponseBean;
import com.alinesno.cloud.common.core.response.ResponseGenerator;
import com.alinesno.cloud.common.web.base.controller.BaseController;
import com.alinesno.cloud.portal.desktop.web.bean.LinkPathTypeBean;
import com.alinesno.cloud.portal.desktop.web.entity.LinkPathEntity;
import com.alinesno.cloud.portal.desktop.web.entity.LinkPathTypeEntity;
import com.alinesno.cloud.portal.desktop.web.entity.ModuleEntity;
import com.alinesno.cloud.portal.desktop.web.repository.LinkPathRepository;
import com.alinesno.cloud.portal.desktop.web.repository.LinkPathTypeRepository;
import com.alinesno.cloud.portal.desktop.web.service.IModuleService;

import cn.hutool.core.bean.BeanUtil;

/**
 * 学习控制层
 * @author LuoAnDong
 * @since 2019年5月14日 上午7:18:00
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
@RequestMapping(value = "public/portal/link")
public class LinkController extends BaseController {

	private static final Logger log = LoggerFactory.getLogger(LinkController.class) ; 

	@Autowired
	private LinkPathTypeRepository linkPathTypeRepository ; 

	/**
	 * 所有模板
	 */
	@Autowired
	private IModuleService moduleService ; 
	
	@Autowired
	private LinkPathRepository linkPathRepository ; 
	
	@ResponseBody
	@RequestMapping(value = "module")
	public List<ModuleEntity> module(Model model , HttpServletRequest request , String group , String type) {
		log.debug("进入所有模块.");
		List<ModuleEntity> modules = moduleService.findAll() ; 
		
		return modules ;
	}
	
	@ResponseBody
	@RequestMapping(value = "function")
	public List<LinkPathEntity> function(Model model , HttpServletRequest request , String group , String type) {
		log.debug("进入所有模块.");
		List<LinkPathEntity> modules = linkPathRepository.findAll() ; 
		
		return modules ;
	}

	@ResponseBody
	@RequestMapping(value = "group")
	public ResponseBean group(Model model , HttpServletRequest request , String group , String type) {
		log.debug("进入学习目录.");
		
		List<LinkPathTypeEntity> pathGroupTypeList = StringUtils.isNotBlank(type)?
							linkPathTypeRepository.findAllByLinkGroupAndLinkTypeCode(group , type):
							linkPathTypeRepository.findAllByLinkGroup(group);
							
		List<LinkPathTypeBean> pathGroupTypeBeanList = new ArrayList<LinkPathTypeBean>() ; 
		for(LinkPathTypeEntity t : pathGroupTypeList) {
			LinkPathTypeBean b = new LinkPathTypeBean() ; 
			BeanUtil.copyProperties(t, b); 
			List<LinkPathEntity> links = linkPathRepository.findAllByLinkType(b.getLinkTypeCode()) ; 
			log.debug("links:{}" , JSONObject.toJSON(links));
			b.setLinks(links);
			pathGroupTypeBeanList.add(b) ;
		}
	
		return ResponseGenerator.genSuccessResult(pathGroupTypeBeanList) ;
	}
	
}
