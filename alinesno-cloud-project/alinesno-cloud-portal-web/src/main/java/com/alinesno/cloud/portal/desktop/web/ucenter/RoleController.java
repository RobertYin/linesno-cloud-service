package com.alinesno.cloud.portal.desktop.web.ucenter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.dubbo.config.annotation.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.base.boot.entity.ManagerResourceEntity;
import com.alinesno.cloud.base.boot.entity.ManagerRoleEntity;
import com.alinesno.cloud.base.boot.entity.ManagerRoleResourceEntity;
import com.alinesno.cloud.base.boot.service.IManagerResourceService;
import com.alinesno.cloud.base.boot.service.IManagerRoleResourceService;
import com.alinesno.cloud.base.boot.service.IManagerRoleService;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.core.page.DatatablesPageBean;
import com.alinesno.cloud.common.core.response.ResponseBean;
import com.alinesno.cloud.common.core.response.ResponseGenerator;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.controller.FeignMethodController;
import com.alinesno.cloud.common.web.base.form.FormToken;
import com.alinesno.cloud.common.web.login.aop.DataFilter;

/**
 * 部门管理 
 * @author LuoAnDong
 * @since 2019年3月24日 下午1:16:44
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
@RequestMapping("portal/ucenter/role")
public class RoleController extends FeignMethodController<ManagerRoleEntity , IManagerRoleService> {

	private static final Logger log = LoggerFactory.getLogger(RoleController.class);

	@Reference
	private IManagerRoleService managerRoleService ; 
	
	@Reference
	private IManagerResourceService managerResourceService ; 
	
	@Reference
	private IManagerRoleResourceService managerRoleResourceService ; 

	
	@Override
	public ResponseBean delete(String ids) {
		managerRoleService.deleteRoleByIds(ids) ; 
		return ResponseGenerator.ok(null);
	}

	/**
	 * 查询出菜单列表
	 * @param roleId
	 * @return 
	 */
	@ResponseBody
	@PostMapping("/menuData")
    public List<ManagerResourceEntity> menus(String roleId){
		RestWrapper restWrapper = new RestWrapper() ; 
		restWrapper.eq("roleId", roleId) ; 
		List<ManagerRoleResourceEntity> mrList = managerRoleResourceService.findAll(restWrapper) ; 
		
		for(ManagerRoleResourceEntity r : mrList) {
			log.debug("r = {}" , r);
		}
		
		if(mrList != null) {
			List<String> resourceIds = new ArrayList<String>() ; 
			for(ManagerRoleResourceEntity r : mrList) {
				resourceIds.add(r.getResourceId()) ; 
			}
			List<ManagerResourceEntity> list = managerResourceService.findAllById(resourceIds) ; 
			return list ; 
		}
		return null ; 
    }
	
	/**
	 * 查询所有列表
	 * @param roleId
	 * @return 
	 */
	@ResponseBody
	@PostMapping("/allRole")
    public List<ManagerRoleEntity> all(String roleId){
		return managerRoleService.findAll() ; 
    }
	
	/**
	 * 保存新对象
	 * 
	 * @param model
	 * @param ManagerRoleEntity
	 * @return
	 */
	@FormToken(remove = true)
	@ResponseBody
	@PostMapping("/saveFunctions")
	public ResponseBean saveFunctions(Model model, HttpServletRequest request, ManagerRoleEntity managerRoleEntity , String functionIds) {
		if(StringUtils.isBlank(functionIds)) {
			return ResponseGenerator.genFailMessage("请选择功能."); 
		}

		fillOperator(managerRoleEntity) ;
		
		boolean isSave = managerRoleService.saveRole(managerRoleEntity , functionIds);
		return ResponseGenerator.ok(isSave);
	}

	/**
	 * 代码管理查询功能
	 * 
	 * @return
	 */
	@FormToken(save = true)
	@GetMapping("/add")
	public void add(Model model, HttpServletRequest request) {
	}

	/**
	 * 代码管理查询功能
	 * 
	 * @return
	 */
//	@FormToken(save = true)
//	@GetMapping("/modify")
//	@Override
//	public void modify(HttpServletRequest request ,  Model model, String id) {
//		Assert.hasLength(id, "主键不能为空.");
//		ManagerRoleEntity code = IManagerRoleService.getOne(id);
//		model.addAttribute("bean", code);
//	}

	/**
	 * 删除
	 */
	@ResponseBody
	@PostMapping("/delete")
	public ResponseBean delete(@RequestParam(value = "rowsId[]") String[] rowsId) {
		log.debug("rowsId = {}", ToStringBuilder.reflectionToString(rowsId));
		if (rowsId != null && rowsId.length > 0) {
			managerRoleService.deleteByIds(rowsId);
		}
		return ResponseGenerator.ok(null);
	}

	@DataFilter
	@TranslateCode("[{hasStatus:has_status}]")
	@ResponseBody
	@RequestMapping("/datatables")
	public DatatablesPageBean datatables(HttpServletRequest request, Model model, DatatablesPageBean page) {
		log.debug("page = {}", ToStringBuilder.reflectionToString(page));
		return this.toPage(model, managerRoleService, page);
	}

//	private DatatablesPageBean toPage(Model model, IManagerRoleService IManagerRoleService, DatatablesPageBean page) {
//
//		RestWrapper restWrapper = new RestWrapper();
//		RestPage<ManagerRoleEntity> pageable = new RestPage<ManagerRoleEntity>(page.getStart() / page.getLength(),page.getLength());
//		restWrapper.setPageable(pageable);
//		restWrapper.builderCondition(page.getCondition());
//
//		RestPage<ManagerRoleEntity> pageableResult = IManagerRoleService.findAllByWrapperAndPageable(restWrapper);
//
//		DatatablesPageBean p = new DatatablesPageBean();
//		p.setData(pageableResult.getContent());
//		p.setDraw(page.getDraw());
//		p.setRecordsFiltered((int) pageableResult.getTotalElements());
//		p.setRecordsTotal((int) pageableResult.getTotalElements());
//
//		return p;
//	}
	
	/**
	 * 保存新对象
	 * 
	 * @param model
	 * @param ManagerRoleEntity
	 * @return
	 */
//	@FormToken(remove = true)
	@ResponseBody
	@PostMapping("/updateFunctions")
	public ResponseBean updateFunctions(Model model, HttpServletRequest request, ManagerRoleEntity ManagerRoleEntity , String functionIds) {
		return this.saveFunctions(model, request, ManagerRoleEntity, functionIds) ; 
	}

	@Override
	public IManagerRoleService getFeign() {
		return managerRoleService;
	}


}
