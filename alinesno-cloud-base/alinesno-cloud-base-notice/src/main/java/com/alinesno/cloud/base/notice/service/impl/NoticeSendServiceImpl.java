package com.alinesno.cloud.base.notice.service.impl;

import java.util.List;
import java.util.Map;

import javax.lang.exception.RpcServiceRuntimeException;

import org.apache.dubbo.config.annotation.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.alinesno.cloud.base.notice.dto.WechatMessageDto;
import com.alinesno.cloud.base.notice.entity.EmailSendEntity;
import com.alinesno.cloud.base.notice.entity.SmsSendEntity;
import com.alinesno.cloud.base.notice.euums.NoticeSendTypeEnum;
import com.alinesno.cloud.base.notice.service.INoticeSendService;
import com.alinesno.cloud.base.notice.strategy.EmailSendStrategy;
import com.alinesno.cloud.base.notice.wechat.IWechatNoticeService;
import com.alinesno.cloud.common.core.context.ApplicationContextProvider;

/**
 * 通知服务 
 * @author LuoAnDong
 * @since 2019年10月1日 下午9:44:27
 */
@Service
public class NoticeSendServiceImpl implements INoticeSendService {

	private static final Logger log = LoggerFactory.getLogger(NoticeSendServiceImpl.class);

	@Autowired
	private IWechatNoticeService wechatNoticeService ; 


	@Override
	public SmsSendEntity sendRealtimeSms(SmsSendEntity sms, NoticeSendTypeEnum noticeSendTypeEnum) {
		
		return null;
	}

	@Override
	public SmsSendEntity sendSms(SmsSendEntity sms, NoticeSendTypeEnum noticeSendTypeEnum) {
		
		return null;
	}

	@Override
	public SmsSendEntity sendTemplateSms(String templateName, SmsSendEntity sms, NoticeSendTypeEnum noticeSendTypeEnum) {
		
		return null;
	}

	@Override
	public SmsSendEntity sendBatchSms(List<String> sms, String subject, String htmlBody, NoticeSendTypeEnum noticeSendTypeEnum) {
		return null;
	}

	@Override
	public void sendWechatMessage(Map<String, Object> messageKey, WechatMessageDto message) {
		wechatNoticeService.sendMessage(messageKey , message) ;
	}

	@Override
	public EmailSendEntity sendTextEmail(EmailSendEntity email, NoticeSendTypeEnum noticeSendTypeEnum) {
		
		Assert.notNull(noticeSendTypeEnum , "邮件通知发送策略不能为空.");
		log.debug("发送邮件策略:{}" , noticeSendTypeEnum.value());
		
		EmailSendStrategy service ;
		
		try {
			service = (EmailSendStrategy) ApplicationContextProvider.getBean(noticeSendTypeEnum.value()+"SendService");
		} catch (Exception e) {
			e.printStackTrace();
			throw new RpcServiceRuntimeException("消息推送接口["+noticeSendTypeEnum.value()+"SendService]未实现:"+e.getMessage()) ; 
		} 
		
		return service.sendTextEmail(email).getBody() ; 
		
	}

	@Override
	public EmailSendEntity sendHtmlEmail(EmailSendEntity email, NoticeSendTypeEnum noticeSendTypeEnum) {
		
		return null;
	}

	@Override
	public EmailSendEntity sendTemplateEmail(Map<String, Object> maps, String templateName, EmailSendEntity email,
			NoticeSendTypeEnum noticeSendTypeEnum) {
		
		return null;
	}

	@Override
	public EmailSendEntity sendBatchTextEmail(List<EmailSendEntity> emails, String subject, String htmlBody,
			NoticeSendTypeEnum noticeSendTypeEnum) {
		
		return null;
	}

	@Override
	public EmailSendEntity sendBatchHtmlEmail(List<EmailSendEntity> emails, String subject, String htmlBody,
			NoticeSendTypeEnum noticeSendTypeEnum) {
		
		return null;
	}
	
	
}
