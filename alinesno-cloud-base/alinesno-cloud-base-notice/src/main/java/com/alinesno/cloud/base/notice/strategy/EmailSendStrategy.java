package com.alinesno.cloud.base.notice.strategy;

import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;

import com.alinesno.cloud.base.notice.entity.EmailSendEntity;

public interface EmailSendStrategy {

	/**
	 * 发送纯文本邮件
	 * 
	 * @param emailEntity
	 */
	public ResponseEntity<EmailSendEntity> sendTextEmail(EmailSendEntity email);

	/**
	 * 发送超文件邮件(HTML)
	 * 
	 * @param emailEntity
	 * @return
	 */
	public ResponseEntity<EmailSendEntity> sendHtmlEmail(EmailSendEntity email);

	/**
	 * 发送模板邮件
	 * 
	 * @param maps         属性值
	 * @param templateName 模板名称
	 * @param emailEntity  邮件信息
	 * @return
	 */
	public ResponseEntity<EmailSendEntity> sendTemplateEmail(Map<String, Object> maps, String templateName,
			EmailSendEntity email);

	/**
	 * 批量发送文本邮件
	 * 
	 * @param emails
	 * @return
	 */
	public ResponseEntity<EmailSendEntity> sendBatchTextEmail(List<EmailSendEntity> emails, String subject,
			String htmlBody);

	/**
	 * 批量发送Html邮件
	 * 
	 * @param emails
	 * @return
	 */
	public ResponseEntity<EmailSendEntity> sendBatchHtmlEmail(List<EmailSendEntity> emails, String subject,
			String htmlBody);
}
