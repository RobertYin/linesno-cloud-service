package com.alinesno.cloud.base.notice.strategy;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.dubbo.config.annotation.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;

import com.alinesno.cloud.base.notice.entity.SmsSendEntity;
import com.alinesno.cloud.base.notice.service.ISmsSendService;
import com.alinesno.cloud.common.core.service.impl.IBaseServiceImpl;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-02 16:08:00
 */
@Service
public class SmsAliyunnSendServiceImpl extends IBaseServiceImpl<SmsSendEntity, String> implements ISmsSendService {

	//日志记录
	private static final Logger log = LoggerFactory.getLogger(SmsAliyunnSendServiceImpl.class);
		
	// 产品名称:云通信短信API产品,开发者无需替换
	@Value("${aliyun.sms.product}")
	private String product ; 

	// 产品域名,开发者无需替换
	@Value("${aliyun.sms.domain}")
	private String domain ; 

	// TODO 此处需要替换成开发者自己的AK(在阿里云访问控制台寻找)
	@Value("${aliyun.sms.access-key}")
	private String accessKeyId ; 

	@Value("${aliyun.sms.access-key-secret}")
	private String accessKeySecret ; 

	@Value("${aliyun.sms.connection-timeout}")
	private String connectionTimeout ; 

	@Value("${aliyun.sms.read-timeout}")
	private String readTimeout ; 

	@SuppressWarnings("deprecation")
	@Override
	public ResponseEntity<SmsSendEntity> sendRealtimeSms(SmsSendEntity sms) {
		
		log.debug("sms request = {}", ToStringBuilder.reflectionToString(sms));
		try {
			// 可自助调整超时时间
			System.setProperty("sun.net.client.defaultConnectTimeout", connectionTimeout);
			System.setProperty("sun.net.client.defaultReadTimeout", readTimeout);

			// 初始化acsClient,暂不支持region化
			IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
			DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
			IAcsClient acsClient = new DefaultAcsClient(profile);

			// 组装请求对象-具体描述见控制台-文档部分内容
			SendSmsRequest request = new SendSmsRequest();

			// 必填:待发送手机号
			request.setPhoneNumbers(sms.getPhone());

			// 必填:短信签名-可在短信控制台中找到
			request.setSignName(sms.getSignName());

			// 必填:短信模板-可在短信控制台中找到
			request.setTemplateCode(sms.getTemplateCode());

			// 可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
			request.setTemplateParam(sms.getTemplate());

			// 选填-上行短信扩展码(无特殊需求用户请忽略此字段)
			// request.setSmsUpExtendCode("90997");

			// 可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
			request.setOutId(sms.getOutId());

			// hint 此处可能会抛出异常，注意catch
			SendSmsResponse sendSmsResponse = acsClient.getAcsResponse(request);

			log.debug("sendSmsResponse = {}", ToStringBuilder.reflectionToString(sendSmsResponse));

		} catch (Exception e) {
			log.debug("短信发送失败:{}" , e);
		}

		return null;
	}

	@Override
	public ResponseEntity<SmsSendEntity> sendSms(SmsSendEntity sms) {
		return null;
	}

	@Override
	public ResponseEntity<SmsSendEntity> sendTemplateSms(String templateName, SmsSendEntity sms) {
		return null;
	}

	@Override
	public ResponseEntity<SmsSendEntity> sendBatchSms(List<String> sms, String subject, String htmlBody) {
		
		
		return null;
	}

}
