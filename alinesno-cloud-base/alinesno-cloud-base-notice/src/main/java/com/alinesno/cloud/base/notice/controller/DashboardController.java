package com.alinesno.cloud.base.notice.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 控制层
 * @author LuoAnDong
 * @since 2018年11月27日 上午6:41:40
 */
//@Controller
//@Scope(SpringInstanceScope.PROTOTYPE)
public class DashboardController {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(DashboardController.class) ; 
	
//	@ResponseBody
//	@GetMapping("/dashboard/side")
//    public ResponseBean side(String resourceParent , String applicationId , HttpServletRequest request){
//		log.debug("resourceParant:{} , applicationId:{}" , resourceParent , applicationId);
////		return ResponseGenerator.ok(resources.getSubResource()) ; 
//		return ResponseGenerator.fail(null) ; 
//    }
//	
//	@RequestMapping("/dashboard")
//    public String dashboard(Model model , HttpServletRequest request){
//		log.debug("dashboard");
//	
//		model.addAttribute("applicationTitle", "短信服务管理") ; 
//		
//		return "dashboard/dashboard" ; 
//    }
//	
//	@RequestMapping("/dashboard/home")
//    public String home(){
//		log.debug("home");
//		return "dashboard/home" ;
	
}
