package com.alinesno.cloud.base.notice.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.junit.Test;

import com.alinesno.cloud.base.notice.entity.EmailSendEntity;
import com.alinesno.cloud.base.notice.service.IEmailSendService;
import com.alinesno.cloud.common.core.junit.JUnitBase;

public class Email163SendServiceImplTest extends JUnitBase {

	@Resource
	private IEmailSendService emailSendService ; 
	
	@Test
	public void testSendTextEmail() {
		log.debug("send text email");

		EmailSendEntity email = new EmailSendEntity() ; 

		email.setEmailSubject("测试邮件.");
		email.setEmailReceiver("landonniao@163.com"); 
		email.setEmailContent("this is a test from alinesno boot!");
		
		emailSendService.sendTextEmail(email) ; 
	}

	@Test
	public void testSendHtmlEmail() {
		EmailSendEntity email = new EmailSendEntity() ; 

		email.setEmailSubject("测试邮件.");
		email.setEmailReceiver("landonniao@163.com"); 
		email.setEmailContent("<h1>email test</h1><br/><b>this is a test html from alinesno boot!</b>");
		
		emailSendService.sendHtmlEmail(email) ; 
	}

	@Test
	public void testSendTemplateEmail() {
		EmailSendEntity email = new EmailSendEntity() ; 
		
		email.setEmailSubject("测试邮件.");
		email.setEmailReceiver("landonniao@163.com"); 
		email.setEmailContent("<h1>email test</h1><br/><b>this is a test html from alinesno boot!</b>");
		
		Map<String, Object> params = new HashMap<>();
		params.put("code", "231231");
		
		String templateName = "hello-email" ; 
		
		
		emailSendService.sendTemplateEmail(params, templateName, email) ; 
	}

}
