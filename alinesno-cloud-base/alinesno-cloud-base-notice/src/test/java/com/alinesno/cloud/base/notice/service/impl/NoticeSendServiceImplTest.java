package com.alinesno.cloud.base.notice.service.impl;

import static org.junit.Assert.fail;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alinesno.cloud.base.notice.entity.EmailSendEntity;
import com.alinesno.cloud.base.notice.euums.NoticeSendTypeEnum;
import com.alinesno.cloud.base.notice.service.INoticeSendService;
import com.alinesno.cloud.common.core.junit.JUnitBase;

public class NoticeSendServiceImplTest extends JUnitBase {

	@Autowired
	private INoticeSendService noticeSendServiceImpl ; 
	
	@Test
	public void testSendRealtimeSms() {
		fail("Not yet implemented");
	}

	@Test
	public void testSendSms() {
		fail("Not yet implemented");
	}

	@Test
	public void testSendTemplateSms() {
		fail("Not yet implemented");
	}

	@Test
	public void testSendBatchSms() {
		fail("Not yet implemented");
	}

	@Test
	public void testSendWechatMessage() {
		fail("Not yet implemented");
	}

	@Test
	public void testSendTextEmail() {
	
		EmailSendEntity email = new EmailSendEntity() ; 
		
		email.setEmailSubject("测试邮件.");
		email.setEmailReceiver("landonniao@163.com"); 
		email.setEmailContent("this is a test from alinesno boot!");
		
		email = noticeSendServiceImpl.sendTextEmail(email, NoticeSendTypeEnum.NOTICE_EMAIL_163) ; 
		
		Assert.assertNotNull(email);
	}

	@Test
	public void testSendHtmlEmail() {
		fail("Not yet implemented");
	}

	@Test
	public void testSendTemplateEmail() {
		fail("Not yet implemented");
	}

	@Test
	public void testSendBatchTextEmail() {
		fail("Not yet implemented");
	}

	@Test
	public void testSendBatchHtmlEmail() {
		fail("Not yet implemented");
	}

}
