package com.alinesno.cloud.base.logger;

import com.alinesno.cloud.base.logger.service.ILogRecordService;
import com.alinesno.cloud.common.core.auto.EnableAlinesnoCommonCore;
import com.alinesno.cloud.common.core.context.ApplicationContextProvider;
import org.apache.dubbo.config.spring.context.annotation.DubboComponentScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * 启动入口
 * 
 * @author LuoAnDong 
 * @since 2018-12-16 18:12:76
 */
@EnableHystrixDashboard
@EnableCircuitBreaker
@EnableHystrix // 熔断器
@SpringBootApplication
@EnableAsync // 开启异步任务
@DubboComponentScan
@EnableAlinesnoCommonCore
public class AlinesnoApplication {
	
	protected static final Logger log = LoggerFactory.getLogger(AlinesnoApplication.class) ; 

	public static void main(String[] args) throws InterruptedException {
		SpringApplication.run(AlinesnoApplication.class, args);
		
		// 启动 Provider 容器，注意这里的 Main 是 com.alibaba.dubbo.container 包下的
        // Main.main(args);
		
		ILogRecordService logRecordService = ApplicationContextProvider.getBean(ILogRecordService.class) ; 
		String name = null ; 
		
		for(int i = 0 ; i < 2000 ; i ++ ) {
			String hello = logRecordService.helloHystrix(name) ; 
			log.debug(hello);	
			Thread.sleep(500);
		}
		
	}

}
