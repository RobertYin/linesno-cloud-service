package com.alinesno.cloud.base.print.entity;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-05-03 14:07:38
 */
@Entity
@Table(name="template")
public class TemplateEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 模板编号
     */
    private String printCode ; 
    
    /**
     * 打印模板
     */
    @Lob
	private String printTemplate;
    /**
     * 打印模板名称
     */
	private String printTemplateName;
    /**
     * 打印模板空间
     */
	private String printNamespace;
    /**
     * 使用次数
     */
	private Integer useCount = 0 ;
    /**
     * 编辑状态
     */
	private String editStatus;


	public String getPrintCode() {
		return printCode;
	}

	public void setPrintCode(String printCode) {
		this.printCode = printCode;
	}

	public String getPrintTemplate() {
		return printTemplate;
	}

	public void setPrintTemplate(String printTemplate) {
		this.printTemplate = printTemplate;
	}

	public String getPrintTemplateName() {
		return printTemplateName;
	}

	public void setPrintTemplateName(String printTemplateName) {
		this.printTemplateName = printTemplateName;
	}

	public String getPrintNamespace() {
		return printNamespace;
	}

	public void setPrintNamespace(String printNamespace) {
		this.printNamespace = printNamespace;
	}

	public Integer getUseCount() {
		return useCount;
	}

	public void setUseCount(Integer useCount) {
		this.useCount = useCount;
	}

	public String getEditStatus() {
		return editStatus;
	}

	public void setEditStatus(String editStatus) {
		this.editStatus = editStatus;
	}


	@Override
	public String toString() {
		return "TemplateEntity{" +
			"printTemplate=" + printTemplate +
			", printTemplateName=" + printTemplateName +
			", printNamespace=" + printNamespace +
			", useCount=" + useCount +
			", editStatus=" + editStatus +
			"}";
	}
}
