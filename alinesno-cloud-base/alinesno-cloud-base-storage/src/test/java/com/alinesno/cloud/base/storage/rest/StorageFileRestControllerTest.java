package com.alinesno.cloud.base.storage.rest;

import com.alinesno.cloud.base.storage.enums.StrategyEnums;
import com.alinesno.cloud.common.core.auto.EnableAlinesnoCommonCore;
import com.alinesno.cloud.common.core.junit.JUnitBase;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.File;
import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;

import static org.junit.Assert.fail;

/**
 * 测试接口
 * 
 * @author LuoAnDong
 * @since 2019年4月10日 上午6:40:03
 */
@EnableAlinesnoCommonCore
public class StorageFileRestControllerTest extends JUnitBase {

	@Autowired
	private WebApplicationContext wac;

	private MockMvc mockMvc;

	/**
	 * 在每次测试执行前构建mvc环境
	 */
	@Before
	public void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	@Test
	public void testUploadFile() throws UnsupportedEncodingException, Exception {
		File file = new File("/Users/luodong/Desktop/demo_05.jpg");
        MockMultipartFile firstFile = new MockMultipartFile("file", "demo_05.jpg", MediaType.TEXT_PLAIN_VALUE, new FileInputStream(file));

		String result = mockMvc.perform(MockMvcRequestBuilders
					.multipart("/storageFile/uploadFile")
					.file(firstFile)
					.param("username", "lisi")
					.param("strategy", StrategyEnums.QINIU.value()))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();
		
		log.debug("result:{}" , result);
	}

	@Test
	public void testDownloadData() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteData() {
		fail("Not yet implemented");
	}

}
