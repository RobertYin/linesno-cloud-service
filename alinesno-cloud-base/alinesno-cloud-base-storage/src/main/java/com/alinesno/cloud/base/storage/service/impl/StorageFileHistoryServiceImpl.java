package com.alinesno.cloud.base.storage.service.impl;

import com.alinesno.cloud.base.storage.entity.StorageFileHistoryEntity;
import com.alinesno.cloud.base.storage.service.IStorageFileHistoryService;
import com.alinesno.cloud.common.core.service.impl.IBaseServiceImpl;
import org.apache.dubbo.config.annotation.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2019-04-09 20:22:17
 */
@Service
public class StorageFileHistoryServiceImpl extends IBaseServiceImpl< StorageFileHistoryEntity, String> implements IStorageFileHistoryService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(StorageFileHistoryServiceImpl.class);

}
