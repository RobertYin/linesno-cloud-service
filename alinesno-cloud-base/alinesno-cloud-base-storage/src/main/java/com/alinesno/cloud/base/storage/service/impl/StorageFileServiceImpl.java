package com.alinesno.cloud.base.storage.service.impl;

import com.alinesno.cloud.base.storage.entity.StorageFileEntity;
import com.alinesno.cloud.base.storage.service.IStorageFileService;
import com.alinesno.cloud.common.core.service.impl.IBaseServiceImpl;
import org.apache.dubbo.config.annotation.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2019-04-09 20:22:17
 */
@Service
public class StorageFileServiceImpl extends IBaseServiceImpl< StorageFileEntity, String> implements IStorageFileService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(StorageFileServiceImpl.class);

	@Override
	public StorageFileEntity uploadData(String localFile) {
		return null;
	}

}
