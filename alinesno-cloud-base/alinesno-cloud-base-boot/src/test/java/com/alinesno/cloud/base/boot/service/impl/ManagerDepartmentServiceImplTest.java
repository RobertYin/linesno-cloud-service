package com.alinesno.cloud.base.boot.service.impl;

import java.util.Optional;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alinesno.cloud.base.boot.entity.ManagerDepartmentEntity;
import com.alinesno.cloud.base.boot.service.IManagerDepartmentService;
import com.alinesno.cloud.common.core.junit.JUnitBase;

public class ManagerDepartmentServiceImplTest  extends JUnitBase {
	
	@Autowired
	private IManagerDepartmentService managerDepartmentService ; 
	
	@Test
	public void testFindAllWithApplication() {
	}

	@Test
	public void testFindByIdString() {
		String id = "564760002510716928"; 
		Optional<ManagerDepartmentEntity> e = managerDepartmentService.findById(id) ; 
		
		log.debug("ManagerDepartmentEntity:{}" , e.get());
	}

}
