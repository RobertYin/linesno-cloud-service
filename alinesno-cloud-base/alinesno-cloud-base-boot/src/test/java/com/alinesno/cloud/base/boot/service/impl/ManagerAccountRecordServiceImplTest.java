package com.alinesno.cloud.base.boot.service.impl;

import static org.junit.Assert.fail;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import com.alinesno.cloud.base.boot.entity.ManagerAccountRecordEntity;
import com.alinesno.cloud.base.boot.service.IManagerAccountRecordService;
import com.alinesno.cloud.common.core.junit.JUnitBase;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;

public class ManagerAccountRecordServiceImplTest extends JUnitBase {

	@Autowired
	private IManagerAccountRecordService managerAccountRecordService ; 
	
	@Test
	public void testFindAll() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllSort() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllById() {
		fail("Not yet implemented");
	}

	@Test
	public void testSaveAll() {
		fail("Not yet implemented");
	}

	@Test
	public void testFlush() {
		fail("Not yet implemented");
	}

	@Test
	public void testSaveAndFlush() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteInBatch() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteAllInBatch() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetOne() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllExampleOfS() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllExampleOfSSort() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllPageable() {
		fail("Not yet implemented");
	}

	@Test
	public void testSave() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindById() {
		fail("Not yet implemented");
	}

	@Test
	public void testExistsById() {
		fail("Not yet implemented");
	}

	@Test
	public void testCount() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteById() {
		fail("Not yet implemented");
	}

	@Test
	public void testDelete() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteAllIterableOfQextendsEntity() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteAll() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindOneExampleOfS() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllExampleOfSPageable() {
		fail("Not yet implemented");
	}

	@Test
	public void testCountExampleOfS() {
		fail("Not yet implemented");
	}

	@Test
	public void testExists() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindOneSpecificationOfEntity() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllSpecificationOfEntity() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllSpecificationOfEntityPageable() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllSpecificationOfEntitySort() {
		fail("Not yet implemented");
	}

	@Test
	public void testCountSpecificationOfEntity() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteByIds() {
		fail("Not yet implemented");
	}

	@Test
	public void testModifyHasStatus() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllByApplicationId() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllByTenantId() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllByTenantIdAndApplicationId() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllRestWrapper() {
		fail("Not yet implemented");
	}

	@SuppressWarnings("unused")
	@Test
	public void testFindAllByWrapperAndPageable() {
		RestWrapper rest = new RestWrapper(); 
		rest.setPageNumber(0);
		rest.setPageSize(10);
		
		rest.eq("applicationId", "") ; 
		
		Page<ManagerAccountRecordEntity> page = managerAccountRecordService.findAllByWrapperAndPageable(rest) ; 
	}

}
