package com.alinesno.cloud.base.boot.service.impl;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alinesno.cloud.base.boot.service.IManagerSourceGenerateService;
import com.alinesno.cloud.common.core.junit.JUnitBase;

/**
 * 代码生成测试
 * @author LuoAnDong
 * @since 2019年9月15日 下午1:18:23
 */
public class ManagerSourceGenerateServiceImplTest extends JUnitBase {

	private String id  = "573942777474187264"; 
	
	@Autowired
	private IManagerSourceGenerateService managerSourceGenerateServiceImpl ; 
	
	@Test
	public void testGeneratorCode() throws IOException {
		byte[] result = managerSourceGenerateServiceImpl.generatorCode(id) ; 
		log.debug("result:{}" , result);
		
		Assert.assertNotNull(result);
		FileUtils.writeByteArrayToFile(new File("/Users/luodong/Desktop/"+System.currentTimeMillis()+".zip"), result);
	}

	@Test
	public void testGit() throws IOException {
		String gitPath =  managerSourceGenerateServiceImpl.git(id) ; 
		log.debug("git path :{}" , gitPath);
	}

	@Test
	public void testJenkins() throws IOException, URISyntaxException {
		String jenkinsPath =  managerSourceGenerateServiceImpl.jenkins(id) ; 
		log.debug("jenkins path :{}" , jenkinsPath);
	}

}
