package com.alinesno.cloud.base.boot.service.impl;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alinesno.cloud.base.boot.service.IUserAccountService;
import com.alinesno.cloud.common.core.junit.JUnitBase;

public class UserAccountServiceImplTest extends JUnitBase {

	@Autowired
	private IUserAccountService userAccountServiceImpl ; 
	
	@Test
	public void testRegistUser() {
		String phone = "15578942583" ; 
		String password = "admin" ; 
		String phoneCode = "1234" ; 
		
		boolean b = userAccountServiceImpl.registUser(phone, password, phoneCode) ; 
		Assert.assertTrue(b);
	}

}
