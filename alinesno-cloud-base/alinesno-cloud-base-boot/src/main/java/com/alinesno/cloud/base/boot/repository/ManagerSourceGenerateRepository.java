package com.alinesno.cloud.base.boot.repository;

import com.alinesno.cloud.base.boot.entity.ManagerSourceGenerateEntity;
import com.alinesno.cloud.common.facade.orm.repository.IBaseJpaRepository;

/**
 * <p>
  *  持久层接口
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-04-17 07:44:37
 */
public interface ManagerSourceGenerateRepository extends IBaseJpaRepository<ManagerSourceGenerateEntity, String> {

}
