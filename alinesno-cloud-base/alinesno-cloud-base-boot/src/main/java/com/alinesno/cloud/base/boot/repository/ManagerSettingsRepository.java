package com.alinesno.cloud.base.boot.repository;

import com.alinesno.cloud.base.boot.entity.ManagerSettingsEntity;
import com.alinesno.cloud.common.facade.orm.repository.IBaseJpaRepository;

/**
 * <p>
  * 参数配置表 持久层接口
 * </p>
 *
 * @author WeiXiaoJin
 * @since 2019-07-06 15:47:49
 */
public interface ManagerSettingsRepository extends IBaseJpaRepository<ManagerSettingsEntity, String> {

}
