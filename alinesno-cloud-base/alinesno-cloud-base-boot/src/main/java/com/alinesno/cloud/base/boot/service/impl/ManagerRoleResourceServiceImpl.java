package com.alinesno.cloud.base.boot.service.impl;

import org.apache.dubbo.config.annotation.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.alinesno.cloud.base.boot.entity.ManagerRoleResourceEntity;
import com.alinesno.cloud.base.boot.repository.ManagerRoleResourceRepository;
import com.alinesno.cloud.base.boot.service.IManagerRoleResourceService;
import com.alinesno.cloud.common.core.service.impl.IBaseServiceImpl;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Service
public class ManagerRoleResourceServiceImpl extends IBaseServiceImpl< ManagerRoleResourceEntity, String> implements IManagerRoleResourceService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(ManagerRoleResourceServiceImpl.class);

	@Autowired
	private ManagerRoleResourceRepository managerRoleResourceRepository ; 
	
	@Override
	public void deleteByRoleId(String roleId) {
		managerRoleResourceRepository.deleteByRoleId(roleId) ; 
	}

}
