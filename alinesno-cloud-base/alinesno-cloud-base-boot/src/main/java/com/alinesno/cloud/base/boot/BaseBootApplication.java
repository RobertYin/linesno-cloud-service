package com.alinesno.cloud.base.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableAsync;

import com.alinesno.cloud.common.core.auto.EnableAlinesnoCommonCore;
import com.alinesno.cloud.common.core.auto.EnableAlinesnoDubboScan;

/**
 * 启动入口
 * @EnableSwagger2 //开启swagger2
 * @author LuoAnDong
 * @since 2018-12-16 17:12:901
 */
@EnableJpaAuditing
@EnableAsync
@SpringBootApplication
@EnableAlinesnoCommonCore
@EnableAlinesnoDubboScan
@EnableCaching
public class BaseBootApplication {

	public static void main(String[] args) {
		 SpringApplication.run(BaseBootApplication.class, args);
		
		// 启动 Provider 容器，注意这里的 Main 是 com.alibaba.dubbo.container 包下的
        // Main.main(args);
	}

}
