package com.alinesno.cloud.base.boot.repository;

import com.alinesno.cloud.base.boot.entity.ManagerTenantLogEntity;
import com.alinesno.cloud.common.facade.orm.repository.IBaseJpaRepository;

/**
 * <p>
  *  持久层接口
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
public interface ManagerTenantLogRepository extends IBaseJpaRepository<ManagerTenantLogEntity, String> {

}
