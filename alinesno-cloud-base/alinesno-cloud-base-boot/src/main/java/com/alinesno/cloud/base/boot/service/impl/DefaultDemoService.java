package com.alinesno.cloud.base.boot.service.impl;

import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Value;

import com.alinesno.cloud.base.boot.service.DemoService;

@Service
public class DefaultDemoService implements DemoService {

	/**
	 * The default value of ${dubbo.application.name} is ${spring.application.name}
	 */
	@Value("${dubbo.application.name}")
	private String serviceName;

	public String sayHello(String name) {
		return String.format("[%s] : Hello, %s", serviceName, name);
	}
}