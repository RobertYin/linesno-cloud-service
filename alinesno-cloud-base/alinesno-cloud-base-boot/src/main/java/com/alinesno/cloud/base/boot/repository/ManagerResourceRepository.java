package com.alinesno.cloud.base.boot.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.alinesno.cloud.base.boot.entity.ManagerResourceEntity;
import com.alinesno.cloud.common.facade.orm.repository.IBaseJpaRepository;

/**
 * <p>
  *  持久层接口
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
public interface ManagerResourceRepository extends IBaseJpaRepository<ManagerResourceEntity, String> {

	/**
	 * 菜单查询 
	 * @param resourceParent
	 * @return
	 */
	List<ManagerResourceEntity> findAllByResourceParent(String resourceParent);

	/**
	 * 菜单查询 
	 * @param resourceParent
	 * @return
	 */
	Optional<ManagerResourceEntity> findByResourceParentAndApplicationId(String responseParent , String applicationId);

	/**
	 * 查询账户在某个应用下的权限
	 * @param applicationId
	 * @param accountId
	 * @return
	 */
	@Query("select t2 from ManagerRoleResourceEntity t1 left join ManagerResourceEntity t2 on t1.resourceId=t2.id where t1.roleId in(select roleId from ManagerAccountRoleEntity t3 where t3.accountId=:accountId) and t2.applicationId=:applicationId ")
	List<ManagerResourceEntity> findAllByApplicationAndAccount(@Param("applicationId") String applicationId, @Param("accountId") String accountId);

	/**
	 * 通过id删除
	 * @param id
	 */
	void deleteByApplicationId(String applicationId);

}
