package com.alinesno.cloud.base.boot.service.impl;

import org.apache.dubbo.config.annotation.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.alinesno.cloud.base.boot.constants.RolePowerEnums;
import com.alinesno.cloud.base.boot.entity.ManagerAccountEntity;
import com.alinesno.cloud.base.boot.entity.ManagerSettingsEntity;
import com.alinesno.cloud.base.boot.repository.ManagerAccountRepository;
import com.alinesno.cloud.base.boot.service.IManagerAccountService;
import com.alinesno.cloud.base.boot.service.IManagerRoleService;
import com.alinesno.cloud.base.boot.service.IManagerSettingsService;
import com.alinesno.cloud.common.core.service.impl.IBaseServiceImpl;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Service
public class ManagerAccountServiceImpl extends IBaseServiceImpl< ManagerAccountEntity, String> implements IManagerAccountService {

	//日志记录
	private static final Logger log = LoggerFactory.getLogger(ManagerAccountServiceImpl.class);

	@Autowired
	private ManagerAccountRepository managerAccountRepository ; 
	
	@Autowired
	private IManagerSettingsService managerSettingsService; 
	
	@Autowired
	private IManagerRoleService managerRoleService ; 

	@Override
	public ManagerAccountEntity findByLoginName(String loginName) {
		log.debug("login name:{}" , loginName);
		return managerAccountRepository.findByLoginName(loginName) ; 
	}

	@Override
	public boolean resetPassword(String userId , String newPassword, String oldPassword) {
		
		Assert.hasLength(newPassword , "用户新密码不能为空.");
		Assert.hasLength(oldPassword, "用户旧密码不能为空.");	

//		@Modifying
//		@Query("UPDATE EventEntity e SET e.notified = ?2 WHERE e.id = ?1")
//		@Transactional
//		void customUpdate(UUID itemId, boolean notified);
	
		managerAccountRepository.updateManagerAccountPassword(newPassword , userId) ; 
		return true ;
	}

	@Override
	public boolean registAccount(String loginName, String password, String phoneCode) {
		
		ManagerAccountEntity a = this.findByLoginName(loginName) ; 
		if(a != null) {
			Assert.isTrue(false , "登陆名【"+loginName+"】已存在.");
		}
		
		// 获取默认值 
		ManagerSettingsEntity settings = managerSettingsService.queryKey("sys.regist.default.application", null); 
		ManagerSettingsEntity role = managerSettingsService.queryKey("sys.regist.default.role", null); 
		ManagerSettingsEntity name = managerSettingsService.queryKey("sys.regist.default.name", null); 
		
		Assert.notNull(settings,"默认应用不能为空.");
		Assert.notNull(role,"默认角色不能为空.");
		Assert.notNull(name,"默认用户名不能为空.");
		
		ManagerAccountEntity e = new ManagerAccountEntity() ; 
		
		e.setApplicationId(settings.getConfigValue());
		e.setRoleId(role.getConfigValue()); 
	
		e.setName(name.getConfigValue());
		
		e.setLoginName(loginName); 
		e.setPassword(password);
		e.setPassword(password);
		e.setRolePower(RolePowerEnums.TENANT.getCode());

		e = jpa.save(e) ; 
		
		// 用户授权
		managerRoleService.authAccount(e, role.getConfigValue()) ; 
		
		return e==null?false:true ;
	}

	
	
}
