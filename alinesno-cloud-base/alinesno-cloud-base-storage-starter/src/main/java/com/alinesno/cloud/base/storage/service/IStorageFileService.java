package com.alinesno.cloud.base.storage.service;

import com.alinesno.cloud.base.storage.entity.StorageFileEntity;
import com.alinesno.cloud.common.facade.services.IBaseService;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2019-04-09 20:22:17
 */
@NoRepositoryBean
public interface IStorageFileService extends IBaseService<StorageFileEntity, String> {

	StorageFileEntity uploadData(String localFile);

}
