package com.alinesno.cloud.base.storage.service;

import com.alinesno.cloud.base.storage.entity.StorageFileEntity;

/**
 * 数据存储接口
 * @author LuoAnDong
 * @since 2018年2月23日 下午2:33:45
 */
public interface StorageService {

	/**
	 * 文件上传
	 * @param fileLoalAbcPath 文件本地绝对路径
	 * @return
	 */
	public String uploadData(String fileLoalAbcPath , String bucket) throws Exception;

	/**
	 * 获取文件下载链接
	 * @param fileId 文件保存id
	 * @return
	 */
	public String downloadData(String fileId , String bucket);
	
	/**
	 * 获取访问链接
	 * @param fileId
	 * @param bucket
	 * @return
	 */
	public String presignedUrl(String filePath , String bucket);

	/**
	 * 删除文件
	 * @param fileId
	 * @return
	 */
	public boolean deleteData(String fileId , String bucket);

	/**
	 * 上传文件
	 * @param fileLoalAbcPath
	 * @param fileName
	 * @return
	 */
	public StorageFileEntity uploadData(String fileLoalAbcPath, String fileName , String bucket) throws Exception;

}