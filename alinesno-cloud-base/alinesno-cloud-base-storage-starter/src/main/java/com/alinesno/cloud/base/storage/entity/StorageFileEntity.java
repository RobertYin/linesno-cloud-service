package com.alinesno.cloud.base.storage.entity;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-06-08 09:19:40
 */
@Entity
@Table(name="storage_file")
public class StorageFileEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

	@Column(name="dfs_group_name")
	private String dfsGroupName;
	@Column(name="download_num")
	private Integer downloadNum;
	@Column(name="download_pwd")
	private String downloadPwd;
	@Column(name="expiration_date")
	private Date expirationDate;
    /**
     * 文件扩展名
     */
	@Column(name="file_ext")
	private String fileExt;
    /**
     * 文件标识
     */
	@Column(name="file_flag")
	private String fileFlag;
    /**
     * 文件名称
     */
	@Column(name="file_name")
	private String fileName;
    /**
     * 文件长度
     */
	@Column(name="file_size")
	private BigDecimal fileSize;
    /**
     * 地址链接
     */
	@Column(name="file_url")
	private String fileUrl;
    /**
     * 文件源
     */
	private String filesource;
    /**
     * 是否公开
     */
	@Column(name="is_public")
	private String isPublic;
    /**
     * 保存类型
     */
	@Column(name="save_type")
	private String saveType;
    /**
     * 阿里云链接
     */
	@Column(name="url_alioss")
	private String urlAlioss;
    /**
     * 本地磁盘链接
     */
	@Column(name="url_disk")
	private String urlDisk;
    /**
     * fastdfs链接
     */
	@Column(name="url_fastdfs")
	private String urlFastdfs;
	@Column(name="url_paxossurlre")
	private String urlPaxossurlre;
    /**
     * 七牛链接
     */
	@Column(name="url_qiniu")
	private String urlQiniu;
	private String urlbfs;
	private String urlmongodb;


	public String getDfsGroupName() {
		return dfsGroupName;
	}

	public void setDfsGroupName(String dfsGroupName) {
		this.dfsGroupName = dfsGroupName;
	}

	public Integer getDownloadNum() {
		return downloadNum;
	}

	public void setDownloadNum(Integer downloadNum) {
		this.downloadNum = downloadNum;
	}

	public String getDownloadPwd() {
		return downloadPwd;
	}

	public void setDownloadPwd(String downloadPwd) {
		this.downloadPwd = downloadPwd;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getFileExt() {
		return fileExt;
	}

	public void setFileExt(String fileExt) {
		this.fileExt = fileExt;
	}

	public String getFileFlag() {
		return fileFlag;
	}

	public void setFileFlag(String fileFlag) {
		this.fileFlag = fileFlag;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public BigDecimal getFileSize() {
		return fileSize;
	}

	public void setFileSize(BigDecimal fileSize) {
		this.fileSize = fileSize;
	}

	public String getFileUrl() {
		return fileUrl;
	}

	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}

	public String getFilesource() {
		return filesource;
	}

	public void setFilesource(String filesource) {
		this.filesource = filesource;
	}

	public String getIsPublic() {
		return isPublic;
	}

	public void setIsPublic(String isPublic) {
		this.isPublic = isPublic;
	}

	public String getSaveType() {
		return saveType;
	}

	public void setSaveType(String saveType) {
		this.saveType = saveType;
	}

	public String getUrlAlioss() {
		return urlAlioss;
	}

	public void setUrlAlioss(String urlAlioss) {
		this.urlAlioss = urlAlioss;
	}

	public String getUrlDisk() {
		return urlDisk;
	}

	public void setUrlDisk(String urlDisk) {
		this.urlDisk = urlDisk;
	}

	public String getUrlFastdfs() {
		return urlFastdfs;
	}

	public void setUrlFastdfs(String urlFastdfs) {
		this.urlFastdfs = urlFastdfs;
	}

	public String getUrlPaxossurlre() {
		return urlPaxossurlre;
	}

	public void setUrlPaxossurlre(String urlPaxossurlre) {
		this.urlPaxossurlre = urlPaxossurlre;
	}

	public String getUrlQiniu() {
		return urlQiniu;
	}

	public void setUrlQiniu(String urlQiniu) {
		this.urlQiniu = urlQiniu;
	}

	public String getUrlbfs() {
		return urlbfs;
	}

	public void setUrlbfs(String urlbfs) {
		this.urlbfs = urlbfs;
	}

	public String getUrlmongodb() {
		return urlmongodb;
	}

	public void setUrlmongodb(String urlmongodb) {
		this.urlmongodb = urlmongodb;
	}


	@Override
	public String toString() {
		return "StorageFileEntity{" +
			"dfsGroupName=" + dfsGroupName +
			", downloadNum=" + downloadNum +
			", downloadPwd=" + downloadPwd +
			", expirationDate=" + expirationDate +
			", fileExt=" + fileExt +
			", fileFlag=" + fileFlag +
			", fileName=" + fileName +
			", fileSize=" + fileSize +
			", fileUrl=" + fileUrl +
			", filesource=" + filesource +
			", isPublic=" + isPublic +
			", saveType=" + saveType +
			", urlAlioss=" + urlAlioss +
			", urlDisk=" + urlDisk +
			", urlFastdfs=" + urlFastdfs +
			", urlPaxossurlre=" + urlPaxossurlre +
			", urlQiniu=" + urlQiniu +
			", urlbfs=" + urlbfs +
			", urlmongodb=" + urlmongodb +
			"}";
	}
}
