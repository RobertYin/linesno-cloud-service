package com.alinesno.cloud.base.notice;

import java.util.Map;

import org.springframework.core.type.AnnotationMetadata;

import com.alinesno.cloud.common.core.auto.CustomAutoConfigurationImportSelector;

/**
 * 自动引入dubbo服务实现
 * 
 * @author LuoAnDong
 * @sine 2019年4月5日 下午3:34:07
 */
public class NoticeLocalConfigurationSelector extends CustomAutoConfigurationImportSelector {

	@Override
	public String[] selectImports(AnnotationMetadata importingClassMetadata) {
		return this.scanComponent() ; 
	}

	@Override
	protected Class<?> getAnnotationClass() {
		return EnableAlinesnoNoticeLocal.class;
	}

}
