package com.alinesno.cloud.base.notice.dto;

import com.alinesno.cloud.common.facade.dto.BaseDto;

/**
 * 微信消息实体
 * @author LuoAnDong
 * @since 2019年10月1日 下午9:38:58
 */
@SuppressWarnings("serial")
public class WechatMessageDto extends BaseDto {

	private String templateId ; // 模板
	private String openId ; // 用户id 
	private String remark ; // 备注信息
	private String url ; // 详情链接
	private String key ;  // 实体
	private String textContent ; // 内容
	private String textColor ; // 
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getOpenId() {
		return openId;
	}
	public void setOpenId(String openId) {
		this.openId = openId;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getTemplateId() {
		return templateId;
	}
	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getTextContent() {
		return textContent;
	}
	public void setTextContent(String textContent) {
		this.textContent = textContent;
	}
	public String getTextColor() {
		return textColor;
	}
	public void setTextColor(String textColor) {
		this.textColor = textColor;
	}
	
}
