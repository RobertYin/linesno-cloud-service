package com.alinesno.cloud.base.notice.service;

import java.util.List;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.http.ResponseEntity;

import com.alinesno.cloud.base.notice.entity.SmsSendEntity;
import com.alinesno.cloud.common.facade.services.IBaseService;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-02 16:08:00
 */
@NoRepositoryBean
public interface ISmsSendService extends IBaseService<SmsSendEntity, String> {

	/**
	 * 发送纯文本短信
	 * @param emailEntity
	 */
	public ResponseEntity<SmsSendEntity> sendRealtimeSms(SmsSendEntity sms) ; 
	
	/**
	 * 发送超文件短信(HTML)
	 * @param emailEntity
	 * @return
	 */
	public ResponseEntity<SmsSendEntity> sendSms(SmsSendEntity sms) ; 

	/**
	 * 发送模板短信
	 * @param maps 属性值 
	 * @param templateName 模板名称
	 * @param emailEntity 短信信息
	 * @return
	 */
	public ResponseEntity<SmsSendEntity> sendTemplateSms(String templateName , SmsSendEntity sms) ; 
	
	/**
	 * 批量发送Html短信
	 * @param emails
	 * @return
	 */
	public ResponseEntity<SmsSendEntity> sendBatchSms(List<String> sms, String subject , String htmlBody) ; 
	
	
}
