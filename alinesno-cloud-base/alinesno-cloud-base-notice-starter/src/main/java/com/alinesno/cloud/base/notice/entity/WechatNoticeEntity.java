package com.alinesno.cloud.base.notice.entity;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;

/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-01-23
 */
@SuppressWarnings("serial")
@Entity
@Table(name="run_wechat_template")
public class WechatNoticeEntity extends BaseEntity {
	 
	/**
	 * 模板内容 
	 */
	@Lob //大字段
	private String templateContent ; 
	
	@Lob //大字段
	private String errorMessage ; 
	
	/**
	 * 模板id
	 */
	private String templateId ; 
	
	/**
	 * 推送用户
	 */
	private String userId ; 
	
	/**
	 * 模板描述 
	 */
	private String templateDesc ;
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public WechatNoticeEntity() {
		super();
	}

	public WechatNoticeEntity(String templateContent, String templateId, String templateDesc) {
		super();
		this.templateContent = templateContent;
		this.templateId = templateId;
		this.templateDesc = templateDesc;
		this.setAddTime(new Timestamp(System.currentTimeMillis()));
	}

	public String getTemplateId() {
		return templateId;
	}

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	public String getTemplateContent() {
		return templateContent;
	}

	public void setTemplateContent(String templateContent) {
		this.templateContent = templateContent;
	}

	public String getTemplateDesc() {
		return templateDesc;
	}

	public void setTemplateDesc(String templateDesc) {
		this.templateDesc = templateDesc;
	} 


}
