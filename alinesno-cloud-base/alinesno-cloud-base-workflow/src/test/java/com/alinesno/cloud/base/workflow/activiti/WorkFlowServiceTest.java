package com.alinesno.cloud.base.workflow.activiti;

import com.alinesno.cloud.base.workflow.activiti.model.ActivitiModel;
import com.alinesno.cloud.common.core.junit.JUnitBase;
import org.activiti.engine.ProcessEngine;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.UnsupportedEncodingException;

import static org.junit.Assert.fail;

public class WorkFlowServiceTest extends JUnitBase {

	@Autowired
	private WorkFlowService workFlowService ; 
	
	@Test
	public void testCreateModel() throws UnsupportedEncodingException {

		ProcessEngine pe = workFlowService.getProcessEngine() ; 
		
		ActivitiModel model = new ActivitiModel() ; 
		model.setName("name_"+System.currentTimeMillis());
		model.setKey(model.getName());
		model.setDescription("测试环境:" + model.getName());
		
		String modelId = workFlowService.createModel(pe, model); 
		log.debug("modelId:{}" , modelId);
		
		Assert.assertNotNull(modelId);
	}

	@Test
	public void testDeploy() {
		String deploymentId = "40001" ; 
		workFlowService.deleteDeployment(deploymentId);
	}

	@Test
	public void testDeleteModel() {
		fail("Not yet implemented");
	}

	@Test
	public void testUpdateState() {
		fail("Not yet implemented");
	}

	@Test
	public void testConvertToModel() {
		fail("Not yet implemented");
	}

	@Test
	public void testResourceRead() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteDeployment() {
		fail("Not yet implemented");
	}

	@Test
	public void testStartProcess() {
		fail("Not yet implemented");
	}

	@Test
	public void testCallBackTask() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetVar() {
		fail("Not yet implemented");
	}

	@Test
	public void testBackProcess() {
		fail("Not yet implemented");
	}

	@Test
	public void testCallBackProcess() {
		fail("Not yet implemented");
	}

	@Test
	public void testEndProcess() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindBackAvtivity() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindProcessDefinitionEntityByTaskId() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindProcessInstanceByTaskId() {
		fail("Not yet implemented");
	}

	@Test
	public void testTransferAssignee() {
		fail("Not yet implemented");
	}

}
