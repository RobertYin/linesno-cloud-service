package com.alinesno.cloud.base.workflow.repository;

import com.alibaba.fastjson.JSONObject;
import com.alinesno.cloud.common.core.junit.JUnitBase;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.Map;

public class WorkflowBpsRepositoryTest extends JUnitBase {

	@Autowired
	private WorkflowBpsRepository workflowBpsRepository ; 
	
	@Test
	public void testFindActivitiInstance() {
		Page<Map<String, Object>> page = workflowBpsRepository.findActivitiInstance(PageRequest.of(0, 10)) ; 
	
		List<Map<String , Object>> list = page.getContent() ; 
		for(Map<String , Object> m : list) {
			log.debug("m:{}" , JSONObject.toJSON(m));
		}
		
	}

}
