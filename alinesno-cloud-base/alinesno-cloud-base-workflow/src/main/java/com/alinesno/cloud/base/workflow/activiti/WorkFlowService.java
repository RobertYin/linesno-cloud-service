package com.alinesno.cloud.base.workflow.activiti;

import com.alinesno.cloud.base.workflow.activiti.model.ActivitiModel;
import org.activiti.engine.*;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.repository.Model;
import org.activiti.engine.runtime.ProcessInstance;

import javax.xml.stream.XMLStreamException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

public interface WorkFlowService {

	/**
	 * 创建新模型
	 * 
	 * @throws UnsupportedEncodingException
	 */

	String createModel(ProcessEngine pe, ActivitiModel model) throws UnsupportedEncodingException;

	/***
	 * 部署模型
	 * 
	 * @param id
	 * @return
	 */

	String deploy(String id);

	/***
	 * 删除模型
	 */

	void deleteModel(String id);

	/***
	 * 挂起/激活
	 */

	String updateState(String state, String procDefId);

	/***
	 * 转化为模型
	 */

	Model convertToModel(String procDefId) throws UnsupportedEncodingException, XMLStreamException;

	/**
	 * 读取资源，通过部署ID
	 * 
	 * @param processDefinitionId 流程定义ID
	 * @param processInstanceId   流程实例ID
	 * @param resourceType        资源类型(xml|image)
	 */

	InputStream resourceRead(String procDefId, String proInsId, String resType) throws Exception;

	/***
	 * 删除正在运行的流程
	 */

	void deleteDeployment(String deploymentId);

	/***
	 * 启动流程,移动端，无法使用shiro---手机端使用
	 * 
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 */

	String startProcess(String id, Model m, String defKey, String title, Map<String, Object> var, String username);

	/***
	 * 流程取回
	 */

	void callBackTask(String taskId);

	/***
	 * 组织必要的流程变量
	 * 
	 * @param var
	 * @param username
	 * @return
	 * @throws ClassNotFoundException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */

	Map<String, Object> getVar(Model m, Map<String, Object> var, String id, String username, String defkey);

	/**
	 * 驳回流程
	 * 
	 * @param taskId     当前任务ID
	 * @param activityId 驳回节点ID
	 * @param variables  流程存储参数
	 * @throws Exception
	 */
	void backProcess(String taskId, String activityId, Map<String, Object> variables) throws Exception;

	/**
	 * 取回流程
	 * 
	 * @param taskId     当前任务ID
	 * @param activityId 取回节点ID
	 * @throws Exception
	 */
	void callBackProcess(String taskId, String activityId) throws Exception;

	/**
	 * 中止流程(特权人直接审批通过等)
	 * 
	 * @param taskId
	 */
	void endProcess(String taskId) throws Exception;

	/**
	 * 根据当前任务ID，查询可以驳回的任务节点
	 * 
	 * @param taskId 当前任务ID
	 */
	List<ActivityImpl> findBackAvtivity(String taskId) throws Exception;

	/**
	 * 根据任务ID获取流程定义
	 * 
	 * @param taskId 任务ID
	 * @return
	 * @throws Exception
	 */
	ProcessDefinitionEntity findProcessDefinitionEntityByTaskId(String taskId) throws Exception;

	/**
	 * 根据任务ID获取对应的流程实例
	 * 
	 * @param taskId 任务ID
	 * @return
	 * @throws Exception
	 */
	ProcessInstance findProcessInstanceByTaskId(String taskId) throws Exception;

	/**
	 * 转办流程
	 * 
	 * @param taskId   当前任务节点ID
	 * @param userCode 被转办人Code
	 */
	void transferAssignee(String taskId, String userCode);

	/**
	 * 创建模型
	 * @param model
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	String createModel(ActivitiModel model) throws UnsupportedEncodingException;

	/**
	 * 获取任务
	 * @return
	 */
	TaskService getTaskService();

	/**
	 * 获取存储服务
	 * @return
	 */
	RepositoryService getRepositoryService();

	/**
	 * 获取运行时任务
	 * @return
	 */
	RuntimeService getRuntimeService();

	/**
	 * 获取历史服务
	 * @return
	 */
	HistoryService getHistoryService();

	/**
	 * 获取引擎服务
	 * @return
	 */
	ProcessEngine getProcessEngine();

}