package com.alinesno.cloud.base.workflow.utils;

import cn.hutool.core.lang.UUID;
import org.activiti.engine.impl.cfg.IdGenerator;
 

/**
 * @author Lion
 * @date 2017年1月24日 下午12:02:35
 * @qq 439635374
 */
public class UuidGenerator implements IdGenerator {

	@Override
	public String getNextId() {
		return UUID.fastUUID().toString() ; //.getUUID();
	}

}
