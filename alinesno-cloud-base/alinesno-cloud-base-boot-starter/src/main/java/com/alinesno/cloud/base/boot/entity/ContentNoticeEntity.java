package com.alinesno.cloud.base.boot.entity;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;
import org.hibernate.annotations.Proxy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Proxy(lazy = false)
@Entity
@Table(name = "content_notice")
public class ContentNoticeEntity extends BaseEntity {

	private static final long serialVersionUID = 1L;

	/**
	 * 文章作者
	 */
	@Column(name = "notice_author")
	private String noticeAuthor;
	
	/**
	 * 文章日期
	 */
	@Column(name = "notice_date")
	private Date noticeDate;
	
	/**
	 * 文章内容
	 */
	@Column(name = "notice_content")
	private String noticeContent;
	
	/**
	 * 文章标题
	 */
	@Column(name = "notice_title")
	private String noticeTitle;
	
	/**
	 * 当前状态(1草稿/2已发布)
	 */
	@Column(name = "notice_status")
	private Integer noticeStatus;
	
	/**
	 * 文章访问密码
	 */
	@Column(name = "notice_password")
	private String noticePassword;
	
	/**
	 * 文章名称
	 */
	@Column(name = "notice_name")
	private String noticeName;
	
	/**
	 * 文章最后修改时间
	 */
	@Column(name = "notice_modifield")
	private Date noticeModifield;
	
	/**
	 * 文章类型
	 */
	@Column(name = "notice_type")
	private String noticeType;

	public String getNoticeAuthor() {
		return noticeAuthor;
	}

	public void setNoticeAuthor(String noticeAuthor) {
		this.noticeAuthor = noticeAuthor;
	}

	public Date getNoticeDate() {
		return noticeDate;
	}

	public void setNoticeDate(Date noticeDate) {
		this.noticeDate = noticeDate;
	}

	public String getNoticeContent() {
		return noticeContent;
	}

	public void setNoticeContent(String noticeContent) {
		this.noticeContent = noticeContent;
	}

	public String getNoticeTitle() {
		return noticeTitle;
	}

	public void setNoticeTitle(String noticeTitle) {
		this.noticeTitle = noticeTitle;
	}

	public Integer getNoticeStatus() {
		return noticeStatus;
	}

	public void setNoticeStatus(Integer noticeStatus) {
		this.noticeStatus = noticeStatus;
	}

	public String getNoticePassword() {
		return noticePassword;
	}

	public void setNoticePassword(String noticePassword) {
		this.noticePassword = noticePassword;
	}

	public String getNoticeName() {
		return noticeName;
	}

	public void setNoticeName(String noticeName) {
		this.noticeName = noticeName;
	}

	public Date getNoticeModifield() {
		return noticeModifield;
	}

	public void setNoticeModifield(Date noticeModifield) {
		this.noticeModifield = noticeModifield;
	}

	public String getNoticeType() {
		return noticeType;
	}

	public void setNoticeType(String noticeType) {
		this.noticeType = noticeType;
	}

}
