package com.alinesno.cloud.base.boot.entity;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;
import org.hibernate.annotations.Proxy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Proxy(lazy = false)
@Entity
@Table(name="manager_role_resource")
public class ManagerRoleResourceEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 角色id
     */
	@Column(name="role_id")
	private String roleId;
    /**
     * 资源id
     */
	@Column(name="resource_id")
	private String resourceId;
    /**
     * 资源类型(resource/action)
     */
	@Column(name="resource_type")
	private String resourceType;
    /**
     * 角色类型(role角色|tenant商户|account账户)
     */
	@Column(name="role_type")
	private String roleType;


	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	public String getRoleType() {
		return roleType;
	}

	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}


	@Override
	public String toString() {
		return "ManagerRoleResourceEntity{" +
			"roleId=" + roleId +
			", resourceId=" + resourceId +
			", resourceType=" + resourceType +
			", roleType=" + roleType +
			"}";
	}
}
