package com.alinesno.cloud.base.boot.entity;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.util.Date;

/**
 * 用户操作记录
 * @author LuoAnDong
 * @since 2019年4月8日 下午10:15:43
 */
@SuppressWarnings("serial")
@Proxy(lazy = false)
@Entity
@Table(name="manager_account_record")
public class ManagerAccountRecordEntity extends BaseEntity {

	private String operation;
	private long methodTime ; 
	private String method;
	
	@Lob
	@Basic(fetch=FetchType.LAZY)
	private String params;
	private String methodDesc ; 
	private String recordType ; //记录类型

	private String ip; //服务器ip
	private String url ;  //请求链接
	private String agent ; // 浏览器信息
	
	private Date createTime;
	private String accountId ;
	private String loginName ;
	private String accountName;
	private String rolePower ;
	
	public String getRecordType() {
		return recordType;
	}
	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public long getMethodTime() {
		return methodTime;
	}
	public void setMethodTime(long methodTime) {
		this.methodTime = methodTime;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getParams() {
		return params;
	}
	public void setParams(String params) {
		this.params = params;
	}
	public String getMethodDesc() {
		return methodDesc;
	}
	public void setMethodDesc(String methodDesc) {
		this.methodDesc = methodDesc;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getAgent() {
		return agent;
	}
	public void setAgent(String agent) {
		this.agent = agent;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public String getRolePower() {
		return rolePower;
	}
	public void setRolePower(String rolePower) {
		this.rolePower = rolePower;
	}

}
