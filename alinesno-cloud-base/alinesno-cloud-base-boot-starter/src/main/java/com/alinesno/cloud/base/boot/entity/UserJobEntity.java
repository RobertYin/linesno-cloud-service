package com.alinesno.cloud.base.boot.entity;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;
import org.hibernate.annotations.Proxy;

import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Proxy(lazy = false)
@Entity
@Table(name="user_job")
public class UserJobEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

	private String owners;


	public String getOwners() {
		return owners;
	}

	public void setOwners(String owners) {
		this.owners = owners;
	}


	@Override
	public String toString() {
		return "UserJobEntity{" +
			"owners=" + owners +
			"}";
	}
}
