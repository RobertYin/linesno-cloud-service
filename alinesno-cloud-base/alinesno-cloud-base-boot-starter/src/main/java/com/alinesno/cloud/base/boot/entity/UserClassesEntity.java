package com.alinesno.cloud.base.boot.entity;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;
import org.hibernate.annotations.Proxy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Proxy(lazy = false)
@Entity
@Table(name="user_classes")
public class UserClassesEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

	private String owners;
	private String classes;
	@Column(name="collge_name")
	private String collgeName;
	private String education;
	@Column(name="is_use")
	private String isUse;
	@Column(name="professional_name")
	private String professionalName;
	@Column(name="school_end_time")
	private Date schoolEndTime;
	@Column(name="school_id")
	private String schoolId;
	@Column(name="school_name")
	private String schoolName;
	@Column(name="school_num")
	private String schoolNum;
	@Column(name="school_start_time")
	private Date schoolStartTime;
	@Column(name="shool_job")
	private String shoolJob;
	@Column(name="use_end_time")
	private String useEndTime;
	@Column(name="use_start_time")
	private String useStartTime;
	@Column(name="user_id")
	private String userId;


	public String getOwners() {
		return owners;
	}

	public void setOwners(String owners) {
		this.owners = owners;
	}

	public String getClasses() {
		return classes;
	}

	public void setClasses(String classes) {
		this.classes = classes;
	}

	public String getCollgeName() {
		return collgeName;
	}

	public void setCollgeName(String collgeName) {
		this.collgeName = collgeName;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public String getIsUse() {
		return isUse;
	}

	public void setIsUse(String isUse) {
		this.isUse = isUse;
	}

	public String getProfessionalName() {
		return professionalName;
	}

	public void setProfessionalName(String professionalName) {
		this.professionalName = professionalName;
	}

	public Date getSchoolEndTime() {
		return schoolEndTime;
	}

	public void setSchoolEndTime(Date schoolEndTime) {
		this.schoolEndTime = schoolEndTime;
	}

	public String getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(String schoolId) {
		this.schoolId = schoolId;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getSchoolNum() {
		return schoolNum;
	}

	public void setSchoolNum(String schoolNum) {
		this.schoolNum = schoolNum;
	}

	public Date getSchoolStartTime() {
		return schoolStartTime;
	}

	public void setSchoolStartTime(Date schoolStartTime) {
		this.schoolStartTime = schoolStartTime;
	}

	public String getShoolJob() {
		return shoolJob;
	}

	public void setShoolJob(String shoolJob) {
		this.shoolJob = shoolJob;
	}

	public String getUseEndTime() {
		return useEndTime;
	}

	public void setUseEndTime(String useEndTime) {
		this.useEndTime = useEndTime;
	}

	public String getUseStartTime() {
		return useStartTime;
	}

	public void setUseStartTime(String useStartTime) {
		this.useStartTime = useStartTime;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}


	@Override
	public String toString() {
		return "UserClassesEntity{" +
			"owners=" + owners +
			", classes=" + classes +
			", collgeName=" + collgeName +
			", education=" + education +
			", isUse=" + isUse +
			", professionalName=" + professionalName +
			", schoolEndTime=" + schoolEndTime +
			", schoolId=" + schoolId +
			", schoolName=" + schoolName +
			", schoolNum=" + schoolNum +
			", schoolStartTime=" + schoolStartTime +
			", shoolJob=" + shoolJob +
			", useEndTime=" + useEndTime +
			", useStartTime=" + useStartTime +
			", userId=" + userId +
			"}";
	}
}
