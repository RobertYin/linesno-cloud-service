package com.alinesno.cloud.base.boot.entity;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;
import org.hibernate.annotations.Proxy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-04-17 07:44:37
 */
@Proxy(lazy = false)
@Entity
@Table(name = "manager_source_generate")
public class ManagerSourceGenerateEntity extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "author_name")
	private String authorName;
	
	@Column(name = "db_driver")
	private String dbDriver;
	
	@Column(name = "db_pwd")
	private String dbPwd;
	
	@Column(name = "db_url")
	private String dbUrl;
	
	@Column(name = "db_user")
	private String dbUser;
	
	@Column(name = "boot_prefix")
	private String bootPrefix;
	
	@Column(name = "feign_server_path")
	private String feignServerPath;
	
	@Column(name = "package_name")
	private String packageName;
	
	private String applicationName ; // 应用名称
	
	@Column(name="git_repository_url")
	private String gitRepositoryUrl;
	
	@Column(name="git_user_name")
	private String gitUserName;
	
	@Column(name="git_password")
	private String gitPassword;
	
	@Column(name="jenkins_jobname")
	private String jenkinsJobname; // 持续集成地址 
	
	@Column(name="jenkins_url")
	private String jenkinsUrl ; // 持续集成地址 
	
	@Column(name="jenkins_user_name")
	private String jenkinsUserName ;  // 用户名
	
	@Column(name="jenkins_password")
	private String jenkinsPassword ; // 密码
	
	private boolean serviceProject ; // 默认为单体应用
	
	private boolean dockerProject ;   // 服务容器化
	
	public String getJenkinsJobname() {
		return jenkinsJobname;
	}

	public void setJenkinsJobname(String jenkinsJobname) {
		this.jenkinsJobname = jenkinsJobname;
	}

	public String getJenkinsUrl() {
		return jenkinsUrl;
	}

	public void setJenkinsUrl(String jenkinsUrl) {
		this.jenkinsUrl = jenkinsUrl;
	}

	public String getJenkinsUserName() {
		return jenkinsUserName;
	}

	public void setJenkinsUserName(String jenkinsUserName) {
		this.jenkinsUserName = jenkinsUserName;
	}

	public String getJenkinsPassword() {
		return jenkinsPassword;
	}

	public void setJenkinsPassword(String jenkinsPassword) {
		this.jenkinsPassword = jenkinsPassword;
	}

	public boolean isServiceProject() {
		return serviceProject;
	}

	public void setServiceProject(boolean serviceProject) {
		this.serviceProject = serviceProject;
	}

	public boolean isDockerProject() {
		return dockerProject;
	}

	public void setDockerProject(boolean dockerProject) {
		this.dockerProject = dockerProject;
	}

	public String getGitRepositoryUrl() {
		return gitRepositoryUrl;
	}

	public void setGitRepositoryUrl(String gitRepositoryUrl) {
		this.gitRepositoryUrl = gitRepositoryUrl;
	}

	public String getGitUserName() {
		return gitUserName;
	}

	public void setGitUserName(String gitUserName) {
		this.gitUserName = gitUserName;
	}

	public String getGitPassword() {
		return gitPassword;
	}

	public void setGitPassword(String gitPassword) {
		this.gitPassword = gitPassword;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public String getDbDriver() {
		return dbDriver;
	}

	public void setDbDriver(String dbDriver) {
		this.dbDriver = dbDriver;
	}

	public String getDbPwd() {
		return dbPwd;
	}

	public void setDbPwd(String dbPwd) {
		this.dbPwd = dbPwd;
	}

	public String getDbUrl() {
		return dbUrl;
	}

	public void setDbUrl(String dbUrl) {
		this.dbUrl = dbUrl;
	}

	public String getDbUser() {
		return dbUser;
	}

	public void setDbUser(String dbUser) {
		this.dbUser = dbUser;
	}

	public String getBootPrefix() {
		return bootPrefix;
	}

	public void setBootPrefix(String bootPrefix) {
		this.bootPrefix = bootPrefix;
	}

	public String getFeignServerPath() {
		return feignServerPath;
	}

	public void setFeignServerPath(String feignServerPath) {
		this.feignServerPath = feignServerPath;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	@Override
	public String toString() {
		return "ManagerSourceGenerateEntity [authorName=" + authorName + ", dbDriver=" + dbDriver + ", dbPwd=" + dbPwd
				+ ", dbUrl=" + dbUrl + ", dbUser=" + dbUser + ", bootPrefix=" + bootPrefix + ", feignServerPath="
				+ feignServerPath + ", packageName=" + packageName + ", applicationName=" + applicationName
				+ ", gitRepositoryUrl=" + gitRepositoryUrl + ", gitUserName=" + gitUserName + ", gitPassword="
				+ gitPassword + "]";
	}
}
