package com.alinesno.cloud.base.boot.service;

import com.alinesno.cloud.base.boot.entity.ContentPostTypeEntity;
import com.alinesno.cloud.common.facade.services.IBaseService;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@NoRepositoryBean
public interface IContentPostTypeService extends IBaseService< ContentPostTypeEntity, String> {

	List<ContentPostTypeEntity> findAllWithApplication(RestWrapper restWrapper);

}
