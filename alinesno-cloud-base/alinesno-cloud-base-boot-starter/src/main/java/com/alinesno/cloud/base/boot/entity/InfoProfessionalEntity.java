package com.alinesno.cloud.base.boot.entity;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;
import org.hibernate.annotations.Proxy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Proxy(lazy = false)
@Entity
@Table(name="info_professional")
public class InfoProfessionalEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

	private String owners;
	@Column(name="professional_name")
	private String professionalName;
	@Column(name="professional_prop")
	private String professionalProp;


	public String getOwners() {
		return owners;
	}

	public void setOwners(String owners) {
		this.owners = owners;
	}

	public String getProfessionalName() {
		return professionalName;
	}

	public void setProfessionalName(String professionalName) {
		this.professionalName = professionalName;
	}

	public String getProfessionalProp() {
		return professionalProp;
	}

	public void setProfessionalProp(String professionalProp) {
		this.professionalProp = professionalProp;
	}


	@Override
	public String toString() {
		return "InfoProfessionalEntity{" +
			"owners=" + owners +
			", professionalName=" + professionalName +
			", professionalProp=" + professionalProp +
			"}";
	}
}
