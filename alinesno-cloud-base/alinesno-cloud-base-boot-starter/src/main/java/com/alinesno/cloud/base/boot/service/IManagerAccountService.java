package com.alinesno.cloud.base.boot.service;

import com.alinesno.cloud.base.boot.entity.ManagerAccountEntity;
import com.alinesno.cloud.common.facade.services.IBaseService;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@NoRepositoryBean
public interface IManagerAccountService extends IBaseService< ManagerAccountEntity, String> {
	
	/**
	 * 通过用户名查询用户信息
	 * @return
	 */
	ManagerAccountEntity findByLoginName(String loginName);

	boolean resetPassword(String id, String newPassword, String oldPassword);

	/**
	 * 注册用户
	 * @param loginName 邮箱 
	 * @param password 密码
	 * @param phoneCode 验证码
	 * @return
	 */
	boolean registAccount(String loginName, String password, String phoneCode);

}
