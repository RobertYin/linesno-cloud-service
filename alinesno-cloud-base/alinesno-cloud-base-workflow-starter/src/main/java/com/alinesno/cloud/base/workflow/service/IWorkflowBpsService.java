package com.alinesno.cloud.base.workflow.service;

import com.alinesno.cloud.base.workflow.bean.WorkItemBean;
import com.alinesno.cloud.base.workflow.entity.WorkflowBpsEntity;
import com.alinesno.cloud.common.facade.services.IBaseService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.ui.Model;

import javax.xml.stream.XMLStreamException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

/**
 * <p>  服务类 </p>
 *
 * @author WeiXiaoJin
 * @since 2019-09-02 08:11:33
 */
@NoRepositoryBean
public interface IWorkflowBpsService extends IBaseService<WorkflowBpsEntity, String> {

	/**
	 * 分页查询运行中的实例
	 * @param pageable
	 * @return
	 */
	Page<Map<String , Object>> findActivitiInstance(Pageable pageable) ;

	/***
	 * 部署模型
	 * 
	 * @param id
	 * @return
	 */

	String deploy(String id);

	/***
	 * 删除模型
	 */

	void deleteModel(String id);

	/***
	 * 挂起/激活
	 */

	String updateState(String state, String procDefId);

	/***
	 * 转化为模型
	 */

	Model convertToModel(String procDefId) throws UnsupportedEncodingException, XMLStreamException;

	/**
	 * 读取资源，通过部署ID
	 * 
	 * @param processDefinitionId 流程定义ID
	 * @param processInstanceId   流程实例ID
	 * @param resourceType        资源类型(xml|image)
	 */

	InputStream resourceRead(String procDefId, String proInsId, String resType) throws Exception;

	/***
	 * 删除正在运行的流程
	 */

	void deleteDeployment(String deploymentId);

	/***
	 * 启动流程,移动端，无法使用shiro---手机端使用
	 * 
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 */

	String startProcess(String id, Model m, String defKey, String title, Map<String, Object> var, String username);

	/***
	 * 流程取回
	 */

	void callBackTask(String taskId);

	/**
	 * 驳回流程
	 * 
	 * @param taskId     当前任务ID
	 * @param activityId 驳回节点ID
	 * @param variables  流程存储参数
	 * @throws Exception
	 */
	void backProcess(String taskId, String activityId, Map<String, Object> variables) throws Exception;

	/**
	 * 取回流程
	 * 
	 * @param taskId     当前任务ID
	 * @param activityId 取回节点ID
	 * @throws Exception
	 */
	void callBackProcess(String taskId, String activityId) throws Exception;

	/**
	 * 中止流程(特权人直接审批通过等)
	 * 
	 * @param taskId
	 */
	void endProcess(String taskId) throws Exception;

	/**
	 * 根据当前任务ID，查询可以驳回的任务节点
	 * 
	 * @param taskId 当前任务ID
	 */
	List<WorkItemBean> findBackAvtivity(String taskId) throws Exception;

	/**
	 * 转办流程
	 * 
	 * @param taskId   当前任务节点ID
	 * @param userCode 被转办人Code
	 */
	void transferAssignee(String taskId, String userCode);

	/**
	 * 创建模型
	 * @param model
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	String createModel(WorkItemBean model) throws UnsupportedEncodingException;
	
}
