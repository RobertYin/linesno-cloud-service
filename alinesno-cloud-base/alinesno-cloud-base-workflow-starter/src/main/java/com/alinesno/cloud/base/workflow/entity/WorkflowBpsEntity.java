package com.alinesno.cloud.base.workflow.entity;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * <p>
 * 
 * </p>
 *
 * @author WeiXiaoJin
 * @since 2019-09-02 08:11:33
 */
@Entity
@Table(name="workflow_bps")
public class WorkflowBpsEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;
	
	@Column(name="bps_key")
	private String bpsKey;
	
	@Column(name="bps_module")
	private String bpsModule;
	
	@Column(name="bps_name")
	private String bpsName;

	public String getBpsKey() {
		return bpsKey;
	}

	public void setBpsKey(String bpsKey) {
		this.bpsKey = bpsKey;
	}

	public String getBpsModule() {
		return bpsModule;
	}

	public void setBpsModule(String bpsModule) {
		this.bpsModule = bpsModule;
	}

	public String getBpsName() {
		return bpsName;
	}

	public void setBpsName(String bpsName) {
		this.bpsName = bpsName;
	}


	@Override
	public String toString() {
		return "WorkflowBpsEntity{" +
			" bpsKey=" + bpsKey +
			", bpsModule=" + bpsModule +
			", bpsName=" + bpsName +
			"}";
	}
}
