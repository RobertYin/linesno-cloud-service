package com.alinesno.cloud.base.logger.entity;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:16:06
 */
@Entity
@Table(name="log_type")
public class LogTypeEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 类型代码
     */
	@Column(name="type_code")
	private String typeCode;
    /**
     * 日志类型名称
     */
	@Column(name="type_name")
	private String typeName;


	public String getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}


	@Override
	public String toString() {
		return "LogTypeEntity{" +
			"typeCode=" + typeCode +
			", typeName=" + typeName +
			"}";
	}
}
