package com.alinesno.cloud.base.print;

import org.apache.dubbo.config.spring.context.annotation.DubboComponentScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * 启动入口<br/>
 * @EnableSwagger2 //开启swagger2 
 * 
 * @author LuoAnDong 
 * @sine 2019-05-03 14:05:339
 */
@EnableJpaAuditing // jpa注解支持
@EnableAsync // 开启异步任务
@DubboComponentScan
@SpringBootApplication
public class AlinesnoApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlinesnoApplication.class, args);
	}

}
