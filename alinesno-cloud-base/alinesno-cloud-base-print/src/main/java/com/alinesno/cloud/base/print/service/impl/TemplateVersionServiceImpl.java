package com.alinesno.cloud.base.print.service.impl;

import com.alinesno.cloud.base.print.entity.TemplateVersionEntity;
import com.alinesno.cloud.base.print.service.ITemplateVersionService;
import com.alinesno.cloud.common.core.service.impl.IBaseServiceImpl;
import org.apache.dubbo.config.annotation.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2019-05-03 14:07:38
 */
@Service
public class TemplateVersionServiceImpl extends IBaseServiceImpl< TemplateVersionEntity, String> implements ITemplateVersionService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(TemplateVersionServiceImpl.class);

}
