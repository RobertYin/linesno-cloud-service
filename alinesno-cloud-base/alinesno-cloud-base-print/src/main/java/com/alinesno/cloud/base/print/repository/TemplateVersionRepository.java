package com.alinesno.cloud.base.print.repository;

import com.alinesno.cloud.base.print.entity.TemplateVersionEntity;
import com.alinesno.cloud.common.facade.orm.repository.IBaseJpaRepository;

/**
 * <p>
  *  持久层接口
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-05-03 14:07:38
 */
public interface TemplateVersionRepository extends IBaseJpaRepository<TemplateVersionEntity, String> {

}
