package com.alinesno.cloud.platform.stack.excel;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alinesno.cloud.operation.cmdb.common.excel.PoiExcelExport;
import com.alinesno.cloud.operation.cmdb.entity.DatabaseEntity;
import com.alinesno.cloud.operation.cmdb.repository.DatabaseRepository;
import com.alinesno.cloud.platform.stack.JUnitBase;

public class ExportExcelTest extends JUnitBase {

	@Autowired
	private DatabaseRepository databaseRepository; // 订单详情服务

	@Test
	public void testExportExcel() {
		
		PoiExcelExport pee = new PoiExcelExport("/Users/luodong/Desktop/book2.xls","sheet1");
        
        String []titleColumn2 = {"dbaLoginName","dataSpaceName","dataSpaceSize","indexSpaceName","indexSpaceSize","spaceDesc","managerMan","hostname","hostIp","sendTime","endTime"};
        String titleName2[] = {"登陆名称","数据库空间","数据库空间大小","索引空间","索引空间大小","数据库使用描述","管理员","主机名称","主机IP","开始使用时间","开始使用时间"} ; 
        int titleSize2[] = {13,20,20,20,20,23,13,13,13,20,20};
        
        List<DatabaseEntity> dataList = databaseRepository.findAll() ; 
        pee.wirteExcel(titleColumn2, titleName2, titleSize2, dataList);
	}

}
