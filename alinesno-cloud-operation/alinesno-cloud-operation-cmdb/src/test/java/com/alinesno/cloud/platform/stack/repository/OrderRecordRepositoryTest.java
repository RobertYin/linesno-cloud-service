package com.alinesno.cloud.platform.stack.repository;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.test.context.junit4.SpringRunner;

import com.alinesno.cloud.operation.cmdb.entity.OrderRecordEntity;
import com.alinesno.cloud.operation.cmdb.repository.OrderRecordRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderRecordRepositoryTest {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass()) ; 
	
	@Autowired
	private OrderRecordRepository orderRecordRepository ;  //订单记录

	String orderId = "20180824000001" ; 
	
	@Test
	public void testFindAllByOrderId() {
		List<OrderRecordEntity> list = orderRecordRepository.findAllByOrderId(orderId , Sort.by(Direction.DESC, "addTime")) ; 
		logger.debug("list = {}" , list);
	}

}
