package com.alinesno.cloud.platform.stack.service.impl;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.alinesno.cloud.operation.cmdb.common.constants.ParamsEnum;
import com.alinesno.cloud.operation.cmdb.common.constants.RunerEnum;
import com.alinesno.cloud.operation.cmdb.entity.ParamsEntity;
import com.alinesno.cloud.operation.cmdb.service.ParamsService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ParamsServiceImplTest {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ParamsService paramsService;
	
	private String masterCode = "10608" ; 

	@Test
	public void testFindParamByName() {
		fail("Not yet implemented");
	}
	
	@Test
	public void testAddApplyDescParams() {
		ParamsEntity p1 = new ParamsEntity();
		p1.setParamName(ParamsEnum.USER_APPLY_DESC.getCode());
		p1.setParamDesc(ParamsEnum.USER_APPLY_DESC.getText());
		p1.setParamValue("为保障云主机服务和客户体验,请镖师遵守如下规则:\n1. 镖师接单必须积极接单,订单任务及时处理并签收;\n2. 如因其它原因不能及时跑单,及时与客户沟通或给管理员反馈;\n3. 订单完成,镖师需在公众号及时【签收】");
		p1.setMasterCode(masterCode);
		
		List<ParamsEntity> ps = new ArrayList<ParamsEntity>();
		ps.add(p1);
		
		Iterable<ParamsEntity> b = paramsService.addParamsList(ps);
		logger.debug("b = {}", b);
	}


	@Test
	public void testAddApplyTypeParams() {
		ParamsEntity p1 = new ParamsEntity();
		p1.setParamName(ParamsEnum.USER_APPLY_TYPE.getCode());
		p1.setParamDesc(ParamsEnum.USER_APPLY_TYPE.getText());
		p1.setParamValue(RunerEnum.USER_APPLY_TYPE_SIMPLE.getCode());
		p1.setMasterCode(masterCode);
		
		List<ParamsEntity> ps = new ArrayList<ParamsEntity>();
		ps.add(p1);
		
		Iterable<ParamsEntity> b = paramsService.addParamsList(ps);
		logger.debug("b = {}", b);
	}

	
	@Test
	public void testAddWechatParams() {
		ParamsEntity p1 = new ParamsEntity();
		p1.setParamName(ParamsEnum.WECHAT_SUBSCRIBE.getCode());
		p1.setParamDesc(ParamsEnum.WECHAT_SUBSCRIBE.getText());
		p1.setParamValue("感谢客官关注云主机  ~ 本云主机提供【帮你买、帮你取、帮你做】等服务，另外提供更高端的高级服务 ~ 点击菜单发布任务，镖师们会完成根据客官要求完成任务，你也可以申请成为镖师接任务，点击下方【我的任务】申请即可");
		p1.setMasterCode(null);
		
		ParamsEntity p2 = new ParamsEntity();
		p2.setParamName(ParamsEnum.WECHAT_MESSAGE_TEXT.getCode());
		p2.setParamDesc(ParamsEnum.WECHAT_MESSAGE_TEXT.getText());
		p2.setParamValue("感谢反馈，云主机管理员会根据你的反馈及时处理.");
		p2.setMasterCode(null);
		
		List<ParamsEntity> ps = new ArrayList<ParamsEntity>();
		ps.add(p1);
		ps.add(p2);
		
		Iterable<ParamsEntity> b = paramsService.addParamsList(ps);
		logger.debug("b = {}", b);
	}

	@Test
	public void testAddParams() {
		
		// 运营时间
		ParamsEntity p1 = new ParamsEntity();
		p1.setParamName(ParamsEnum.RUNNING_TIME.getCode());
		p1.setParamDesc(ParamsEnum.RUNNING_TIME.getText());
		p1.setParamValue("06:12-23:59");
		p1.setMasterCode(masterCode);

		ParamsEntity p2 = new ParamsEntity();
		p2.setParamName(ParamsEnum.ORDER_OUTTIME.getCode()) ; //"order_outtime");
		p2.setParamDesc(ParamsEnum.ORDER_OUTTIME.getText());
		p2.setParamValue("900");
		p2.setMasterCode(masterCode);

		ParamsEntity p3 = new ParamsEntity();
		p3.setParamName(ParamsEnum.ORDER_SALA_OUTTIME.getCode()) ; //"order_sala_outtime");
		p3.setParamDesc(ParamsEnum.ORDER_SALA_OUTTIME.getText());
		p3.setParamValue("60");
		p3.setMasterCode(masterCode);

		ParamsEntity p4 = new ParamsEntity();
		p4.setParamName(ParamsEnum.ORDER_MIN_PAY.getCode()) ; //"order_min_pay");
		p4.setParamDesc(ParamsEnum.ORDER_MIN_PAY.getText());
		p4.setParamValue("2");
		p4.setMasterCode(masterCode);

		ParamsEntity p5 = new ParamsEntity();
		p5.setParamName(ParamsEnum.RUNNING_SHOP_TIME.getCode()) ; //"running_shop_time");
		p5.setParamDesc(ParamsEnum.RUNNING_SHOP_TIME.getText());
		p5.setParamValue("06:12-23:59");
		p5.setMasterCode(masterCode);

		ParamsEntity p6 = new ParamsEntity();
		p6.setParamName(ParamsEnum.RUNNING_SHOP_MIN_PAY.getCode()) ; //"running_shop_min_pay");
		p6.setParamDesc(ParamsEnum.RUNNING_SHOP_MIN_PAY.getText());
		p6.setParamValue("10");
		p6.setMasterCode(masterCode);

		List<ParamsEntity> ps = new ArrayList<ParamsEntity>();
		ps.add(p1);
		ps.add(p2);
		ps.add(p3);
		ps.add(p4);
		ps.add(p5);
		ps.add(p6);

		Iterable<ParamsEntity> b = paramsService.addParamsList(ps);
		logger.debug("b = {}", b);
	}

	/**
	 * 判断是否属于运行时间
	 */
	@Test
	public void testIsRunningTime() {
		String masterCode = "12313" ; 
		boolean b = paramsService.isRunningTime(masterCode);
		logger.debug("是否为运行时间:{}", b);
	}

}
