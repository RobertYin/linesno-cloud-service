package com.alinesno.cloud.operation.cmdb.repository;
import com.alinesno.cloud.operation.cmdb.entity.AccountEntity;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface AccountRepository extends BaseJpaRepository<AccountEntity, String> {

	AccountEntity findByLoginNameAndSalt(String loingName , String salt) ;

	AccountEntity findByLoginName(String loginName); 
	
}