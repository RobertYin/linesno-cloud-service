package com.alinesno.cloud.operation.cmdb.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 链接管理 
 * @author LuoAnDong
 * @since 2018年8月13日 上午7:13:18
 */
@SuppressWarnings("serial")
@Entity
@Table(name="cmdb_links")
public class LinksEntity extends BaseEntity {

	private String linkPath ;  //链接路径
	private String linkDesc ;  //链接描述
	private String linkTags ; //链接标签
	private String linkIcons ; //链接图标
	
	public String getLinkPath() {
		return linkPath;
	}
	public void setLinkPath(String linkPath) {
		this.linkPath = linkPath;
	}
	public String getLinkDesc() {
		return linkDesc;
	}
	public void setLinkDesc(String linkDesc) {
		this.linkDesc = linkDesc;
	}
	public String getLinkTags() {
		return linkTags;
	}
	public void setLinkTags(String linkTags) {
		this.linkTags = linkTags;
	}
	public String getLinkIcons() {
		return linkIcons;
	}
	public void setLinkIcons(String linkIcons) {
		this.linkIcons = linkIcons;
	}
	
}
