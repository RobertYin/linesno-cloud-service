package com.alinesno.cloud.operation.cmdb.third.wechat;

/**
 * 微信模板消息
 * @author LuoAnDong
 * @since 2018年8月6日 上午8:03:23
 */
public class WechatMsg {

	private String url ; //路径 
	private String toUser ;  //推送用户
	private String orderId ; //订单id

	private String message ; //信息
	private String busType ; //业务类型
	private String price ; //费用 
	private String userPhone ; //用户手机号
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getBusType() {
		return busType;
	}
	public void setBusType(String busType) {
		this.busType = busType;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getUserPhone() {
		return userPhone;
	}
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getToUser() {
		return toUser;
	}
	public void setToUser(String toUser) {
		this.toUser = toUser;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

}
