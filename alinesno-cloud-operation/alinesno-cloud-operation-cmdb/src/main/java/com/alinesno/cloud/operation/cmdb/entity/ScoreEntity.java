package com.alinesno.cloud.operation.cmdb.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 订单评分
 * @author LuoAnDong
 * @since 2018年8月12日 下午2:20:37
 */
@SuppressWarnings("serial")
@Entity
@Table(name="cmdb_score")
public class ScoreEntity extends BaseEntity {

	private String score ; //评分(1一般|5满意|9优质)
	private String scoreContent ; //建议和内容
	private String userId ; //用户id
	private String orderId ; //用户id
	
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getScore() {
		return score;
	}
	public void setScore(String score) {
		this.score = score;
	}
	public String getScoreContent() {
		return scoreContent;
	}
	public void setScoreContent(String scoreContent) {
		this.scoreContent = scoreContent;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

}
