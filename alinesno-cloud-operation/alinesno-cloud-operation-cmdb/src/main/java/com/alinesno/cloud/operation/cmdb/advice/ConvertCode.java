package com.alinesno.cloud.operation.cmdb.advice;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ConvertCode {

	/**
	 * 类型
	 * @return
	 */
	public String[] types() default {} ; 
	
	/**
	 * 转换的字段(也上面类型对应)
	 * @return
	 */
	public String[] fields() default {} ; 
	
	/**
	 * 应用的字段
	 * @return
	 */
	public String[] applicationField() default {} ; 
	
	/**
	 * 应用id
	 */
	public String masterCode()  default "" ; //应用id

	/**
	 * 所属资源
	 * @return
	 */
	public String[] resourceField() default {};
}