package com.alinesno.cloud.operation.cmdb.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import cn.afterturn.easypoi.excel.annotation.Excel;

/**
 * 虚拟机列表
 * @author LuoAnDong
 * @since 2018年9月9日 上午11:08:18
 */
@SuppressWarnings("serial")
@Entity
@Table(name="cmdb_machine")
public class MachineEntity extends BaseEntity {

	@Excel(name = "虚拟机名称")
	private String machineName ; //虚拟机名称
	
	private String machinePrices ; //虚拟机价格
	private int machineNum ;  //虚拟机数量 
	
	@Excel(name = "虚拟机分类")
	private String machineType ; //虚拟机分类 
	
	@Excel(name = "虚拟机描述")
	private String machineDesc ; //虚拟机描述 
	
	@Excel(name = "CPU")
	private String machineCpu ; //CPU
	
	@Excel(name = "内存")
	private String machineMemery ; //内存
	
	@Excel(name = "数据盘")
	private String machineStorage ; //数据盘
	
	@Excel(name = "操作系统")
	private String machineSystem ; //操作系统
	
	@Excel(name = "备注")
	private String machineRemark ; //备注
	
	private String machineStatus ; //虚拟机状态(0正常|1下架)
	private String machinePic ; //虚拟机图片
	private int discount ; // 打折(按百分比)
	
	@Excel(name = "虚拟机IP")
	private String machineIp ; //虚拟机IP
	
	@Excel(name = "使用用户")
	private String userName  ; // 使用用户
	
	private String userId ; // 所属用户
	
	private String machineOpenPort ; //开放端口
	private String machineRootname ; // root账户名
	private String machinePwd ; //虚拟机密码
	
	@Excel(name = "主机IP")
	private String masterMachineIP ; //所属主机 
	
	private String masterMachine ; //所属主机 
	
	public String getMachineType() {
		return machineType;
	}
	public void setMachineType(String machineType) {
		this.machineType = machineType;
	}
	public String getMachineCpu() {
		return machineCpu;
	}
	public void setMachineCpu(String machineCpu) {
		this.machineCpu = machineCpu;
	}
	public String getMachineMemery() {
		return machineMemery;
	}
	public void setMachineMemery(String machineMemery) {
		this.machineMemery = machineMemery;
	}
	public String getMachineStorage() {
		return machineStorage;
	}
	public void setMachineStorage(String machineStorage) {
		this.machineStorage = machineStorage;
	}
	public String getMachineSystem() {
		return machineSystem;
	}
	public void setMachineSystem(String machineSystem) {
		this.machineSystem = machineSystem;
	}
	public String getMachineRemark() {
		return machineRemark;
	}
	public void setMachineRemark(String machineRemark) {
		this.machineRemark = machineRemark;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getMasterMachineIP() {
		return masterMachineIP;
	}
	public void setMasterMachineIP(String masterMachineIP) {
		this.masterMachineIP = masterMachineIP;
	}
	public String getMasterMachine() {
		return masterMachine;
	}
	public void setMasterMachine(String masterMachine) {
		this.masterMachine = masterMachine;
	}

	public String getMachineName() {
		return machineName;
	}
	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}
	public String getMachinePrices() {
		return machinePrices;
	}
	public void setMachinePrices(String machinePrices) {
		this.machinePrices = machinePrices;
	}
	public int getMachineNum() {
		return machineNum;
	}
	public void setMachineNum(int machineNum) {
		this.machineNum = machineNum;
	}
	public String getMachineDesc() {
		return machineDesc;
	}
	public void setMachineDesc(String machineDesc) {
		this.machineDesc = machineDesc;
	}
	public String getMachineStatus() {
		return machineStatus;
	}
	public void setMachineStatus(String machineStatus) {
		this.machineStatus = machineStatus;
	}
	public String getMachinePic() {
		return machinePic;
	}
	public void setMachinePic(String machinePic) {
		this.machinePic = machinePic;
	}
	public int getDiscount() {
		return discount;
	}
	public void setDiscount(int discount) {
		this.discount = discount;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getMachineIp() {
		return machineIp;
	}
	public void setMachineIp(String machineIp) {
		this.machineIp = machineIp;
	}
	public String getMachineOpenPort() {
		return machineOpenPort;
	}
	public void setMachineOpenPort(String machineOpenPort) {
		this.machineOpenPort = machineOpenPort;
	}
	public String getMachineRootname() {
		return machineRootname;
	}
	public void setMachineRootname(String machineRootname) {
		this.machineRootname = machineRootname;
	}
	public String getMachinePwd() {
		return machinePwd;
	}
	public void setMachinePwd(String machinePwd) {
		this.machinePwd = machinePwd;
	}
	
}
