package com.alinesno.cloud.operation.cmdb.repository;
import java.util.List;

import org.springframework.data.domain.Sort;

import com.alinesno.cloud.operation.cmdb.entity.AccountResourceEntity;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface AccountResourceRepository extends BaseJpaRepository<AccountResourceEntity, String> {

	List<AccountResourceEntity> findAllByAccountId(String accountId, Sort sort);

	/**
	 * 通过账户id删除信息
	 * @param accountId
	 */
	void deleteByAccountId(String accountId);

}