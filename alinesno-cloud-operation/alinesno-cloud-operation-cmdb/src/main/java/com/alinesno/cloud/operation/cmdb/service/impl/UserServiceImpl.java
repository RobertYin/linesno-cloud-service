package com.alinesno.cloud.operation.cmdb.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.operation.cmdb.common.constants.RunerEnum;
import com.alinesno.cloud.operation.cmdb.common.constants.ScoreActionEnum;
import com.alinesno.cloud.operation.cmdb.common.constants.UserStatusEnum;
import com.alinesno.cloud.operation.cmdb.entity.IntegralRecordEntity;
import com.alinesno.cloud.operation.cmdb.entity.OrdersEntity;
import com.alinesno.cloud.operation.cmdb.entity.UserEntity;
import com.alinesno.cloud.operation.cmdb.repository.IntegralRecordRepository;
import com.alinesno.cloud.operation.cmdb.repository.OrderRepository;
import com.alinesno.cloud.operation.cmdb.repository.UserRepository;
import com.alinesno.cloud.operation.cmdb.service.UserService;

/**
 * 用户服务
 * 
 * @author LuoAnDong
 * @since 2018年10月8日 下午10:51:19
 */
@Service
public class UserServiceImpl implements UserService {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private IntegralRecordRepository integralRecordRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private OrderRepository orderRepository ; 

	@Override
	public List<UserEntity> findAllMember(String masterCode , String id) {
		
		List<UserEntity> users = null ; 
		if(StringUtils.isNotBlank(id)) {
			users = userRepository.findAllByMasterCodeAndFieldPropAndIdNotOrderByIntegralDesc(masterCode , RunerEnum.STATUS_MEMBER.getCode() , id); // 查询所有的非人员
		}else {
			users = userRepository.findAllByMasterCodeAndFieldPropOrderByIntegralDesc(masterCode, RunerEnum.STATUS_MEMBER.getCode()); // 查询所有的非人员
		}

		return users;
	}
	
	@Transactional
	@Override
	public boolean recordIntegral(String userId , int score , ScoreActionEnum action , String orderId , String remark) {
		try {
	
			// 用户评价
			if(ScoreActionEnum.ACTION_SCORE_BEST == action || ScoreActionEnum.ACTION_SCORE_BAD == action) {
				OrdersEntity orderEntity = orderRepository.findByOrderId(orderId) ; 
				userId = orderEntity.getReceiveManagerId() ; 
			}
			
			Optional<UserEntity> user = userRepository.findById(userId) ; 
			
			if(user.isPresent()) {
				
				//保存用户状态  
				UserEntity u = user.get() ; 
				u.setIntegral(u.getIntegral()+score);
				userRepository.save(u) ; 
				
				// 保存积分 
				IntegralRecordEntity record = new IntegralRecordEntity() ; 
				record.setUserId(userId);
				record.setScoreValue(score);
				record.setActionName(action.text);
				record.setActionCode(action.action);
				record.setOrderId(orderId);
				record.setScoreTotal(u.getIntegral());
				record.setActionRemark("【"+action.text+"】"+remark);
				record.setAddTime(new Date());
				
				integralRecordRepository.save(record) ; 
			}
			
			return true ; 
		}catch(Exception e) {
			logger.error("积分记录失败,userId:{} , score:{} , action:{} , orderId:{} , remark:{}" , userId , score , action , orderId , remark );
		}
		return false ; 
	}

	@Override
	public boolean isForbidden(String id) {
		UserEntity user = userRepository.findById(id).get();
		if(UserStatusEnum.FORBIDDEN.getCode() == user.getHasStatus()) {
			return true ; 
		}
		return false;
	}

}
