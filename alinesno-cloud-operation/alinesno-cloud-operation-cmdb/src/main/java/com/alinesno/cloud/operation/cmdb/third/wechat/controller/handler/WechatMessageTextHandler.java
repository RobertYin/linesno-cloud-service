package com.alinesno.cloud.operation.cmdb.third.wechat.controller.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alinesno.cloud.operation.cmdb.common.constants.ParamsEnum;
import com.alinesno.cloud.operation.cmdb.entity.ParamsEntity;
import com.alinesno.cloud.operation.cmdb.service.ParamsService;
import com.alinesno.cloud.operation.cmdb.third.wechat.controller.WechatMessageBase;

import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutTextMessage;

/**
 * 微信消息
 * 
 * @author LuoAnDong
 * @since 2018年10月11日 上午8:58:53
 */
@Component("wechat_message_text")
public class WechatMessageTextHandler extends WechatMessageBase {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(WechatMessageTextHandler.class);

	@Autowired
	private ParamsService paramsService;

	@Override
	public Object handlerMessage(WxMpXmlMessage message, String fromUser, String toUser, String event, String content) {

		ParamsEntity param = paramsService.findParamByName(ParamsEnum.WECHAT_MESSAGE_TEXT.getCode(), null);
		WxMpXmlOutTextMessage text = WxMpXmlOutTextMessage.TEXT().toUser(fromUser).fromUser(toUser)
				.content(param != null ? param.getParamValue() : "感谢反馈，管理员会及时处理.").build();
		return text.toXml();
	}

}
