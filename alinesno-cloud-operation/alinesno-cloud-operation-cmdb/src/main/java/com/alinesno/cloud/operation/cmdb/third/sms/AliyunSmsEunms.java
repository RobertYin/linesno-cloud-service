package com.alinesno.cloud.operation.cmdb.third.sms;
/**
 * 阿里云的短信服务
 * @author LuoAnDong
 * @since 2018年3月3日 上午11:06:54
 */
public enum AliyunSmsEunms {

	CODE("code") ,
	NAME("name"), 
	STATUS("status") , 
	REMARK("remark"), 
	OK("OK");  //发送成功 
	
	private String value ; 
	
	private AliyunSmsEunms(String v) {
		this.value = v ; 
	}
	
	public String value() {
		return value ; 
	}
	
}