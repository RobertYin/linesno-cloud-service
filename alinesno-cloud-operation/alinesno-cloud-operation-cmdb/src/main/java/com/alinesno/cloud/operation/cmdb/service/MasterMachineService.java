package com.alinesno.cloud.operation.cmdb.service;

import com.alinesno.cloud.operation.cmdb.entity.MasterMachineEntity;

public interface MasterMachineService {

	MasterMachineEntity findById(String id);

	boolean saveMasterMachine(MasterMachineEntity e , String masterCode);

	MasterMachineEntity findByMasterCode(String masterCode);

}
