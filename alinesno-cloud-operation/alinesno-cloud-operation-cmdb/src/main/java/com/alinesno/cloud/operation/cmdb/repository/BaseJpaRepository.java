package com.alinesno.cloud.operation.cmdb.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * 工作基础类
 * @author LuoAnDong
 * @since 2018年9月23日 下午5:57:51
 * @param <T>
 * @param <K>
 */
@NoRepositoryBean
public interface BaseJpaRepository<T , K> extends JpaRepository<T, K>  , JpaSpecificationExecutor<T>{
	
	Page<T> findAllByMasterCode(String masterCode, Pageable pageable);
	
	List<T> findAllByMasterCode(String masterCode) ;

}
