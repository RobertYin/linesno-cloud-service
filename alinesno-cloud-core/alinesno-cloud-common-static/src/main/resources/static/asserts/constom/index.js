/**
 * 首页方法封装处理
 * Copyright (c) 2019 ruoyi
 */
layer.config({
    extend: 'moon/style.css',
    skin: 'layer-ext-moon'
});

////////////////////////////////////////////////////ajax start ///////////////////////////////////////////
$(function(){  
	var layerIndex ; 
    $.ajaxSetup({
        type: "POST",
    	contentType : "application/json;charset=utf-8",
    	timeout: 30000 ,
	    dataType:'json',
    	beforeSend:function(xhr){
        	console.log("ajax beforeSend = "+xhr) ;
        	layerIndex = layer.load(1, {shade: [0.8,'#000']});
    	},success:function(result){
        	console.log("ajax success = "+result) ;
        	if(result.status != 200){
        		layer.alert(result.desc) ; 
        	}
    	},
    	complete : function(XMLHttpRequest, textStatus) {
        	console.log("ajax complete = "+XMLHttpRequest + " , status = " + textStatus) ;
        	if(layerIndex != null){
        		layer.close(layerIndex) ; 
        	}
        },
        error: function(jqXHR, textStatus, errorThrown) {
        	console.log("ajax error = " + jqXHR + " , status = " + textStatus) ; 
        	layer.alert("服务响应失败,请联系管理员.") ; 
        }
   });
    
   // key search 
   // enterSearch() ;  
});
//////////////////////////////////////////////////// ajax end ///////////////////////////////////////////

/**
* jquery菜单
*/
;(function ($) {
    $.fn.menus = function (options) {
    	
    	var defaults={
    		url: options.url, // 菜单链接  
    		params: options.params,
    		parentId: options.parentId ,
    		target: options.target // 生成HTML的目标id 
		};
		var endOptions=$.extend(defaults,options); 
	
		var navHtml = '' ; 
		
			$.ajax({
				type:'GET',
			    url: endOptions.url ,
			    data: endOptions.params ,
			    success: function(response){
			        console.log('返回的数据: ' + response);
			        if(response.code != 200 || response.data == null){
			        	// layer.alert(response.message==null?"返回数据失败":response.message) ;
			        	console.log(response.message==null?"返回数据失败":response.message) ;
			        	return ; 
			        }
			        
			        var data = response.data ; 
			        for(var i = 0 ; i < data.length ; i ++){
			        	var item = data[i] ; 
						if(item.menuType == 1){
							navHtml += '<li class="divider"></li><li class="nav-title">'+item.resourceName+'</li>' ; 
						}
						
			        	var itemList = item.subResource ; 
			        	if(itemList != null){
							for(var j = 0 ; j < itemList.length ; j ++){
								
								var itemListSub = itemList[j] ; 
								var itemListSubList = itemListSub.subResource ; 
								
								if(itemListSubList != null && itemListSubList.length > 0){
								
									var subNavHtml = '<ul class="nav nav-second-level collapse">' ;
									for(var n = 0 ; n < itemListSubList.length ; n ++){
										subNavHtml += 
										'<li>' +
											'<a class="menuItem" href="'+itemListSubList[n].resourceLink+'"> <i class="'+ itemListSubList[n].resourceIcon +'"></i> '+ itemListSubList[n].resourceName + '</a>' +
										'</li>' ; 
									}
									subNavHtml += '</ul>' ;
									
									navHtml += 
									'<li>'+
										'<a href="#"><i class="'+ itemListSub.resourceIcon +'"></i> <span class="nav-label">'+ itemListSub.resourceName + '</span><span class="fa arrow"></span></a>'+
										subNavHtml + 
									'</li>' ; 
								}else{
									navHtml += 
									'<li>'+
					                    '<a href="'+itemListSub.resourceLink+'" class="menuItem">'+
					                    '	<i class="'+ itemListSub.resourceIcon +'"></i> '+
					                    '	<span class="nav-label">'+ itemListSub.resourceName + '</span> '+
					                    '</a>'+
					                '</li> ' ; 
								}
							}
			        	}
						
			        }
			        // console.log(navHtml) ; 
					navHtml += '' ; 
					$(endOptions.target).html(navHtml) ; 
					
					$('#side-menu').metisMenu();
					initMenus() ; 
			    }
			});
    };
})(jQuery);

//显示应用菜单
function showApplicationMenus(self , applicationId){
	var id = $(self).data("id") ; 
	console.log("application id = " + id + " , applicationId = " + applicationId) ;
	if(applicationId !== null && typeof(applicationId) != "undefined"){
		id = applicationId ; 
	}
	
	if(id === null || typeof(id) == "undefined"){
		return ; 
	}
	
	$('#side-menu').menus({
		url:'dashboard/side',
		params:'applicationId='+id ,
		target:'ul#side-menu'
	}) ; 
}

$(function() {
	
	var applicationId = $("a#applicationItem").first().data("id") ; 
	showApplicationMenus(self , applicationId) ; 
	
    // MetsiMenu
    // $('#side-menu').metisMenu();

    //固定菜单栏
    $(function() {
        $('.sidebar-collapse').slimScroll({
            height: '100%',
            railOpacity: 0.9,
            alwaysVisible: false
        });
    });

    // 菜单切换
    $('.navbar-minimalize').click(function() {
        $("body").toggleClass("mini-navbar");
        SmoothlyMenu();
    });

    $('#side-menu>li').click(function() {
        if ($('body').hasClass('mini-navbar')) {
            NavToggle();
        }
    });
    $('#side-menu>li li a').click(function() {
        if ($(window).width() < 769) {
            NavToggle();
        }
    });

    $('.nav-close').click(NavToggle);

    //ios浏览器兼容性处理
    if (/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)) {
        $('#content-main').css('overflow-y', 'auto');
    }

});

$(window).bind("load resize",
function() {
    if ($(this).width() < 769) {
        $('body').addClass('mini-navbar');
        $('.navbar-static-side').fadeIn();
        $(".sidebar-collapse .logo").addClass("hide");
    }
});

function NavToggle() {
    $('.navbar-minimalize').trigger('click');
}

function SmoothlyMenu() {
    if (!$('body').hasClass('mini-navbar')) {
        $('#side-menu').hide();
        $(".sidebar-collapse .logo").removeClass("hide");
        setTimeout(function() {
            $('#side-menu').fadeIn(500);
        },
        100);
    } else if ($('body').hasClass('fixed-sidebar')) {
        $('#side-menu').hide();
        $(".sidebar-collapse .logo").addClass("hide");
        setTimeout(function() {
            $('#side-menu').fadeIn(500);
        },
        300);
    } else {
        $('#side-menu').removeAttr('style');
    }
}

/**
 * iframe处理
 */
// $(function() {
function initMenus(){
    //计算元素集合的总宽度
    function calSumWidth(elements) {
        var width = 0;
        $(elements).each(function() {
            width += $(this).outerWidth(true);
        });
        return width;
    }
    
    // 激活指定选项卡
    function setActiveTab(element) {
        if (!$(element).hasClass('active')) {
            var currentId = $(element).data('id');
            // 显示tab对应的内容区
            $('.RuoYi_iframe').each(function() {
                if ($(this).data('id') == currentId) {
                    $(this).show().siblings('.RuoYi_iframe').hide();
                }
            });
            $(element).addClass('active').siblings('.menuTab').removeClass('active');
            scrollToTab(element);
        }
    }

    //滚动到指定选项卡
    function scrollToTab(element) {
        var marginLeftVal = calSumWidth($(element).prevAll()),
        marginRightVal = calSumWidth($(element).nextAll());
        // 可视区域非tab宽度
        var tabOuterWidth = calSumWidth($(".content-tabs").children().not(".menuTabs"));
        //可视区域tab宽度
        var visibleWidth = $(".content-tabs").outerWidth(true) - tabOuterWidth;
        //实际滚动宽度
        var scrollVal = 0;
        if ($(".page-tabs-content").outerWidth() < visibleWidth) {
            scrollVal = 0;
        } else if (marginRightVal <= (visibleWidth - $(element).outerWidth(true) - $(element).next().outerWidth(true))) {
            if ((visibleWidth - $(element).next().outerWidth(true)) > marginRightVal) {
                scrollVal = marginLeftVal;
                var tabElement = element;
                while ((scrollVal - $(tabElement).outerWidth()) > ($(".page-tabs-content").outerWidth() - visibleWidth)) {
                    scrollVal -= $(tabElement).prev().outerWidth();
                    tabElement = $(tabElement).prev();
                }
            }
        } else if (marginLeftVal > (visibleWidth - $(element).outerWidth(true) - $(element).prev().outerWidth(true))) {
            scrollVal = marginLeftVal - $(element).prev().outerWidth(true);
        }
        $('.page-tabs-content').animate({
            marginLeft: 0 - scrollVal + 'px'
        },
        "fast");
    }

    //查看左侧隐藏的选项卡
    function scrollTabLeft() {
        var marginLeftVal = Math.abs(parseInt($('.page-tabs-content').css('margin-left')));
        // 可视区域非tab宽度
        var tabOuterWidth = calSumWidth($(".content-tabs").children().not(".menuTabs"));
        //可视区域tab宽度
        var visibleWidth = $(".content-tabs").outerWidth(true) - tabOuterWidth;
        //实际滚动宽度
        var scrollVal = 0;
        if (($(".page-tabs-content").width()) < visibleWidth) {
            return false;
        } else {
            var tabElement = $(".menuTab:first");
            var offsetVal = 0;
            while ((offsetVal + $(tabElement).outerWidth(true)) <= marginLeftVal) { //找到离当前tab最近的元素
                offsetVal += $(tabElement).outerWidth(true);
                tabElement = $(tabElement).next();
            }
            offsetVal = 0;
            if (calSumWidth($(tabElement).prevAll()) > visibleWidth) {
                while ((offsetVal + $(tabElement).outerWidth(true)) < (visibleWidth) && tabElement.length > 0) {
                    offsetVal += $(tabElement).outerWidth(true);
                    tabElement = $(tabElement).prev();
                }
                scrollVal = calSumWidth($(tabElement).prevAll());
            }
        }
        $('.page-tabs-content').animate({
            marginLeft: 0 - scrollVal + 'px'
        },
        "fast");
    }

    //查看右侧隐藏的选项卡
    function scrollTabRight() {
        var marginLeftVal = Math.abs(parseInt($('.page-tabs-content').css('margin-left')));
        // 可视区域非tab宽度
        var tabOuterWidth = calSumWidth($(".content-tabs").children().not(".menuTabs"));
        //可视区域tab宽度
        var visibleWidth = $(".content-tabs").outerWidth(true) - tabOuterWidth;
        //实际滚动宽度
        var scrollVal = 0;
        if ($(".page-tabs-content").width() < visibleWidth) {
            return false;
        } else {
            var tabElement = $(".menuTab:first");
            var offsetVal = 0;
            while ((offsetVal + $(tabElement).outerWidth(true)) <= marginLeftVal) { //找到离当前tab最近的元素
                offsetVal += $(tabElement).outerWidth(true);
                tabElement = $(tabElement).next();
            }
            offsetVal = 0;
            while ((offsetVal + $(tabElement).outerWidth(true)) < (visibleWidth) && tabElement.length > 0) {
                offsetVal += $(tabElement).outerWidth(true);
                tabElement = $(tabElement).next();
            }
            scrollVal = calSumWidth($(tabElement).prevAll());
            if (scrollVal > 0) {
                $('.page-tabs-content').animate({
                    marginLeft: 0 - scrollVal + 'px'
                },
                "fast");
            }
        }
    }

    //通过遍历给菜单项加上data-index属性
    $(".menuItem").each(function(index) {
        if (!$(this).attr('data-index')) {
            $(this).attr('data-index', index);
        }
    });

    function menuItem() {
        // 获取标识数据
        var dataUrl = $(this).attr('href'),
        dataIndex = $(this).data('index'),
        menuName = $.trim($(this).text()),
        flag = true;
        $(".nav ul li").removeClass("active");
        $(this).parent("li").addClass("active");
        if (dataUrl == undefined || $.trim(dataUrl).length == 0) return false;

        // 选项卡菜单已存在
        $('.menuTab').each(function() {
            if ($(this).data('id') == dataUrl) {
                if (!$(this).hasClass('active')) {
                    $(this).addClass('active').siblings('.menuTab').removeClass('active');
                    scrollToTab(this);
                    // 显示tab对应的内容区
                    $('.mainContent .RuoYi_iframe').each(function() {
                        if ($(this).data('id') == dataUrl) {
                            $(this).show().siblings('.RuoYi_iframe').hide();
                            return false;
                        }
                    });
                }
                flag = false;
                return false;
            }
        });
        // 选项卡菜单不存在
        if (flag) {
            var str = '<a href="javascript:;" class="active menuTab" data-id="' + dataUrl + '">' + menuName + ' <i class="fa fa-times-circle"></i></a>';
            $('.menuTab').removeClass('active');

            // 添加选项卡对应的iframe
            var str1 = '<iframe class="RuoYi_iframe" name="iframe' + dataIndex + '" width="100%" height="100%" src="' + dataUrl + '" frameborder="0" data-id="' + dataUrl + '" seamless></iframe>';
            $('.mainContent').find('iframe.RuoYi_iframe').hide().parents('.mainContent').append(str1);
            
            $.modal.loading("数据加载中，请稍后...");
            
            $('.mainContent iframe:visible').load(function () {
            	$.modal.closeLoading();
            });
            
            // 添加选项卡
            $('.menuTabs .page-tabs-content').append(str);
            scrollToTab($('.menuTab.active'));
        }
        return false;
    }
    
    function menuBlank() {
    	// 新窗口打开外网以http://开头，如http://ruoyi.vip
    	var dataUrl = $(this).attr('href');
    	window.open(dataUrl);
    	return false;
    }

    $('.menuItem').on('click', menuItem);
    
    $('.menuBlank').on('click', menuBlank);

    // 关闭选项卡菜单
    function closeTab() {
        var closeTabId = $(this).parents('.menuTab').data('id');
        var currentWidth = $(this).parents('.menuTab').width();
        var panelUrl = $(this).parents('.menuTab').data('panel');
        // 当前元素处于活动状态
        if ($(this).parents('.menuTab').hasClass('active')) {

            // 当前元素后面有同辈元素，使后面的一个元素处于活动状态
            if ($(this).parents('.menuTab').next('.menuTab').size()) {

                var activeId = $(this).parents('.menuTab').next('.menuTab:eq(0)').data('id');
                $(this).parents('.menuTab').next('.menuTab:eq(0)').addClass('active');

                $('.mainContent .RuoYi_iframe').each(function() {
                    if ($(this).data('id') == activeId) {
                        $(this).show().siblings('.RuoYi_iframe').hide();
                        return false;
                    }
                });

                var marginLeftVal = parseInt($('.page-tabs-content').css('margin-left'));
                if (marginLeftVal < 0) {
                    $('.page-tabs-content').animate({
                        marginLeft: (marginLeftVal + currentWidth) + 'px'
                    },
                    "fast");
                }

                //  移除当前选项卡
                $(this).parents('.menuTab').remove();

                // 移除tab对应的内容区
                $('.mainContent .RuoYi_iframe').each(function() {
                    if ($(this).data('id') == closeTabId) {
                        $(this).remove();
                        return false;
                    }
                });
            }

            // 当前元素后面没有同辈元素，使当前元素的上一个元素处于活动状态
            if ($(this).parents('.menuTab').prev('.menuTab').size()) {
                var activeId = $(this).parents('.menuTab').prev('.menuTab:last').data('id');
                $(this).parents('.menuTab').prev('.menuTab:last').addClass('active');
                $('.mainContent .RuoYi_iframe').each(function() {
                    if ($(this).data('id') == activeId) {
                        $(this).show().siblings('.RuoYi_iframe').hide();
                        return false;
                    }
                });

                //  移除当前选项卡
                $(this).parents('.menuTab').remove();

                // 移除tab对应的内容区
                $('.mainContent .RuoYi_iframe').each(function() {
                    if ($(this).data('id') == closeTabId) {
                        $(this).remove();
                        return false;
                    }
                });
                
                if($.common.isNotEmpty(panelUrl)){
            		$('.menuTab[data-id="' + panelUrl + '"]').addClass('active').siblings('.menuTab').removeClass('active');
            		$('.mainContent .RuoYi_iframe').each(function() {
                        if ($(this).data('id') == panelUrl) {
                            $(this).show().siblings('.RuoYi_iframe').hide();
                            return false;
                        }
            		});
            	}
            }
        }
        // 当前元素不处于活动状态
        else {
            //  移除当前选项卡
            $(this).parents('.menuTab').remove();

            // 移除相应tab对应的内容区
            $('.mainContent .RuoYi_iframe').each(function() {
                if ($(this).data('id') == closeTabId) {
                    $(this).remove();
                    return false;
                }
            });
        }
        scrollToTab($('.menuTab.active'));
        return false;
    }
    
    $('.menuTabs').on('click', '.menuTab i', closeTab);

    //滚动到已激活的选项卡
    function showActiveTab() {
        scrollToTab($('.menuTab.active'));
    }
    $('.tabShowActive').on('click', showActiveTab);

    // 点击选项卡菜单
    function activeTab() {
        if (!$(this).hasClass('active')) {
            var currentId = $(this).data('id');
            // 显示tab对应的内容区
            $('.mainContent .RuoYi_iframe').each(function() {
                if ($(this).data('id') == currentId) {
                    $(this).show().siblings('.RuoYi_iframe').hide();
                    return false;
                }
            });
            $(this).addClass('active').siblings('.menuTab').removeClass('active');
            scrollToTab(this);
        }
    }

    // 点击选项卡菜单
    $('.menuTabs').on('click', '.menuTab', activeTab);

    // 刷新iframe
    function refreshTab() {
    	var currentId = $('.page-tabs-content').find('.active').attr('data-id');
    	var target = $('.RuoYi_iframe[data-id="' + currentId + '"]');
        var url = target.attr('src');
        target.attr('src', url).ready();
    }
    
    // 关闭当前选项卡
    function tabCloseCurrent() {
    	$('.page-tabs-content').find('.active i').trigger("click");
    }
    
    //关闭其他选项卡
    function tabCloseOther() {
        $('.page-tabs-content').children("[data-id]").not(":first").not(".active").each(function() {
            $('.RuoYi_iframe[data-id="' + $(this).data('id') + '"]').remove();
            $(this).remove();
        });
        $('.page-tabs-content').css("margin-left", "0");
    }
    
    // 关闭全部选项卡
    function tabCloseAll() {
    	$('.page-tabs-content').children("[data-id]").not(":first").each(function() {
            $('.RuoYi_iframe[data-id="' + $(this).data('id') + '"]').remove();
            $(this).remove();
        });
        $('.page-tabs-content').children("[data-id]:first").each(function() {
            $('.RuoYi_iframe[data-id="' + $(this).data('id') + '"]').show();
            $(this).addClass("active");
        });
        $('.page-tabs-content').css("margin-left", "0");
    }
    
    
    // 全屏显示
    $('#fullScreen').on('click', function () {
    	$(document).toggleFullScreen();
    });
    
    // 刷新按钮
    $('.tabReload').on('click', refreshTab);

    // 双击选项卡全屏显示
    $('.menuTabs').on('dblclick', '.menuTab', activeTabMax);

    // 左移按扭
    $('.tabLeft').on('click', scrollTabLeft);

    // 右移按扭
    $('.tabRight').on('click', scrollTabRight);
    
    // 关闭当前
    $('.tabCloseCurrent').on('click', tabCloseCurrent);
    
    // 关闭其他
    $('.tabCloseOther').on('click', tabCloseOther);

    // 关闭全部
    $('.tabCloseAll').on('click', tabCloseAll);
    
    // tab全屏显示
    $('.tabMaxCurrent').on('click', function () {
        $('.page-tabs-content').find('.active').trigger("dblclick");
    });
    
    // 关闭全屏
    $('#ax_close_max').click(function(){
    	$('#content-main').toggleClass('max');
    	$('#ax_close_max').hide();
    })
    
    // 双击选项卡全屏显示
    function activeTabMax() {
        $('#content-main').toggleClass('max');
        $('#ax_close_max').show();
    }
    
    $(window).keydown(function(event) {
        if (event.keyCode == 27) {
            $('#content-main').removeClass('max');
            $('#ax_close_max').hide();
        }
    });
    
    // 右键菜单实现
    $.contextMenu({
        selector: ".menuTab",
        trigger: 'right',
        autoHide: true,
        items: {
            "close_current": {
                name: "关闭当前",
                icon: "fa-close",
                callback: function(key, opt) {
                	opt.$trigger.find('i').trigger("click");
                }
            },
            "close_other": {
                name: "关闭其他",
                icon: "fa-window-close-o",
                callback: function(key, opt) {
                	setActiveTab(this);
                    tabCloseOther();
                }
            },
            "close_left": {
                name: "关闭左侧",
                icon: "fa-reply",
                callback: function(key, opt) {
                	setActiveTab(this);
                	this.prevAll('.menuTab').not(":last").each(function() {
                	    if ($(this).hasClass('active')) {
                	        setActiveTab(this);
                	    }
                	    $('.RuoYi_iframe[data-id="' + $(this).data('id') + '"]').remove();
                	    $(this).remove();
                	});
                	$('.page-tabs-content').css("margin-left", "0");
                }
            },
            "close_right": {
                name: "关闭右侧",
                icon: "fa-share",
                callback: function(key, opt) {
                	setActiveTab(this);
                	this.nextAll('.menuTab').each(function() {
                        $('.menuTab[data-id="' + $(this).data('id') + '"]').remove();
                        $(this).remove();
                    });
                }
            },
            "close_all": {
                name: "全部关闭",
                icon: "fa-window-close",
                callback: function(key, opt) {
                    tabCloseAll();
                }
            },
            "step": "---------",
            "full": {
                name: "全屏显示",
                icon: "fa-arrows-alt",
                callback: function(key, opt) {
                	setActiveTab(this);
                    var target = $('.RuoYi_iframe[data-id="' + this.data('id') + '"]');
                	target.fullScreen(true);
                }
            },
            "refresh": {
                name: "刷新页面",
                icon: "fa-refresh",
                callback: function(key, opt) {
                	setActiveTab(this);
                	var target = $('.RuoYi_iframe[data-id="' + this.data('id') + '"]');
                	var url = target.attr('src');
                    target.attr('src', url).ready();
                    $.modal.loading("数据加载中，请稍后...");
                    target.attr('src', url).load(function () {
                    	$.modal.closeLoading();
                    });
                }
            },
            "open": {
                name: "新窗口打开",
                icon: "fa-link",
                callback: function(key, opt) {
                	var target = $('.RuoYi_iframe[data-id="' + this.data('id') + '"]');
                    window.open(target.attr('src'));
                }
            },
        }
    })
}
// });