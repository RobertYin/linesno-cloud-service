package com.alinesno.cloud.common.config;

import org.jasypt.encryption.StringEncryptor;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 密码工具类
 * @author LuoAnDong
 * @since 2019年7月27日 上午8:40:47
 */
public class StringEncryptorUtils {

	@Autowired
    private StringEncryptor encryptor;
	
	/**
	 * 字符串加密 
	 * @return
	 */
	public String stringEncryptor(String str) {
		return encryptor.encrypt(str) ; 
	}
	
}
