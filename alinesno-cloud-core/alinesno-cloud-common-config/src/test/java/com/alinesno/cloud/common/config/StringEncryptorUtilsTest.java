package com.alinesno.cloud.common.config;

import org.jasypt.encryption.StringEncryptor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StringEncryptorUtilsTest {

	private static final Logger log = LoggerFactory.getLogger(StringEncryptorUtilsTest.class) ; 

	@Autowired
	StringEncryptor stringEncryptor;
	
	@Test
	public void test() {
		String str = "zookeeper://47.97.200.139:2181" ; 
		String e = stringEncryptor.encrypt(str) ; 
		log.debug("e:{}" , e);
	}

}
