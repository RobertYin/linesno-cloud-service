package com.alinesno.cloud.compoment.generate.generator.tools;

import java.io.IOException;
import java.net.URISyntaxException;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 测试jenkins工程
 * @author LuoAnDong
 * @since 2019年9月15日 下午3:32:12
 */
public class JenkinsToolsTest {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(JenkinsToolsTest.class) ;
	
	@Test
	public void testBuildJob() {
	}

	@Test
	public void testBuildMavenJob() throws IOException, URISyntaxException {
	
		String credentialsId = "a3164ab0-d246-4d5f-a852-1d775e879599" ; 
		
		JenkinsJobBean bean = new JenkinsJobBean() ; 
		bean.setCredentialsId(credentialsId);
		bean.setDescription("示例jenkins工程");
		bean.setDocker(true);
		bean.setGitBranch("master");
		bean.setGitUrl("https://gitee.com/landonniao/alinesno-cloud-demo-jenkins.git");
		bean.setJobName("alinesno-cloud-demo-"+System.currentTimeMillis());
	
		String mvnGoals = "clean package spring-boot:repackage -U " ; 
		if(bean.isDocker()) {
			mvnGoals += " docker:build" ; 
		}
		bean.setGoals(mvnGoals) ; 
	
		String url = "http://jenkins.linesno.com" ; 
		String username = "developer" ; 
		String password = "developer123$%^" ; 
		
		JenkinsTools.buildMavenJob(bean, url, username, password);
	}

}
