package com.alinesno.cloud.compoment.generate;

/**
 * 生成代码
 * 
 * @author luodong
 *
 */
public class GeneratorTest extends Generator {

	@Override
	public void initTableName() {
	}

	public static void main(String[] args) {
		GeneratorTest g = new GeneratorTest();
		
		g.setSuperRepositoryClassName("com.alinesno.cloud.common.facade.orm.repository.IBaseJpaRepository");
		g.setSuperServiceClassName("com.alinesno.cloud.common.facade.services.IBaseService");
		g.setSuperServiceImplClassName("com.alinesno.cloud.common.core.service.impl.IBaseServiceImpl");
		g.setSuperEntityClassName("com.alinesno.cloud.common.facade.orm.entity.BaseEntity");
		g.setSuperControllerClassName("com.alinesno.cloud.common.web.base.controller.LocalMethodBaseController");

		// 启动服务
		g.setArtifactId("alinesno-cloud-business-crm") ; 
		g.parentPackage="com.alinesno.cloud";
		g.generator("crm"); 
		
	}

}
