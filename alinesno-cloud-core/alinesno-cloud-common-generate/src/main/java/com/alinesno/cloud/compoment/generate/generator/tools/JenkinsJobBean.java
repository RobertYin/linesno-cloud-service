package com.alinesno.cloud.compoment.generate.generator.tools;

/**
 * Jenkins任务实体
 * @author LuoAnDong
 * @since 2019年9月14日 下午8:48:53
 */
public class JenkinsJobBean {

	private String jobName ; // 构建任务名称
	private String description ; // 任务描述 
	private String gitUrl ;  // 创建地址
	private String gitBranch ;  // 分支
	private String rootPom ;  // pom.xml地址 
	private String goals ;  // maven打包命令
	private String credentialsId ; // jenkins认证
	private boolean docker ; // 是否集成docker
	
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getCredentialsId() {
		return credentialsId;
	}
	public void setCredentialsId(String credentialsId) {
		this.credentialsId = credentialsId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getGitUrl() {
		return gitUrl;
	}
	public void setGitUrl(String gitUrl) {
		this.gitUrl = gitUrl;
	}
	public String getGitBranch() {
		return gitBranch;
	}
	public void setGitBranch(String gitBranch) {
		this.gitBranch = gitBranch;
	}
	public String getRootPom() {
		return rootPom;
	}
	public void setRootPom(String rootPom) {
		this.rootPom = rootPom;
	}
	public String getGoals() {
		return goals;
	}
	public void setGoals(String goals) {
		this.goals = goals;
	}
	public boolean isDocker() {
		return docker;
	}
	public void setDocker(boolean docker) {
		this.docker = docker;
	}
	
}
