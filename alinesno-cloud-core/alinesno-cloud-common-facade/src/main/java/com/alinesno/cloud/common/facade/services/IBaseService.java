package com.alinesno.cloud.common.facade.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;
import com.alinesno.cloud.common.facade.orm.repository.IBaseJpaRepository;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;

/**
 * 业务服务基类
 * @author LuoAnDong
 * @since 2018年11月20日 下午7:51:58
 */
@NoRepositoryBean
public interface IBaseService <Entity extends BaseEntity , ID> extends IBaseJpaRepository<Entity , ID>{

	/**
	 * 批量删除 
	 * @param ids
	 */
	void deleteByIds(ID[] ids);

	boolean modifyHasStatus(ID id);

	List<Entity> findAll(RestWrapper restWrapper);

	Page<Entity> findAllByWrapperAndPageable(RestWrapper restWrapper);

	Entity findEntityById(ID id);
	
	/**
	 * 找出前几条数据
	 * @param i
	 * @return
	 */
	List<Entity> findTop(int number , RestWrapper restWrapper);
	
	/**
	 * 条件删除 
	 * @param restWrapper
	 */
	void deleteByWrapper(RestWrapper restWrapper);
	
}
