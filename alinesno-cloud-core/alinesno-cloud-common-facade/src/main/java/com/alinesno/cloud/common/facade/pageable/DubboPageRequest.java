package com.alinesno.cloud.common.facade.pageable;

import java.io.Serializable;

import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.AbstractPageRequest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.lang.Nullable;

public class DubboPageRequest  implements Pageable, Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int page = 0 ;
	private int size = 0 ;

	private DubboSort sort;


	public DubboPageRequest() {
		super();
	}

	/**
	 * Creates a new {@link AbstractPageRequest}. Pages are zero indexed, thus providing 0 for {@code page} will return
	 * the first page.
	 *
	 * @param page must not be less than zero.
	 * @param size must not be less than one.
	 */
	public DubboPageRequest(int page, int size) {

		if (page < 0) {
			throw new IllegalArgumentException("Page index must not be less than zero!");
		}

		if (size < 1) {
			throw new IllegalArgumentException("Page size must not be less than one!");
		}

		this.page = page;
		this.size = size;
	}

	/**
	 * Creates a new {@link PageRequest} with sort parameters applied.
	 *
	 * @param page zero-based page index.
	 * @param size the size of the page to be returned.
	 * @param sort can be {@literal null}.
	 * @deprecated since 2.0, use {@link #of(int, int, Sort)} instead.
	 */
	@Deprecated
	public DubboPageRequest(int page, int size, DubboSort sort) {

		if (page < 0) {
			throw new IllegalArgumentException("Page index must not be less than zero!");
		}

		if (size < 1) {
			throw new IllegalArgumentException("Page size must not be less than one!");
		}

		this.page = page;
		this.size = size;

		this.sort = sort;
	}	
	
	/**
	 * Creates a new unsorted {@link PageRequest}.
	 *
	 * @param page zero-based page index.
	 * @param size the size of the page to be returned.
	 * @since 2.0
	 */
	public static DubboPageRequest of(int page, int size) {
		return of(page, size, DubboSort.unsorted());
	}
	
	/**
	 * Creates a new {@link PageRequest} with sort parameters applied.
	 *
	 * @param page zero-based page index.
	 * @param size the size of the page to be returned.
	 * @param sort must not be {@literal null}.
	 * @since 2.0
	 */
	public static DubboPageRequest of(int page, int size, DubboSort sort) {
		return new DubboPageRequest(page, size, sort);
	}
	
	/**
	 * Creates a new {@link PageRequest} with sort direction and properties applied.
	 *
	 * @param page zero-based page index.
	 * @param size the size of the page to be returned.
	 * @param direction must not be {@literal null}.
	 * @param properties must not be {@literal null}.
	 * @since 2.0
	 */
	public static DubboPageRequest of(int page, int size, DubboSort.Direction direction, String... properties) {
		return of(page, size, DubboSort.by(direction, properties));
	}


	/*
	 * (non-Javadoc)
	 * @see org.springframework.data.domain.Pageable#getPageSize()
	 */
	public int getPageSize() {
		return size;
	}

	/*
	 * (non-Javadoc)
	 * @see org.springframework.data.domain.Pageable#getPageNumber()
	 */
	public int getPageNumber() {
		return page;
	}

	/*
	 * (non-Javadoc)
	 * @see org.springframework.data.domain.Pageable#getOffset()
	 */
	public long getOffset() {
		return (long) page * (long) size;
	}

	/*
	 * (non-Javadoc)
	 * @see org.springframework.data.domain.Pageable#hasPrevious()
	 */
	public boolean hasPrevious() {
		return page > 0;
	}

	/*
	 * (non-Javadoc)
	 * @see org.springframework.data.domain.Pageable#previousOrFirst()
	 */
	public Pageable previousOrFirst() {
		return hasPrevious() ? previous() : first();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;

		result = prime * result + page;
		result = prime * result + size;

		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(@Nullable Object obj) {

		if (this == obj) {
			return true;
		}

		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		DubboPageRequest other = (DubboPageRequest) obj;
		return this.page == other.page && this.size == other.size;
	}
	

	/*
	 * (non-Javadoc)
	 * @see org.springframework.data.domain.Pageable#getSort()
	 */
	public DubboSort getDubboSort() {
		return sort;
	}

	/*
	 * (non-Javadoc)
	 * @see org.springframework.data.domain.Pageable#next()
	 */
	public DubboPageRequest next() {
		return new DubboPageRequest(getPageNumber() + 1, getPageSize(), getDubboSort());
	}

	/*
	 * (non-Javadoc)
	 * @see org.springframework.data.domain.AbstractPageRequest#previous()
	 */
	public DubboPageRequest previous() {
		return getPageNumber() == 0 ? this : new DubboPageRequest(getPageNumber() - 1, getPageSize(), getDubboSort());
	}

	/*
	 * (non-Javadoc)
	 * @see org.springframework.data.domain.Pageable#first()
	 */
	public Pageable first() {
		return new DubboPageRequest(0, getPageSize(), getDubboSort());
	}

	@Override
	public Sort getSort() {
		Sort sort = Sort.unsorted() ; 
		BeanUtils.copyProperties(getDubboSort(), sort);  
		return sort ;
	}

}
