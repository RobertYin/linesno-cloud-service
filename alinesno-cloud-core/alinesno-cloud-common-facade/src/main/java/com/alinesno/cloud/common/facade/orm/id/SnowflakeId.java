package com.alinesno.cloud.common.facade.orm.id;

import java.io.Serializable;
import java.util.Properties;

import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.Configurable;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.type.Type;

/**
 * 生成id的方法 
 * @author LuoAnDong
 * @since 2018年8月12日 下午5:56:46
 */
public class SnowflakeId implements IdentifierGenerator,Configurable{
	
    public SnowflakeIdWorker snowFlakeIdWorker;

	@Override
	public void configure(Type arg0, Properties arg1, ServiceRegistry arg2) throws MappingException {
        snowFlakeIdWorker = new SnowflakeIdWorker(0,0) ; 
	}

	@Override
	public Serializable generate(SharedSessionContractImplementor arg0, Object arg1) throws HibernateException {
		return snowFlakeIdWorker.nextId()+"" ;
	}


}