package com.alinesno.cloud.common.core.auto;

import java.util.ArrayList;
import java.util.List;

import com.alinesno.cloud.common.core.cache.RedisConfig;
import com.alinesno.cloud.common.core.config.AsyncConfig;
import com.alinesno.cloud.common.core.context.ApplicationContextProvider;
import com.alinesno.cloud.common.core.dubbo.filter.DubboServicePermissionFilter;
import com.alinesno.cloud.common.core.exceptions.aspect.RpcServiceExceptionAspect;
import com.alinesno.cloud.common.core.hystrix.HystrixDashboardConfiguration;

/**
 * 对外提供核心对象,便于springboot实现enable功能
 * @author LuoAnDong
 * @since 2019年4月7日 下午2:41:44
 */
public class CoreImportProvider {

	/**
	 * 提供接口对象 
	 * @return
	 */
	public static List<String> classLoader(){
		List<String> s = new ArrayList<>() ; 
		
		s.add(ApplicationContextProvider.class.getName()) ; 
		s.add(AsyncConfig.class.getName()) ;  // 添加异步线程池
		s.add(HystrixDashboardConfiguration.class.getName()) ; 
		s.add(RedisConfig.class.getName()) ;  // Redis配置
		
		// Dubbo统一异常处理
		s.add(RpcServiceExceptionAspect.class.getName()) ;  
		s.add(DubboServicePermissionFilter.class.getName()) ;  

		return s ; 
	}
	
}
