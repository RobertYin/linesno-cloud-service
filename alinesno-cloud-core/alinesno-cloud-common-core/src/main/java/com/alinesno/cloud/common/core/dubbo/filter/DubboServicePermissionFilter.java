package com.alinesno.cloud.common.core.dubbo.filter;

import org.apache.dubbo.common.constants.CommonConstants;
import org.apache.dubbo.common.extension.Activate;
import org.apache.dubbo.rpc.Filter;
import org.apache.dubbo.rpc.Invocation;
import org.apache.dubbo.rpc.Invoker;
import org.apache.dubbo.rpc.Result;
import org.apache.dubbo.rpc.RpcContext;
import org.apache.dubbo.rpc.RpcException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Dubbo服务拦截器，用于权限，调用日志等拦截，用于全局拦截
 * 
 * @author LuoAnDong
 * @since 2019年9月19日 上午7:23:53
 */
@Activate(group = {CommonConstants.PROVIDER, CommonConstants.CONSUMER }, order = -2000)
public class DubboServicePermissionFilter implements Filter {

	private static final Logger log = LoggerFactory.getLogger(DubboServicePermissionFilter.class);

	@Override
	public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
		Result result = invoker.invoke(invocation);
		log.debug("Dubbo权限过滤器:{}", result);

		// =======服务调用方========
		if (RpcContext.getContext().isConsumerSide()) {
			log.debug("consumer side");
				
		} else if (RpcContext.getContext().isProviderSide()) {
			// =======服务提供者========
			log.debug("provider side");
			
		}

		return result;
	}

}
