package com.alinesno.cloud.common.core.rest;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;

import com.alinesno.cloud.common.facade.wrapper.RestWrapper;

public abstract class SuperRestController {
	

	private static final Logger log = LoggerFactory.getLogger(SuperRestController.class);


	protected <T> ResponseEntity<T> ok(T body) {
		return ResponseEntity.ok(body);
	}
	
	@SuppressWarnings("serial")
	protected <T> Specification<T> SpecificationBuilder(RestWrapper entityWrapper) {
		
		log.debug("entity wrapper = {}" , ToStringBuilder.reflectionToString(entityWrapper));
		
		Specification<T> spec = new Specification<T>() {
			@Override
			public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				return entityWrapper.toSelfPredicate(root, query, criteriaBuilder);
			}
		};
		return spec ;
	}


}
